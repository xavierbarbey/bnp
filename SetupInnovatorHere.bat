@echo off

echo *********************************************************************************************************************************************************************
echo You are going to setup new Innovator instance in local IIS using this repository as a code tree. Adjust configuration files to work with this instance.
echo *********************************************************************************************************************************************************************

SET PathToThisBatFileFolder=%~dp0
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\CheckAdminPrivileges.bat 
IF errorlevel 1 GOTO END
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\GetMachineSpecificIncludes.bat 
IF errorlevel 1 GOTO END
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\GitIgnoreTracked.bat
IF errorlevel 1 GOTO END
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\SetupExternalTools.bat
IF errorlevel 1 GOTO END

SET WorkingDirectory=%CD%
CD /D "%PathToThisBatFileFolder%"

@REM Remove trailing backslash as it is interpreted as invalid character by NAnt
SET PathToThisBatFileFolder=%PathToThisBatFileFolder:~0,-1%

FOR /f "delims= " %%i in ('wmic computersystem get name') DO FOR /f "delims=" %%t in ("%%i") DO SET Local.Machine.Name=%%t
FOR /f "delims=" %%i in ('git rev-parse --abbrev-ref HEAD') DO SET Commit.Pointer=%%i
SET Path.To.Installed.Innovator=%PathToThisBatFileFolder%

"%PathToNantExe%" ^
	"/f:AutomatedProcedures\NantScript.xml" ^
	SetupParameters.For.Developer.Environment ^
	Clean.Up.For.Developer.Environment ^
	Setup.Innovator.From.Backups ^
	Deploy ^
	Print.Url.Of.Installed.Innovator ^
	-D:Do.Deploy.Agent.Service=false ^
	-D:Do.Deploy.Conversion.Server=false ^
	-D:Do.Deploy.Innovator.Application=false ^
	-D:Do.Deploy.OAuth.Server=false ^
	-D:Do.Deploy.Self.Service.Report=false ^
	-D:Do.Deploy.Vault.Server=false ^
	-D:MachineSpecific.Includes.Folder.Path=%MachineSpecificIncludesPath% ^
	-D:Is.MachineSpecific.Includes.Mandatory=true

:END
if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

CD /D "%WorkingDirectory%"
pause