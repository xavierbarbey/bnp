<?xml version="1.0" encoding="utf-8"?>
<!-- FILENAME: VaultServer/web-debug.config
     PURPOSE:  configure Innovator Vault Server application.
     USE:      INTENDED FOR DEBUGGING
     NOTE:     For details on editing this file you must refer to http://msdn.microsoft.com/
               "NET Framework Developer's Guide" "ASP.NET Configuration"


maxRequestLength     unit: Kilobytes (2^10 bytes).
executionTimeout     unit: seconds.
sessionState timeout unit: minutes (60 seconds).

note: under heavy load it make sense to modify machine.config
and set processModel responseDeadlockInterval

-->

<configuration>
	<configSections>
		<section name="oauth" type="Aras.OAuth.Configuration.OAuthSection, Aras.OAuth.Configuration" />
		<section name="mimeTypeModule" type="Aras.Vault.Core.MimeTypeModuleConfigurationSection, VaultWebApplicationCore" />
		<sectionGroup name="Aras">
			<sectionGroup name="Net">
				<section name="RequestProvider" type="Aras.Net.Configuration.RequestProviderConfigurationSection, Aras.Net" />
			</sectionGroup>
		</sectionGroup>
	</configSections>
	<oauth configSource="OAuth.config" />
	<runtime>
		<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
			<!-- Binding redirects from Aras.Web.Server/App.config -->
			<dependentAssembly>
				<assemblyIdentity name="System.IdentityModel.Tokens.Jwt" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-5.3.0.0" newVersion="5.3.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.0.0.0" newVersion="11.0.0.0" />
			</dependentAssembly>
		</assemblyBinding>
	</runtime>
	<system.web>
    <compilation defaultLanguage="vb" debug="false" targetFramework="4.7.2" />
    <customErrors mode="On" />
    <authentication mode="None" /> 
    <authorization><allow users="*" /></authorization>
    <trace enabled="false" requestLimit="100" pageOutput="false" traceMode="SortByTime" localOnly="true" />
    <sessionState mode="Off" />
    <globalization requestEncoding="utf-8" responseEncoding="utf-8" />
    <httpRuntime maxRequestLength="2097151" executionTimeout="36000" requestValidationMode="2.0" targetFramework="4.7.2" />
    <pages validateRequest="false" />
		<httpModules>
			<add name="MimeTypeModule" type="Aras.Vault.Core.MimeTypeModule, VaultWebApplicationCore" />
		</httpModules>
	</system.web>
	<system.webServer>
		<validation validateIntegratedModeConfiguration="false" />
		<modules>
			<add name="MimeTypeModule" type="Aras.Vault.Core.MimeTypeModule, VaultWebApplicationCore" />
		</modules>
		<handlers  accessPolicy="Read, Script">
			<add verb="POST"
				name="OData"
				path="*/vault/odata"
				type="Aras.Server.Vault.VaultODataHttpHandler"
				resourceType="Unspecified" />
		</handlers>
		<httpProtocol>
			<customHeaders>
				<remove name="Access-Control-Allow-Origin" />
				<remove name="Access-Control-Allow-Headers" />
				<!--
					Replace value="*" to value="https://innovator.site" (where "innovator.site" is domain name of deployed Innovator)
					in the header below to allow browser requests only from "innovator.site" and thus prevent potential CSRF attacks.
				-->
				<add name="Access-Control-Allow-Origin" value="*" />
				<add name="Access-Control-Allow-Headers" value="content-disposition,content-range,content-type,locale,soapaction,timezone_name,transactionid,vaultid,Aras-Content-Range-Checksum-Type,Aras-Content-Range-Checksum,Authorization,X-Aras-Authorization" />
				<add name="X-Content-Type-Options" value="nosniff" />
			</customHeaders>
		</httpProtocol>
		<security> 
			<requestFiltering> 
				<requestLimits maxAllowedContentLength="2147483648" />
			</requestFiltering> 
		</security>
      <directoryBrowse enabled="false" />
      <defaultDocument>
        <files>
          <clear />
          <add value="VaultServer.aspx" />
        </files>
      </defaultDocument>
      <httpErrors>
        <clear />
      </httpErrors>
  </system.webServer>
	<!--Use mimeType value="*" to setup default behavior(inline or attachment) for the rest mime types that are not implicitly specified in this section-->
	<mimeTypeModule>
		<mimeType value="drawing/x-dwf">
			<headers>
				<add name="Content-Disposition" value="attachment" />
			</headers>
		</mimeType>
		<mimeType value="application/x-xvlplayer">
			<headers>
				<add name="Content-Disposition" value="attachment" />
			</headers>
		</mimeType>
		<mimeType value="text/html">
			<headers>
				<add name="Content-Disposition" value="inline" />
				<!-- X-Content-Security-Policy is used only for IE -->
				<add name="X-Content-Security-Policy" value="sandbox" />
				<!-- script-src is used only for Firefox 45 -->
				<add name="Content-Security-Policy" value="sandbox;script-src 'none'" />
			</headers>
		</mimeType>
		<mimeType value="x-gzip">
			<headers>
				<add name="Content-Disposition" value="attachment" />
			</headers>
		</mimeType>
		<mimeType value="application/zip">
			<headers>
				<add name="Content-Disposition" value="attachment" />
			</headers>
		</mimeType>
		<mimeType value="text/xml">
			<headers>
				<add name="Content-Disposition" value="attachment" />
			</headers>
		</mimeType>
		<mimeType value="image/png">
			<headers>
				<add name="Content-Disposition" value="inline" />
			</headers>
		</mimeType>
		<mimeType value="image/tiff">
			<headers>
				<add name="Content-Disposition" value="inline" />
			</headers>
		</mimeType>
		<mimeType value="image/bmp">
			<headers>
				<add name="Content-Disposition" value="inline" />
			</headers>
		</mimeType>
		<mimeType value="application/pdf">
			<headers>
				<add name="Content-Disposition" value="inline" />
			</headers>
		</mimeType>
		<mimeType value="image/gif">
			<headers>
				<add name="Content-Disposition" value="inline" />
			</headers>
		</mimeType>
		<mimeType value="image/jpeg">
			<headers>
				<add name="Content-Disposition" value="inline" />
			</headers>
		</mimeType>
		<mimeType value="image/svg+xml">
			<headers>
				<add name="Content-Disposition" value="inline" />
			</headers>
		</mimeType>
		<mimeType value="*">
			<headers>
				<add name="Content-Disposition" value="attachment" />
			</headers>
		</mimeType>
	</mimeTypeModule>
  	<!--Specify the providers; if authentication mode="Basic" then use Example1, if mode="Windows" or another use Example2-->
	<Aras>
		<Net>
			<RequestProvider>
				<providers>
					<provider uriPattern=".*">
						<Timeout value="7200000" />
					</provider>
				<!--Example1:-->
					<!--<provider uriPattern="{uriPattern}" type="{type}">
							<authentication mode="{mode}">
								<Basic>
									<credentials>
										<user name="{name}" password="{password}" />
									</credentials>
								</Basic>
							</authentication>
							<Proxy mode="{mode}">
								<Basic>
									<credentials>
										<user name="{name}" password="{password}" />
									</credentials>
								</Basic>
							</Proxy>
					</provider>-->
					<!--Example2:-->
					<!--<provider uriPattern="{uriPattern}" type="{type}">
							<authentication mode="{mode}">
							</authentication>
							<Proxy mode="{mode}">							
							</Proxy>
					</provider>-->
				</providers>
			</RequestProvider>
		</Net>
	</Aras>
</configuration>
