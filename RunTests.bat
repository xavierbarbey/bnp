@echo off

echo RunTests.bat
echo Target audience: QA team, Development team
echo Purpose: Run all tests. This inculdes both unit and integration tests.

SET PathToThisBatFileFolder=%~dp0
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\GetMachineSpecificIncludes.bat
IF errorlevel 1 GOTO END
CALL "%PathToThisBatFileFolder%AutomatedProcedures\BatchUtilityScripts\SetupExternalTools.bat
IF errorlevel 1 GOTO END

FOR /f "delims= " %%i in ('wmic computersystem get name') DO FOR /f "delims=" %%t in ("%%i") DO SET Local.Machine.Name=%%t
FOR /f "delims=" %%i in ('git rev-parse --abbrev-ref HEAD') DO SET Commit.Pointer=%%i
SET Path.To.Installed.Innovator=%PathToThisBatFileFolder%

"%PathToNantExe%" ^
	"/f:%PathToThisBatFileFolder%AutomatedProcedures\NantScript.xml" ^
	SetupParameters.For.Developer.Environment ^
	RunTests ^
	-D:MachineSpecific.Includes.Folder.Path=%MachineSpecificIncludesPath% ^
	-D:Is.MachineSpecific.Includes.Mandatory=true

:END
if not errorlevel 1 (
	powershell write-host -foregroundcolor green "SUCCESS!!!"
) else (
	powershell write-host -foregroundcolor red "FAILURE!!!"
)

pause