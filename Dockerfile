FROM mcr.microsoft.com/windows:1809

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

ENV DOTNETCORE_VERSION=2.1.8 \
    FIREFOXESR_VERSION=60.0 \
    MSBUILDTOOLS_VERSION=15.0.26320.2 \
    NODEJS_VERSION=8.9.4

# Disable IE customization windows during first run to allow client unit tests
RUN New-Item -Path "\"HKLM:\\SOFTWARE\\Policies\\Microsoft\\Internet Explorer\\Main\"" -Force; \
    New-ItemProperty -Path "\"HKLM:\\SOFTWARE\\Policies\\Microsoft\\Internet Explorer\\Main\"" -Name "DisableFirstRunCustomize" -Value 1 -Force

# Setup NetFx3
RUN Invoke-WebRequest -UseBasicParsing -Uri https://dotnetbinaries.blob.core.windows.net/dockerassets/microsoft-windows-netfx3-1809.zip -OutFile microsoft-windows-netfx3.zip; \
    Expand-Archive microsoft-windows-netfx3.zip; \
    Remove-Item -Force microsoft-windows-netfx3.zip; \
    Add-WindowsPackage -Online -PackagePath .\\microsoft-windows-netfx3\\microsoft-windows-netfx3-ondemand-package~31bf3856ad364e35~amd64~~.cab; \
    Remove-Item -Force -Recurse microsoft-windows-netfx3

# Enable .Net and IIS Features
RUN Enable-WindowsOptionalFeature -Online -FeatureName IIS-WebServer,IIS-ASP,IIS-ASPNET,IIS-ASPNET45,IIS-NetFxExtensibility,IIS-ISAPIExtensions,IIS-ISAPIFilter,IIS-WindowsAuthentication -All

# Download chocolatey
RUN Set-ExecutionPolicy Unrestricted -Force; \
    Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')

# Install chocolatey packages
RUN choco install git.install -y
# Since Google maintains only last version of Google Chrome
RUN choco install googlechrome -y
RUN choco install dotnetfx --version 4.7.2.20180712 -y
RUN choco install netfx-4.7.2-devpack -y
RUN choco install microsoft-build-tools --version $env:MSBUILDTOOLS_VERSION -y
RUN choco install dotnetcore-runtime --version $env:DOTNETCORE_VERSION -y; \
    choco install dotnetcore-windowshosting --version $env:DOTNETCORE_VERSION -y
RUN choco install nodejs --version $env:NODEJS_VERSION -y
RUN choco install firefoxesr --version $env:FIREFOXESR_VERSION -y

# Enhance a limit for a filename to 4096 characters to avaoid long path issues
# Allow Windows to handle paths more than 260 characters
RUN & 'C:\\Program Files\\Git\\bin\\git.exe' config --global core.longpaths true;