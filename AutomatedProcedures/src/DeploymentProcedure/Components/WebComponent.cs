﻿using DeploymentProcedure.Components.Base;
using DeploymentProcedure.Logging;
using DeploymentProcedure.Utility;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;

namespace DeploymentProcedure.Components
{
	public class WebComponent : CodeTreeComponent
	{
		public string VirtualDirectoryPath { get; set; }
		public string ApplicationPoolName { get; set; }
		public string ManagedRuntimeVersion { get; set; } = "v4.0";
		public string ManagedPipelineMode { get; set; } = "Integrated";
		public string SiteName { get; set; } = "Default web Site";
		public string Protocol { get; set; } = "http";
		public string Url => string.Format("{0}://{1}{2}", Protocol, ServerName, VirtualDirectoryPath);

		#region Implementing Setup logic
		public override void Setup()
		{
			base.Setup();

			SetupApplicationPool(ApplicationPoolName, ManagedRuntimeVersion, ManagedPipelineMode);
			SetupApplication(SiteName, VirtualDirectoryPath, ApplicationPoolName, InstallationPath);
		}

		public void SetupApplicationPool(string apppoolName, string managedRuntimeVersion, string managedPipelineMode)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				ApplicationPool appPool = serverManager.ApplicationPools[apppoolName];
				if (appPool == null)
				{
					Logger.Instance.Log(LogLevel.Info, "\tCreating new '{0}' application pool on '{1}'...", apppoolName, ServerName);

					appPool = serverManager.ApplicationPools.Add(apppoolName);
				}
				else
				{
					Logger.Instance.Log(LogLevel.Info, "\tRecycling '{0}' application pool on '{1}'...", apppoolName, ServerName);

					appPool.Recycle();
				}

				Logger.Instance.Log(LogLevel.Info, "\tSetting up '{0}' managed runtime version and '{1}' managed pipeline mode '{2}' application pool.",
					managedRuntimeVersion, managedPipelineMode, apppoolName);

				appPool.ManagedRuntimeVersion = managedRuntimeVersion;
				appPool.ManagedPipelineMode = ParseManagedPipelineMode(managedPipelineMode);

				serverManager.CommitChanges();

				Logger.Instance.Log(LogLevel.Info, "\tChanges were commited.");
			}
		}

		public void SetupApplication(string siteName, string virtualDirectoryName, string apppoolName, string physicalPath)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				Application app = serverManager.Sites[siteName].Applications[virtualDirectoryName];
				if (app != null)
				{
					Logger.Instance.Log(LogLevel.Info, "\tRemoving '{0}' application from '{1}'...", virtualDirectoryName, ServerName);

					serverManager.Sites[SiteName].Applications.Remove(app);
				}

				Logger.Instance.Log(LogLevel.Info, "\tCreaing new '{0}' application with '{1}' application pool.", virtualDirectoryName, apppoolName);

				app = serverManager.Sites[SiteName].Applications.Add(virtualDirectoryName, physicalPath);
				app.ApplicationPoolName = apppoolName;

				serverManager.CommitChanges();

				Logger.Instance.Log(LogLevel.Info, "\tChanges were commited.");
			}
		}

		protected void ConfigureInnovatorIISWebApplication(string locationPath)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				Configuration config = serverManager.GetApplicationHostConfiguration();

				ConfigurationSection aspSection = config.GetSection("system.webServer/asp", locationPath);
				aspSection.ChildElements["session"]["timeout"] = TimeSpan.FromMinutes(20); // "00:20:00"
				aspSection.ChildElements["limits"]["scriptTimeout"] = TimeSpan.FromSeconds(90); // "00:01:30"

				ConfigurationSection anonymousAuthenticationSection = config.GetSection("system.webServer/security/authentication/anonymousAuthentication", locationPath);
				anonymousAuthenticationSection["enabled"] = true;

				ConfigurationSection basicAuthenticationSection = config.GetSection("system.webServer/security/authentication/basicAuthentication", locationPath);
				basicAuthenticationSection["enabled"] = false;

				ConfigurationSection windowsAuthenticationSection = config.GetSection("system.webServer/security/authentication/windowsAuthentication", locationPath);
				windowsAuthenticationSection["enabled"] = false;

				ConfigurationSection httpLoggingSection = config.GetSection("system.webServer/httpLogging", locationPath);
				httpLoggingSection["dontLog"] = false;

				serverManager.CommitChanges();
			}
		}

		public void SetupWinAuth(string locationPath)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				Configuration config = serverManager.GetApplicationHostConfiguration();

				ConfigurationSection anonymousAuthenticationSection = config.GetSection("system.webServer/security/authentication/anonymousAuthentication", locationPath);
				anonymousAuthenticationSection["enabled"] = false;

				ConfigurationSection windowsAuthenticationSection = config.GetSection("system.webServer/security/authentication/windowsAuthentication", locationPath);
				windowsAuthenticationSection["enabled"] = true;

				serverManager.CommitChanges();
			}
		}

		private static ManagedPipelineMode ParseManagedPipelineMode(string managedPipelineModeString)
		{
			return (ManagedPipelineMode)Enum.Parse(typeof(ManagedPipelineMode), managedPipelineModeString);
		}
		#endregion

		#region Implementing Cleanup logic
		public override void Remove()
		{
			Logger.Instance.Log(LogLevel.Info, "\nRemoving component ({0}):\n", Id);

			RemoveApplicationPoolWithApplications(ApplicationPoolName);
			RemoveLocationFromIISApplicationHostConfigByPath(VirtualDirectoryPath);

			base.Remove();
		}

		public void RemoveLocationFromIISApplicationHostConfigByPath(string locationPath)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				Configuration config = serverManager.GetApplicationHostConfiguration();

				config.RemoveLocationPath(locationPath);
				serverManager.CommitChanges();
			}
		}

		public void RemoveApplicationPoolWithApplications(string apppoolName)
		{
			using (ServerManager serverManager = ServerManager.OpenRemote(ServerName))
			{
				foreach (Site site in serverManager.Sites)
				{
					List<Application> applicationsToRemoveList = site.Applications.Where(app => app.ApplicationPoolName.Equals(apppoolName)).ToList();
					foreach (Application applicationToRemove in applicationsToRemoveList)
					{
						Logger.Instance.Log(LogLevel.Info, "\tRemoving '{0}' application '{1}'...", applicationToRemove.Path, ServerName);

						site.Applications.Remove(applicationToRemove);

					}
				}

				List<ApplicationPool> applicationPoolsToRemoveList = serverManager.ApplicationPools.Where(apppool => apppool.Name.Equals(apppoolName)).ToList();
				foreach (ApplicationPool applicationPoolToRemove in applicationPoolsToRemoveList)
				{
					Logger.Instance.Log(LogLevel.Info, "\tRemoving '{0}' application pool '{1}'...", applicationPoolToRemove.Name, ServerName);

					serverManager.ApplicationPools.Remove(applicationPoolToRemove);

				}

				serverManager.CommitChanges();
			}
		}
		#endregion

		#region Implementing ApplyPackage
		public override void HealthCheck()
		{
			ValidationHelper.CheckHostAvailability(ServerName);
			ValidationHelper.CheckWebApplicationAvailability(this);
		}
		#endregion
	}
}
