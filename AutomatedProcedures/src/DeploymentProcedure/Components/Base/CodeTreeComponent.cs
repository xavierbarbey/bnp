﻿using DeploymentProcedure.Logging;
using DeploymentProcedure.Packages;
using DeploymentProcedure.Utility;
using DeploymentProcedure.Utility.Base;
using System;
using System.Linq;
using System.Xml.Serialization;

namespace DeploymentProcedure.Components.Base
{
	public abstract class CodeTreeComponent : Component
	{
		[XmlIgnore]
		internal IFileSystem LocalFileSystem => FileSystemFactory.Local;
		[XmlIgnore]
		internal IFileSystem TargetFileSystem => FileSystemFactory.GetFileSystem(ServerName);

		public virtual string BaselineSourcePath { get; set; }
		public virtual string DeploymentPackageDirectoryName { get; set; }
		public string InstallationPath { get; set; }
		public string ServerName { get; set; } = InitializeServerNameFromEnvironment() ?? "localhost";
		public virtual string PathToCodeTreeTemplates { get; }
		public virtual string PathToConfig { get; set; }
		public virtual string PathToConfigTemplate { get; }
		public virtual string PathToBasicConfig { get; }

		#region Implementing Setup logic
		public override void RunPreSetupValidation()
		{
			base.RunPreSetupValidation();

			ValidationHelper.CheckHostAvailability(ServerName);
			ValidationHelper.CheckWritePermissionsToDirectory(TargetFileSystem, InstallationPath);
		}

		public override void Setup()
		{
			base.Setup();

			LocalFileSystem.CopyDirectory(BaselineSourcePath, TargetFileSystem, InstallationPath);
			SetupConfig();
			SetupCodeTreeTemplates();
		}

		protected void SetupConfigFromTemplate(string pathToConfig, string pathToTemplate)
		{
			if (!TargetFileSystem.FileExists(pathToConfig) && LocalFileSystem.FileExists(pathToTemplate))
			{
				Logger.Instance.Log(LogLevel.Info, "\tCreating config '{0}' from '{1}' template", pathToConfig, pathToTemplate);

				LocalFileSystem.CopyFile(pathToTemplate, TargetFileSystem, pathToConfig, true);
			}
		}

		protected void SetupNtfsPermissionsToFolder(string pathToFolder)
		{
			if (!TargetFileSystem.DirectoryExists(pathToFolder))
			{
				TargetFileSystem.CreateDirectory(pathToFolder);
			}

			ProcessWrapper.Execute("icacls", "{0} /grant *S-1-5-11:(OI)(CI)(M)", pathToFolder);
		}

		private void SetupCodeTreeTemplates()
		{
			if (LocalFileSystem.DirectoryExists(PathToCodeTreeTemplates))
			{
				LocalFileSystem.CopyDirectory(PathToCodeTreeTemplates, TargetFileSystem, InstallationPath);
			}
		}

		private void SetupConfig()
		{
			Logger.Instance.Log(LogLevel.Info, "\nSetting up config for component ({0}):\n", Id);

			SetupConfigFromTemplate(PathToConfig, PathToConfigTemplate);

			if (!string.IsNullOrEmpty(PathToBasicConfig))
			{
				Logger.Instance.Log(LogLevel.Info, "\tCreating basic config '{0}'", PathToBasicConfig);

				string configContent = string.Format("<ConfigFilePath value=\"{0}\" />", PathToConfig);
				TargetFileSystem.WriteAllTextToFile(PathToBasicConfig, configContent);
			}
		}
		#endregion

		#region Implementing Cleanup logic
		public override void Remove()
		{
			if (TargetFileSystem.FileExists(PathToConfig))
			{
				TargetFileSystem.DeleteFile(PathToConfig);
			}

			if (TargetFileSystem.DirectoryExists(InstallationPath) && !string.Equals(LocalFileSystem.GetFullPath(BaselineSourcePath), TargetFileSystem.GetFullPath(InstallationPath), StringComparison.InvariantCultureIgnoreCase))
			{
				try
				{
					TargetFileSystem.DeleteDirectory(InstallationPath);
				}
				catch (UnauthorizedAccessException)
				{
					foreach (System.Diagnostics.Process p in Win32ProcessHelper.GetProcessesLockingFile(GetUnremovedFile(InstallationPath)))
					{
						Console.WriteLine("File is locked by Process(ID: {0}, NAME: {1}). Killing the process to unlock files", p.Id, p.ProcessName);
						if (!p.WaitForExit(1000))
						{
							Win32ProcessHelper.TerminateProcess(p);
							p.WaitForExit(5000);
						}
					}

					TargetFileSystem.DeleteDirectory(InstallationPath);
				}
			}
		}

		private string GetUnremovedFile(string dir)
		{
			foreach (string subdir in TargetFileSystem.EnumerateDirectories(dir))
			{
				return GetUnremovedFile(subdir);
			}

			return TargetFileSystem.EnumerateFiles(dir).FirstOrDefault();
		}
		#endregion

		#region Implementing ApplyPackage
		public override void ApplyPackage(Package package)
		{
			base.ApplyPackage(package);

			string deploymentPackageSourcePath = FileSystem.CombinePaths(package.PathToDeploymentPackage, DeploymentPackageDirectoryName);
			if (LocalFileSystem.DirectoryExists(deploymentPackageSourcePath))
			{
				LocalFileSystem.CopyDirectory(deploymentPackageSourcePath, TargetFileSystem, InstallationPath);
			}
			else
			{
				Logger.Instance.Log(LogLevel.Info, "\tNothing found to deploy to component ({0}):\n", Id);
			}
		}

		public override void HealthCheck()
		{
			ValidationHelper.CheckHostAvailability(ServerName);
			ValidationHelper.CheckDirectoryExistence(TargetFileSystem, InstallationPath);
			ValidationHelper.CheckWritePermissionsToDirectory(TargetFileSystem, InstallationPath);
		}
		#endregion

		private static string InitializeServerNameFromEnvironment()
		{
			string urlOfDeploymentServer = Environment.GetEnvironmentVariable("Url.Of.Deployment.Server");
			return !string.IsNullOrEmpty(urlOfDeploymentServer) ? new Uri(urlOfDeploymentServer).Host : null;
		}
	}
}
