﻿using Aras.IOM;
using DeploymentProcedure.Components.Base;
using DeploymentProcedure.Components.Type;
using DeploymentProcedure.Logging;
using DeploymentProcedure.Utility;
using System;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using DeploymentProcedure.Packages;
using DeploymentProcedure.Components.Utility;

namespace DeploymentProcedure.Components
{
	public class DatabaseComponent : Component
	{
		public string SqlServer { get; set; }
		public string DatabaseName { get; set; }
		public string PathToDatabaseBackup { get; set; } = Settings.Default.PathToDBBak;
		public string SaLogin { get; set; } = "sa";
		public SecretString SaPassword { get; set; } = new SecretString { Value = "innovator" };
		public string InnovatorLogin { get; set; } = "innovator";
		public SecretString InnovatorPassword { get; set; } = new SecretString { Value = "innovator" };
		public string InnovatorRegularLogin { get; set; } = "innovator_regular";
		public SecretString InnovatorRegularPassword { get; set; } = new SecretString { Value = "innovator_regular" };
		public string LoginOfRootInnovatorUser { get; set; } = "root";
		public SecretString PasswordOfRootInnovatorUser { get; set; } = new SecretString { Value = "innovator" };
		public string InnovatorUrl { get; set; }
		public string InnovatorServerAspxUrl => InnovatorUrl.TrimEnd('/') + "/Server/InnovatorServer.aspx";
		public string PreAmlDeploymentScriptsDirectoryName { get; set; } = "AmlDeploymentScripts\\1-BeforeAmlPackagesImport";
		public string PostAmlDeploymentScriptsDirectoryName { get; set; } = "AmlDeploymentScripts\\2-AfterAmlPackagesImport";
		public string AmlPackagesDirectoryName { get; set; } = "AML-packages";
		public string AmlPackagesManifest { get; set; } = "imports.mf";
		public string PathToConsoleUpgradeLogFile { get; set; } = Path.Combine(Settings.Default.PathToLogs, "Upgrade.DB.log");
		public string LanguagePacksDirectoryName { get; set; } = "Language packs";
		public string PathToLanguageToolConfig { get; set; }

		private string SaConnectionString => string.Format("Data Source={0};User Id={1};Password={2}", SqlServer, SaLogin, SaPassword.Value);
		private string SaConnectionStringToDatabase => string.Format("{0};Initial Catalog={1}", SaConnectionString, DatabaseName);

		#region Implementing Setup logic
		public override void RunPreSetupValidation()
		{
			base.RunPreSetupValidation();

			ValidationHelper.CheckSqlServerConnection(SaConnectionString);
			ValidationHelper.CheckSqlServerConnection(string.Format("Data Source={0};User Id={1};Password={2}", SqlServer, InnovatorLogin, InnovatorPassword.Value));
			ValidationHelper.CheckSqlServerConnection(string.Format("Data Source={0};User Id={1};Password={2}", SqlServer, InnovatorRegularLogin, InnovatorRegularPassword.Value));
		}

		public override void Setup()
		{
			base.Setup();

			RestoreDatabase();

			MapUserToLogin("innovator", InnovatorLogin);
			MapUserToLogin("innovator_regular", InnovatorRegularLogin);

			UpdateInnovatorUser(CryptoHelper.MD5Hash("innovator"), "admin", "Innovator", "Admin", "30B991F927274FA3829655F50C99472E");
			UpdateInnovatorUser(CryptoHelper.MD5Hash("innovator"), "root", "Super", "User", "AD30A6D8D3B642F5A2AFED1A4B02BEFA");
			UpdateInnovatorUser(CryptoHelper.MD5Hash("vadmin"), "vadmin", "Vault", "Admin", "EB2D5AA617FB41A28F081345B8B5FECB");
		}

		private void RestoreDatabase()
		{
			using (SqlConnection connection = CreateLoggingSqlConnection(SaConnectionString))
			{
				connection.Open();

				string restoreDatabaseQuery = ResourceHelper.RetrieveResource("RestoreDatabase.sql");
				using (SqlCommand restoreDatabaseCommand = new SqlCommand(restoreDatabaseQuery, connection))
				{
					restoreDatabaseCommand.Parameters.AddWithValue("@path_to_db_bak", PathToDatabaseBackup);
					restoreDatabaseCommand.Parameters.AddWithValue("@database_name", DatabaseName);
					restoreDatabaseCommand.Parameters.AddWithValue("@database_name_to_sql_object", DatabaseName);
					restoreDatabaseCommand.Parameters.AddWithValue("@sql_server_data_paths_list", string.Empty);

					restoreDatabaseCommand.ExecuteNonQuery();
				}
			}
		}

		// TODO: foresee Win Auth. Maybe again stored procedure
		private void MapUserToLogin(string user, string login)
		{
			using (SqlConnection connection = CreateLoggingSqlConnection(SaConnectionStringToDatabase))
			{
				connection.Open();

				using (SqlCommand mapUsersToLoginsCommand = new SqlCommand("sp_change_users_login", connection))
				{
					mapUsersToLoginsCommand.CommandType = CommandType.StoredProcedure;

					mapUsersToLoginsCommand.Parameters.AddWithValue("@Action", "Update_One");
					mapUsersToLoginsCommand.Parameters.AddWithValue("@UserNamePattern", user);
					mapUsersToLoginsCommand.Parameters.AddWithValue("@LoginName", login);

					mapUsersToLoginsCommand.ExecuteNonQuery();
				}
			}
		}

		private void UpdateInnovatorUser(string passwordHash, string loginName, string firstName, string lastName, string id)
		{
			using (SqlConnection connection = CreateLoggingSqlConnection(SaConnectionStringToDatabase))
			{
				connection.Open();

				const string updateUserQuery = "UPDATE [innovator].[USER] SET" +
												" [password] = @PasswordHash" +
												", [logon_enabled] = '1'" +
												", [login_name] = @LoginName" +
												", [first_name] = @FirstName" +
												", [last_name] = @LastName" +
												" WHERE [id] = @Id";
				using (SqlCommand mapUsersToLoginsCommand = new SqlCommand(updateUserQuery, connection))
				{
					mapUsersToLoginsCommand.Parameters.AddWithValue("@PasswordHash", passwordHash);
					mapUsersToLoginsCommand.Parameters.AddWithValue("@LoginName", loginName);
					mapUsersToLoginsCommand.Parameters.AddWithValue("@FirstName", firstName);
					mapUsersToLoginsCommand.Parameters.AddWithValue("@LastName", lastName);
					mapUsersToLoginsCommand.Parameters.AddWithValue("@Id", id);

					mapUsersToLoginsCommand.ExecuteNonQuery();
				}
			}
		}

		private static SqlConnection CreateLoggingSqlConnection(string connectionString)
		{
			SqlConnection connection = new SqlConnection(connectionString);
			connection.InfoMessage += (s, e) => Logger.Instance.Log(LogLevel.Info, e.Message);
			return connection;
		}
		#endregion

		#region Implementing Clenaup logic
		public override void Remove()
		{
			Logger.Instance.Log(LogLevel.Info, "\nRemoving component ({0}):\n", Id);

			using (SqlConnection connection = new SqlConnection(SaConnectionString))
			{
				connection.Open();

				string dropDatabaseQuery = "DECLARE @drop_database_query NVARCHAR(4000) = N'IF DB_ID(''' + @database_name + ''') IS NOT NULL" +
											" BEGIN DROP DATABASE [' + @database_name + ']; END';" +
											" EXECUTE sp_executesql  @drop_database_query";
				using (SqlCommand dropDatabaseCommand = new SqlCommand(dropDatabaseQuery, connection))
				{
					dropDatabaseCommand.Parameters.AddWithValue("@database_name", DatabaseName);

					dropDatabaseCommand.ExecuteNonQuery();
				}
			}
		}
		#endregion

		#region Implementing ApplyPackage
		public override void ApplyPackage(Package package)
		{
			base.ApplyPackage(package);

			if (package.FeatureLicenses != null && package.FeatureLicenses.Count > 0)
			{
				Logger.Instance.Log(LogLevel.Info, "\t Applying feature licenses to {0}...", Id);

				FeatureLicenseManager featureLicenseManager = new FeatureLicenseManager(InnovatorUrl, DatabaseName, LoginOfRootInnovatorUser, PasswordOfRootInnovatorUser.Value);
				foreach (SecretString featureLicense in package.FeatureLicenses)
				{
					featureLicenseManager.ImportFeatureLicense(featureLicense.Value);
				}
			}

			string pathToPreAmlDeploymentScripts = Path.Combine(package.PathToDeploymentPackage, PreAmlDeploymentScriptsDirectoryName);
			if (Directory.Exists(pathToPreAmlDeploymentScripts))
			{
				Logger.Instance.Log(LogLevel.Info, "\nApplying pre-deployment AML script to ({0}):\n", Id);

				foreach (string pathToPreAmlDeploymentScript in Directory.GetFiles(pathToPreAmlDeploymentScripts))
				{
					Logger.Instance.Log(LogLevel.Info, "\t Applying pre script {0}...", pathToPreAmlDeploymentScript);

					ApplyAmlFromFile(pathToPreAmlDeploymentScript);
				}
			}

			RunConsoleUpgrade(Path.Combine(package.PathToDeploymentPackage, AmlPackagesDirectoryName), AmlPackagesManifest);
			RunLanguageTool(Path.Combine(package.PathToDeploymentPackage, LanguagePacksDirectoryName), PathToLanguageToolConfig);

			string pathToPostAmlDeploymentScripts = Path.Combine(package.PathToDeploymentPackage, PostAmlDeploymentScriptsDirectoryName);
			if (Directory.Exists(pathToPostAmlDeploymentScripts))
			{
				Logger.Instance.Log(LogLevel.Info, "\nApplying post-deployment AML script to ({0}):\n", Id);

				foreach (string pathToPostAmlDeploymentScript in Directory.GetFiles(pathToPostAmlDeploymentScripts))
				{
					Logger.Instance.Log(LogLevel.Info, "\t Applying post script {0}...", pathToPostAmlDeploymentScript);

					ApplyAmlFromFile(pathToPostAmlDeploymentScript);
				}
			}

			foreach (Variable variable in package.Variables)
			{
				ApplyAml(variable.GetAml());
			}
		}

		public override void HealthCheck()
		{
			ValidationHelper.CheckAmlApplicability(this);
		}

		private void RunConsoleUpgrade(string pathToAmlPackages, string amlPackagesManifest)
		{
			if (Directory.Exists(pathToAmlPackages))
			{
				Logger.Instance.Log(LogLevel.Info, "\nApplying AML packages to ({0}):\n", Id);

				int consoleUpgradeResultCode = ProcessWrapper.Execute(Settings.Default.PathToConsoleUpgrade,
					"database=\"{0}\" server=\"{1}\" login=\"root\" password=\"{2}\" release=\"{3}\" mfFile=\"{4}\" import verbose merge dir=\"{5}\" log=\"{6}\" timeout=\"1000000\"",
					DatabaseName, InnovatorUrl, PasswordOfRootInnovatorUser.Value, Settings.Default.InnovatorVersion, Path.Combine(pathToAmlPackages, amlPackagesManifest), pathToAmlPackages, PathToConsoleUpgradeLogFile);
				if (consoleUpgradeResultCode != 0)
				{
					throw new Exception(string.Format("Failed to apply AML packages from {0}, consoleUpgrade.exe returned exit code {1}.", pathToAmlPackages, consoleUpgradeResultCode));
				}
			}
		}

		private void RunLanguageTool(string pathtoLanguagePacks, string pathToLanguageToolConfig)
		{
			if (Directory.Exists(pathtoLanguagePacks))
			{
				Logger.Instance.Log(LogLevel.Info, "\nApplying language packs to ({0}):\n", Id);

				foreach (string languageCodeFolder in Directory.GetDirectories(pathtoLanguagePacks))
				{
					string languageCode = languageCodeFolder.Substring(languageCodeFolder.LastIndexOf('\\'), 2);

					if (string.IsNullOrEmpty(pathToLanguageToolConfig))
					{
						pathToLanguageToolConfig = Path.Combine(pathtoLanguagePacks, languageCodeFolder, "ImportConfig.xml");
					}

					int languageToolResultCode = ProcessWrapper.Execute(Settings.Default.PathToLanguageTool,
						"-import -config_file:\"{0}\" -folder:\"{1}\" -language_code:\"{2}\" -server:\"{3}\" -db:\"{4}\" -login:\"{5}\" -pwd:\"{6}\"",
						pathToLanguageToolConfig, Path.Combine(languageCodeFolder, "xml"), languageCode, SqlServer, DatabaseName, InnovatorLogin, InnovatorPassword.Value);
					if (languageToolResultCode != 0)
					{
						throw new Exception(string.Format("Failed to apply Language packs from {0}, LangugeTool.exe returned exit code {1}.", pathtoLanguagePacks, languageToolResultCode));
					}
				}
			}
		}

		private void ApplyAmlFromFile(string pathToFile)
		{
			string aml = File.ReadAllText(pathToFile);
			ApplyAml(aml);
		}

		private void ApplyAml(string aml)
		{
			HttpServerConnection connection = IomFactory.CreateHttpServerConnection(InnovatorUrl, DatabaseName, LoginOfRootInnovatorUser, PasswordOfRootInnovatorUser.Value);
			try
			{
				connection.Login();

				Innovator innovator = new Innovator(connection);
				Item result = innovator.applyAML(aml);
				if (result.isError())
				{
					Logger.Instance.Log(LogLevel.Error, result.ToString());

					throw new Exception(string.Format("Failed to apply AML {0}.", aml));
				}
			}
			finally
			{
				connection.Logout();
			}
		}
		#endregion
	}
}
