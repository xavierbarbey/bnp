﻿using System.IO;

namespace DeploymentProcedure.Components
{
	public class SchedulerComponent : WindowsServiceComponent
	{
		#region Overriding CodeTreeComponent properties
		public override string PathToExecutable => Path.Combine(InstallationPath, "InnovatorService.exe");
		public override string PathToCodeTreeTemplates => Path.Combine(Settings.Default.PathToTemplates, "Scheduler");
		public override string PathToConfig => Path.Combine(InstallationPath, "InnovatorServiceConfig.xml");
		public override string PathToConfigTemplate => Path.Combine(Settings.Default.PathToTemplates, "InnovatorServiceConfig.xml");
		public override string BaselineSourcePath { get; set; } = Path.Combine(Settings.Default.PathToCodeTree, "Scheduler");
		public override string DeploymentPackageDirectoryName { get; set; } = "Scheduler";
		#endregion
	}
}
