﻿namespace DeploymentProcedure.Logging
{
	internal enum LogLevel
	{
		Error = 1,
		Warning = 2,
		Info = 4,
		Debug = 8
	}
}
