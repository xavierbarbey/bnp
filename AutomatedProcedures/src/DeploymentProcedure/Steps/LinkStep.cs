﻿using DeploymentProcedure.Connectors;
using DeploymentProcedure.Connectors.Base;
using DeploymentProcedure.Steps.Base;
using System.Collections.Generic;
using System.Xml.Serialization;
using DeploymentProcedure.Components.Base;

namespace DeploymentProcedure.Steps
{
	public class LinkStep : BaseStep
	{
		[XmlElement("agent2innovator", typeof(AgentToInnovatorConnector))]
		[XmlElement("conversion2database", typeof(ConversionToDatabaseConnector))]
		[XmlElement("database2agent", typeof(DatabaseToAgentConnector))]
		[XmlElement("database2innovator", typeof(DatabaseToInnovatorConnector))]
		[XmlElement("database2scheduler", typeof(DatabaseToSchedulerConnector))]
		[XmlElement("database2ssr", typeof(DatabaseToSelfServiceReportingConnector))]
		[XmlElement("oauth2innovator", typeof(OAuthToInnovatorConnector))]
		[XmlElement("vault2database", typeof(VaultToDatabaseConnector))]
		[XmlElement("winauth", typeof(WinAuthConnector))]

		public List<BaseConnector> Connectors { get; set; }

		public override void Execute(IReadOnlyCollection<Component> instanceComponents)
		{
			foreach (BaseConnector connector in Connectors)
			{
				connector.Connect(instanceComponents);
			}
		}
	}
}
