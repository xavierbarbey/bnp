﻿using System.Collections.Generic;
using System.IO;

namespace DeploymentProcedure.Utility.Base
{
	internal interface IFileSystem
	{
		bool IsLocal { get; }
		string ServerName { get; }
		XmlHelper XmlHelper { get; }

		void CopyDirectory(string sourceDirectoryPath, IFileSystem targetFileSystem, string targetDirectoryPath, bool overwrite = true);
		void CopyFile(string sourceFilePath, IFileSystem targetFileSystem, string targetFilePath, bool overwrite);
		void CreateDirectory(string directoryPath);
		void CreateFile(string filePath);
		void DeleteDirectory(string directoryPath);
		void DeleteFile(string filePath);
		bool DirectoryExists(string directoryPath);
		IEnumerable<string> EnumerateDirectories(string path);
		IEnumerable<string> EnumerateFiles(string path);
		bool FileExists(string filePath);
		string GetFullPath(string path);
		FileStream OpenFile(string filePath);
		void WriteAllTextToFile(string filePath, string content);
	}
}
