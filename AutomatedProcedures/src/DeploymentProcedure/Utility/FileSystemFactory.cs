﻿using DeploymentProcedure.Utility.Base;
using System.Collections.Generic;

namespace DeploymentProcedure.Utility
{
	internal partial class FileSystemFactory
	{
		private static IDictionary<string, IFileSystem> _usedFileSystems = new Dictionary<string, IFileSystem>
		{
			{ FileSystem.LocalServerName, new LocalFileSystem(FileSystem.LocalServerName) }
		};

		internal static IFileSystem Local => _usedFileSystems[FileSystem.LocalServerName];

		internal static IFileSystem GetFileSystem(string serverName)
		{
			if (_usedFileSystems.ContainsKey(serverName))
			{
				return _usedFileSystems[serverName];
			}
			else if (FileSystem.IsLocalMachine(serverName))
			{
				return Local;
			}
			else
			{
				ServerFileSystem serverFileSystem = new ServerFileSystem(serverName);
				_usedFileSystems.Add(serverName, serverFileSystem);
				return serverFileSystem;
			}
		}
	}
}
