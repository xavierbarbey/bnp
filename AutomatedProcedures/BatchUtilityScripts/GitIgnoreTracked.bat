@echo off
powershell "git ls-files -v | Select-String -Pattern '^[a-z]' -CaseSensitive | ForEach-Object { git update-index --no-assume-unchanged $_.Line.Substring(2) }"
powershell "[System.IO.File]::ReadLines('.gitignoretracked') | ForEach-Object { git update-index --assume-unchanged $_ }"
EXIT /b 0