def getCustomEnvironmentVariableName(environmnetVariableName) {
	def nantParametersNamesToEnvironmentVariablesNamesMap = [
		'Innovator.License.Type': 'LicenseType',
		'Innovator.Activation.Key': 'LicenseActivationKey',
		'Innovator.License.Key': 'LicenseKey',
		'Innovator.License.String': env.InnovatorLicenseStringSecretTextId ?: 'InnovatorLicense12',
		'MSSQL.Innovator.Password': env.CustomMssqlInnovatorPasswordCredentialId,
		'MSSQL.Innovator.Regular.Password': env.CustomMssqlInnovatorRegularPasswordCredentialId,
		'MSSQL.SA.Password': env.CustomMssqlSaPasswordCredentialId
	]

	return nantParametersNamesToEnvironmentVariablesNamesMap[environmnetVariableName] ?: environmnetVariableName
}

def getEnvironmentVariableWithCustomMapping(environmnetVariableName) {
	return env[environmnetVariableName] ?: env[getCustomEnvironmentVariableName(environmnetVariableName)]
}

def putCredentialToBindingsIfValid(credential, credentialBindings) {
	def doAdd = false
	def credentialVariableName = credential.variable ?: credential.usernameVariable
	credential.credentialsId = getCustomEnvironmentVariableName(credential.credentialsId)
	try {
		withCredentials([credential]) {
			def credentialVariableValue = getEnvironmentVariableWithCustomMapping(credentialVariableName)
			doAdd = (credentialVariableValue != null) && credentialVariableValue != ''
		}
	}
	catch (org.jenkinsci.plugins.credentialsbinding.impl.CredentialNotFoundException e) {
		doAdd = false
	}
	finally {
		if (doAdd) {
			credentialBindings.add(credential)
		}
	}
	return doAdd
}

nantParameters = [:]
def addNantParameterFromEnvironment(name, defaultValue = null) {
	def nantPropertyValue = getEnvironmentVariableWithCustomMapping(name) ?: defaultValue
	if (nantPropertyValue) {
		addNantParameter(name, nantPropertyValue)
	}
}

def addNantParameter(name, value) {
	nantParameters.put(name, value)
}

def removeNantParameter(name) {
	nantParameters.remove(name)
}

def getNantParametersString() {
	addAllJobParametersAsNantParameters()

	def nantParametersString = ''
	nantParameters.each { name, value ->
		nantParametersString += " -D:${name}=\"${value}\""
	}
	return nantParametersString
}

nantEnvironmentVariables = [:]
def addNantEnvironmentVariableFromEnvironment(name, defaultValue = null) {
	def nantPropertyValue = getEnvironmentVariableWithCustomMapping(name) ?: defaultValue
	if (nantPropertyValue) {
		addNantEnvironmentVariable(name, nantPropertyValue)
	}
}

def addNantEnvironmentVariable(name, value) {
	nantEnvironmentVariables.put(name, value)
}

def removeNantEnvironmentVariable(name) {
	nantEnvironmentVariables.remove(name)
}

def getNantEnvironmentVariableArray() {
	def environmentVariables = []
	nantEnvironmentVariables.each { name, value ->
		environmentVariables.add("${name}=${value}")
	}
	return environmentVariables
}

def addAllJobParametersAsNantParameters() {
	params.each { name, value ->
		if (!nantEnvironmentVariables.containsKey(name)) {
			addNantParameter(name, value)
			addNantEnvironmentVariable(name, value)
		}
	}
}

def runNantTargets(targetList) {
	def nantParametersString = getNantParametersString()
	def environmentVariablesForNant = getNantEnvironmentVariableArray()
	def nantTargetsString = targetList.join(' ')

	withEnv(environmentVariablesForNant) {
		bat """
			call AutomatedProcedures\\BatchUtilityScripts\\SetupExternalTools.bat
			%PathToNantExe% -buildfile:AutomatedProcedures\\NantScript.xml ${nantParametersString} ${nantTargetsString}
		"""
	}
}

return this