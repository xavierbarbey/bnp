// A pipeline script intended to perform deploy on a target environment via Jenkins
def commitPointerToDeploy = env.BranchName ?: env.TagName
def buildSpecificName
def repositoryUrl
def setTag = !new Boolean(env.SkipSetTag)
def setupNewInstance = !new Boolean(env.DoNotSetupNewInstance)
def userEmailForTag
def userNameForTag
def buildAgentNodeLabel = env.CustomBuildAgentNodeLabel ?: 'master'
def deployAgentNodeLabel = env.NodeLabel ?: commitPointerToDeploy
def innovatorLicenseStringSecretTextId = env.InnovatorLicenseStringSecretTextId ?: 'Innovator.License.String'
def gitTagCredentialId = env.GitTagCredentialId ?: 'jenkins-user'

if (!commitPointerToDeploy) {
	error "The BranchName or TagName parameter is mandatory for deployment jobs." + 
		"Go 'Configure'->'This project is parametrised' and add one of these string parameters: BranchName or TagName"
}

if (setTag) {
	if (env.UserEmailForTag && env.UserNameForTag) {
		userEmailForTag = env.UserEmailForTag
		userNameForTag = env.UserNameForTag
	} else {
		error "The UserEmailForTag and UserNameForTag parameters are mandatory for deployment jobs." +
			"They are used to create a git tag" 
			"Go 'Configure'->'This project is parametrised' and add these string parameters"
	}
}

def gitCheckoutTimeout = 60
def jenkinsfileProperties
def jenkinsfileUtilities

def emailExtRecipientProviders
def EmailSuccess
def EmailFailure
def EmailRecipients
def innovatorDeploymentPackageArchiveName

try
{
	node(buildAgentNodeLabel) {
		buildAgentNodeLabel = env.NODE_NAME
		buildSpecificName = "${commitPointerToDeploy}-${env.BUILD_NUMBER}"
		innovatorDeploymentPackageArchiveName = "Deployment-Package-From-${buildSpecificName}.zip"
		emailExtRecipientProviders = (env["EmailExtRecipientProviders"] == null) ? '' : env["EmailExtRecipientProviders"]
		EmailRecipients = (env["EmailSuccess"] == null) ? '' : env["EmailSuccess"]
		EmailFailure = (env["EmailFailure"] == null) ? '' : env["EmailFailure"]
		stage('Prepare Deployment Package') {
			repositoryUrl = scm.getUserRemoteConfigs()[0].getUrl()
			def repositoryName = repositoryUrl.tokenize('/').last()
			def referenceRepository = (env.sandboxRootDir == null) ? [] : [[$class: 'CloneOption', reference: "${env.sandboxRootDir}\\${repositoryName}"]]

			checkout([
				$class: 'GitSCM',
				branches: scm.branches,
				extensions: scm.extensions + referenceRepository,
				userRemoteConfigs: scm.userRemoteConfigs
			])

			jenkinsfileProperties = readJSON file: "AutomatedProcedures/JenkinsfileProperties.json"
			jenkinsfileUtilities = load "AutomatedProcedures/JenkinsScripts/JenkinsfileUtilities.groovy"

			jenkinsfileUtilities.addNantEnvironmentVariable('Commit.Pointer', commitPointerToDeploy)
			jenkinsfileUtilities.addNantEnvironmentVariable('Deployment.Package.Archive.Name', innovatorDeploymentPackageArchiveName)
			jenkinsfileUtilities.addNantParameterFromEnvironment('Perform.Import.Of.SampleData')

			jenkinsfileUtilities.runNantTargets(['CreateZipWithDeploymentPackageAndScripts'])

			stash name: "InnovatorDeploymentPackage", includes:innovatorDeploymentPackageArchiveName
		}
	}
	node(deployAgentNodeLabel) {
		stage('Cleanup Jenkins Workspace') {
			cleanWs notFailBuild: true
		}

		unstash name: "InnovatorDeploymentPackage"
		unzip zipFile: innovatorDeploymentPackageArchiveName

		def credentialBindings = []
		jenkinsfileProperties.credentials.each { credential ->
			jenkinsfileUtilities.putCredentialToBindingsIfValid(credential, credentialBindings)
		}

		def existingFeatureLicensesIDs = []
		// attach feature licence credential IDs specified in the Feature.License.Credential.IDs parameter
		def featureLicenseCredentialIDs = env["Feature.License.Credential.IDs"]?.tokenize('\n;, ') ?: jenkinsfileProperties.featureLicenseCredentialIDs
		featureLicenseCredentialIDs.each { credentialId ->
			def featureLicenseCredential = [$class: "StringBinding", credentialsId: credentialId, variable: credentialId]
			if (jenkinsfileUtilities.putCredentialToBindingsIfValid(featureLicenseCredential, credentialBindings)) {
				existingFeatureLicensesIDs.add(credentialId)
			}
		}

		withCredentials(credentialBindings) {
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Innovator.License.Type', 'Unlimited')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Innovator.License.Company')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Innovator.Activation.Key')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Innovator.License.Key')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Innovator.License.String')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('SMTP.Server')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Last.Commit.Before.Current.Release')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('MSSQL.Innovator.Password', 'innovator')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('MSSQL.Innovator.Regular.Password', 'innovator')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('MSSQL.SA.Password')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('MSSQL.Server')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('MSSQL.Database.Name')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Path.To.CodeTree.Zip')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Path.To.DB.Bak')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Path.To.Sandbox.Directory')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Overwrite.Existing.CodeTree.During.Restoration')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Url.Of.Deployment.Server')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('Port.Of.Deployment.Server')
			jenkinsfileUtilities.addNantEnvironmentVariableFromEnvironment('IIS.Default.SiteName')
			//based on the Name.Of.Innovator.Instance path to Innovator and url will be generated
			jenkinsfileUtilities.addNantEnvironmentVariable('Build.Number', env.BUILD_NUMBER)
			jenkinsfileUtilities.addNantEnvironmentVariable('Path.To.Deployment.Package.Dir', pwd())

			try {
				stage('Setup Innovator') {
					if (setupNewInstance) {
						jenkinsfileUtilities.runNantTargets(['Setup.Innovator.For.Deploy.Task'])
					} else {
						echo "Innovator Setup is skipped, proceed to deploy."
					}
				}
				stage('Deploy') {
					jenkinsfileUtilities.addNantEnvironmentVariable('Feature.License.Credential.IDs', existingFeatureLicensesIDs.join(','));
					jenkinsfileUtilities.runNantTargets(['SetupParameters.For.Deploy.Task', 'Deploy.Package', 'Print.Url.Of.Installed.Innovator'])
				}
			}
			finally {
				stage('Upload deployment package') {
					archiveArtifacts artifacts: '*-Package-From-*.zip', allowEmptyArchive: true
				}
			}
		}
	}
	if (setTag) {
		node(buildAgentNodeLabel) {
			stage('Git tag') {
				if (repositoryUrl.startsWith('https://')) {
					withCredentials([usernamePassword(credentialsId: gitTagCredentialId, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
						bat("git config user.email ${userEmailForTag}")
						bat("git config user.name \"${userNameForTag}\"")
						bat("git tag ${buildSpecificName} -a -m \"Jenkins ${buildSpecificName} build\"")
						bat("git push ${repositoryUrl.replace(/https:\/\/.*@|https:\/\//, "https://${GIT_USERNAME}:${GIT_PASSWORD}@")} ${buildSpecificName}")
					}
				}
				else {
					sshagent([gitTagCredentialId]) {
						bat("git config user.email ${userEmailForTag}")
						bat("git config user.name \"${userNameForTag}\"")
						bat("git tag ${buildSpecificName} -a -m \"Jenkins ${buildSpecificName} build\"")
						bat("git push ${repositoryUrl} ${buildSpecificName}")
					}
				}
			}
		}
	}
	currentBuild.result = 'SUCCESS'
}
finally {
	stage('Send emails') {
		if (currentBuild.result == null) {
			currentBuild.result = 'FAILURE'

			def recipientProviders = []
			if (emailExtRecipientProviders != null && emailExtRecipientProviders != '') {
				def providerNames = emailExtRecipientProviders.split()
				for (i = 0; i < providerNames.size(); i++) {
						recipientProviders.add([$class: providerNames[i]])
				}
			}
			def providedRecipients = recipientProviders.isEmpty() ? '' : emailextrecipients(recipientProviders)
			EmailRecipients = "${EmailFailure ?: ''} ${providedRecipients}".trim()
		}
		if (EmailRecipients) {
			emailext body: '$DEFAULT_CONTENT',subject: '[JENKINS] ' + '$DEFAULT_SUBJECT', to: EmailRecipients
		}
	}
}