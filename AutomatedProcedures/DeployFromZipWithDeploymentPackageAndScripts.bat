@echo off

echo DeployFromZipArchiveWithPackageAndScripts.bat:
echo Target audience: Deployment engineer, QA team;
echo Purpose: Deploy upgrade to the target server from zip with diff

SET NAntTargetsToRun=Deploy.Package
SET PathToThisBatFileFolder=%~dp0
CALL "%PathToThisBatFileFolder%BatchUtilityScripts\SetupExternalTools.bat
IF errorlevel 1 GOTO END

CALL:ConfirmUpdate "Confirm that you sure you want to update Innovator code tree? [Y] Yes [N] No" Do.Deploy.Innovator.Application -D:Do.Deploy.Innovator.Application
CALL:ConfirmUpdate "Confirm that you sure you want to update Innovator database? [Y] Yes [N] No" Do.Deploy.Innovator.Database -D:Do.Deploy.Innovator.Database
CALL:ConfirmUpdate "Confirm that you sure you want to update Vault Server code tree? [Y] Yes [N] No" Do.Deploy.Vault.Server -D:Do.Deploy.Vault.Server
CALL:ConfirmUpdate "Confirm that you sure you want to update Agent Service code tree? [Y] Yes [N] No" Do.Deploy.Agent.Service -D:Do.Deploy.Agent.Service
CALL:ConfirmUpdate "Confirm that you sure you want to update Conversion Server code tree? [Y] Yes [N] No" Do.Deploy.Conversion.Server -D:Do.Deploy.Conversion.Server
CALL:ConfirmUpdate "Confirm that you sure you want to update OAuth Server code tree? [Y] Yes [N] No" Do.Deploy.OAuth.Server -D:Do.Deploy.OAuth.Server
CALL:ConfirmUpdate "Confirm that you sure you want to update Self Service Reporting code tree? [Y] Yes [N] No" Do.Deploy.Self.Service.Report -D:Do.Deploy.Self.Service.Report

"%PathToNantExe%" ^
	"/f:%PathToThisBatFileFolder%NantScript.xml" ^
	%NAntTargetsToRun% ^
	%Do.Deploy.Innovator.Application% ^
	%Do.Deploy.Innovator.Database% ^
	%Do.Deploy.Vault.Server% ^
	%Do.Deploy.Agent.Service% ^
	%Do.Deploy.Conversion.Server% ^
	%Do.Deploy.OAuth.Server% ^
	%Do.Deploy.Self.Service.Report% ^
	"-D:Path.To.Deployment.Package.Dir=%PathToThisBatFileFolder%.."

if not errorlevel 1 (
	powershell write-host -foregroundcolor green "EVERYTHING WAS DEPLOYED SUCCESSFULLY!!!"
)

pause
goto :eof

:ConfirmUpdate
CHOICE /T 15 /N /d N /M %1
IF errorlevel 2 (
	REM If selected 'no' - evaluate nant parameter to skip update
	SET %2="%3=false"
	GOTO :eof
)
REM If selected 'yes' - set empty string, so nant will not skip update
IF errorlevel 1 SET %2=
GOTO :eof