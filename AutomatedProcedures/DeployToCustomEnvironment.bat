@echo off
SET Custom.Path.To.Installed.Innovator="T:"
SET Custom.Url.Of.Installed.Innovator="http://prod-server/InnovatorServer_Prod"
SET Custom.MSSQL.Server="(local)"
SET Custom.MSSQL.Database.Name="ARAS_PLM_PROD"
SET Custom.Agent.Service.Name="ArasInnovatorAgent_AgentService_innovatorServer"
SET Custom.Agent.Service.Host.Name="prod-server"

SET NAntParameters=-D:Path.To.Installed.Innovator=%Custom.Path.To.Installed.Innovator%
SET NAntParameters=%NAntParameters% -D:Url.Of.Installed.Innovator=%Custom.Url.Of.Installed.Innovator%
SET NAntParameters=%NAntParameters% -D:MSSQL.Database.Name=%Custom.MSSQL.Database.Name%
SET NAntParameters=%NAntParameters% -D:MSSQL.Server=%Custom.MSSQL.Server%
SET NAntParameters=%NAntParameters% -D:Agent.Service.Name=%Custom.Agent.Service.Name%
SET NAntParameters=%NAntParameters% -D:Agent.Service.Host.Name=%Custom.Agent.Service.Host.Name%

Call Deploy.bat %NAntParameters%