<%@ Page  CodePage="65001" explicit="true" Inherits="Aras.Web.Server.Authenticate" Language="C#" strict="true" EnableSessionState="false" %>
<%--
	Despite the fact, that EnableSessionState="false", simple request to this endpoint will still create Session
	if HttpSessionStateConfigurationModule is used. To avoid creation of sessions requests to this endpoint need to
	contain header "Aras-Set-HttpSessionState-Behavior: switch_to_initial"
--%>