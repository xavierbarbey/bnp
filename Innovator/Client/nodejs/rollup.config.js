﻿/* eslint-disable no-console */
/*
example of command:
cls && rollup -c --file-input ../Modules/core/index.js --file-output ../jsBundles/core.js --name core --silent
*/
const path = require('path');
const rollupBabel = require('rollup-plugin-babel');
const rollupUglify = require('rollup-plugin-uglify');
const babelConfig = require('./babel.config');
const uglifyConfig = require('./uglify.config');
const jsCompileHelper = require('./jsCompileHelper');

const fileNameInput = process.argv[4];
const pathFileNameOutput = process.argv[6];
const name = process.argv[8];

module.exports = {
	input: fileNameInput,
	external: ['inferno'],
	plugins: [
		rollupBabel(babelConfig),
		rollupUglify.uglify(uglifyConfig),
		{
			generateBundle(...args) {
				if (pathFileNameOutput) {
					console.log(path.join(process.cwd(), pathFileNameOutput));
					console.log(jsCompileHelper.getDependenciesFiles(...args).join('\n'));
				}
			}
		}
	],
	output: [
		{
			name: name,
			format: 'iife',
			file: pathFileNameOutput,
			globals: {
				'inferno': 'Inferno'
			},
			sourcemap: true
		},
	],
};
