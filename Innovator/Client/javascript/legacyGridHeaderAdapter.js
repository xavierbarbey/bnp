﻿(function() {
	const listSet = new Set();
	listSet.add('list');
	listSet.add('filter list');
	listSet.add('color list');
	listSet.add('mv_list');

	const adaptGridHeader = function(itemType, userPreferencesItem) {
		const userPreferences = ArasModules.xmlToJson(userPreferencesItem.xml).Item;

		const itemTypeVisibleProps = aras.getvisiblePropsForItemType(itemType);
		const xPropertiesPath = 'Relationships/Item[@type=\'xItemTypeAllowedProperty\' and not(inactive=\'1\')]/related_id/Item[@type=\'xPropertyDefinition\']';
		const xProperties = Array.from(itemType.selectNodes(xPropertiesPath));
		const visibleProps = itemTypeVisibleProps.concat(xProperties);

		const headList = getHeadLists(visibleProps);
		const isRelationshipsGrid = (searchLocation === 'Relationships Grid');
		const columnsOrder = userPreferences['col_order'].split(';');
		const columnsWidth = userPreferences['col_widths'].split(';');

		const widthMap = columnsOrder.reduce(function(acc, columnName, index) {
			acc.set(columnName, parseInt(columnsWidth[index]));

			return acc;
		}, new Map());

		const gridHeaderInfo = buildHead(visibleProps, isRelationshipsGrid, headList, widthMap);
		gridHeaderInfo.indexHead = columnsOrder.filter(function(columnName) {
			return widthMap.get(columnName) > 0;
		});

		return gridHeaderInfo;
	};

	const buildHead = function(visibleProperties, isRelshipGrid, lists, widthMap) {
		const getLabel = function(label, property) {
			const labelWithPostfix = label + ' [...]';

			const dataType = getValue(property.data_type);
			switch (dataType) {
				case 'item': {
					const dataSource = getValue(property.data_source);
					return dataSource ? labelWithPostfix : label;
				}
				case 'date':
				case 'text':
				case 'image':
				case 'formatted text':
				case 'color': {
					return labelWithPostfix;
				}
			}
			return label;
		};
		const getEditableType = function(dataType, dataSource, isForeign) {
			switch (dataType) {
				case 'date': {
					return 'dateTime';
				}
				case 'item': {
					if (aras.getItemTypeName(dataSource) === 'File') {
						return 'File';
					}
					return isForeign ? 'FIELD' : 'InputHelper';
				}
				case 'list':
				case 'filter list':
				case 'color list': {
					return 'FilterComboBox';
				}
				case 'mv_list': {
					return 'CheckedMultiSelect';
				}
				default: {
					return 'FIELD';
				}
			}
		};
		const getClasses = function(dataType, dataSource, isForeign) {
			const isItem = (dataType === 'item'); // could be property of foreign item
			const isFile = (aras.getItemTypeName(dataSource) === 'File'); // could be property of foreign item

			return isItem && !isFile && !isForeign ? 'InputHelper' : '';
		};
		const getSortFormatLocaleInfo = function(dataType, property) {
			const resultObject = {
				locale: null,
				inputformat: undefined,
				sort: null
			};
			const locale = aras.getSessionContextLocale();
			switch (dataType) {
				case 'date': {
					const defaultPattern = 'MM/dd/yyyy';
					const format = aras.getDotNetDatePattern(getValue(property.pattern)) || defaultPattern;

					resultObject.sort = 'DATE';
					resultObject.inputformat = format;
					resultObject.locale = locale;
					break;
				}
				case 'decimal': {
					const format = aras.getDecimalPattern(
						getValue(property.prec),
						getValue(property.scale)
					);

					resultObject.sort = 'NUMERIC';
					resultObject.inputformat = format || null;
					resultObject.locale = locale;
					break;
				}
				case 'integer':
				case 'float': {
					resultObject.sort = 'NUMERIC';
					resultObject.locale = locale;
					break;
				}
				case 'ubigint':
				case 'global_version': {
					resultObject.sort = 'UBIGINT';
					resultObject.locale = locale;
					break;
				}
			}
			return resultObject;
		};

		const getDataSourceName = function(dataType, property) {
			const dataSource = getValue(property.data_source);
			return (dataType === 'item' && dataSource) ? aras.getItemTypeName(dataSource) : null;
		};

		const getSearchType = function(dataType, dataSource, isForeign, name) {
			switch (dataType) {
				case 'date': {
					return 'date';
				}
				case 'item': {
					return (!isForeign && aras.getItemTypeName(dataSource) !== 'File') ? 'singular' : '';
				}
				case 'list':
				case 'filter list':
				case 'color list': {
					return 'filterList';
				}
				case 'mv_list': {
					return 'multiValueList';
				}
			}
			return (name === 'classification') ? 'classification' : '';
		};

		const getListsData = function(dataType, dataSource, lists) {
			if (listSet.has(dataType)) {
				return lists[dataSource];
			}
			return {
				options: [],
				optionsLabels: []
			};
		};

		const getClaimedByColumnData = function() {
			return {
				label: '',
				field: 'L',
				styles: 'text-align:center;',
				headerStyles: 'text-align:center;',
				columnCssStyles: {
					'text-align': 'center'
				},
				cellType: Aras.Client.Controls.Experimental.TypeEditCell,
				options: [
					{
						value: '',
						icon: '',
						label: 'Clear Criteria'
					},
					{
						value: '<img src="../images/ClaimOn.svg" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" />',
						icon: 'svg-claimon',
						label: 'Claimed By Me'
					},
					{
						value: '<img src="../images/ClaimOther.svg" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" />',
						icon: 'svg-claimother',
						label: 'Claimed By Others'
					},
					{
						value: '<img src="../images/ClaimAnyone.svg" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" />',
						icon: 'svg-claimanyone',
						label: 'Claimed By Anyone'
					}
				],
				optionsLables: [
					'<span style="padding: 0 22px;"> Clear Criteria</span>',
					'<img src="../images/ClaimOn.svg" align="left" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" /> Claimed By Me',
					'<img src="../images/ClaimOther.svg" align="left" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" /> ' +
						'Claimed By Others',
					'<img src="../images/ClaimAnyone.svg" align="left" style="margin-right: 4px; height: auto; width: auto; max-width: 20px; max-height: 20px;" /> ' +
						'Claimed By Anyone'
				],
				editable: false,
				editableType: 'dropDownButton',
				searchType: 'dropDownIcon',
				icon: '../images/ClaimColumn.svg',
				sort: null,
				locale: null,
				layoutIndex: 0,
				width: 32,
				classes: '',
				bginvert: null,
				dataSourceName: null
			};
		};

		const headMap = new Map();
		const columnsOrder = [];

		if (widthMap.has('L')) {
			columnsOrder.push('L');
			headMap.set('L', getClaimedByColumnData());
		}

		const head = visibleProperties.reduce(function(acc, xmlProperty, index) {
			const property = ArasModules.xmlToJson(xmlProperty);

			const name = getValue(property.name);
			const label = getValue(property.label);
			const headerLabel = getLabel(label || name, property);
			const fieldKey = name + (isRelshipGrid ? '_R' : '_D');
			const width = widthMap.get(fieldKey) || 100;
			const columnAlign = getValue(property.column_alignment) || 'left';
			const styles = 'text-align:' + columnAlign + ';';

			// we have to save isForeign value since on the next line property we are using can be changed to property of source item
			const isForeign = getValue(property.data_type) === 'foreign';
			const sourceProperty = isForeign ?
				ArasModules.xmlToJson(aras.uiMergeForeignPropertyWithSource(xmlProperty, true)) :
				property;

			const dataType = getValue(sourceProperty.data_type);
			const dataSource = getValue(sourceProperty.data_source);
			const editableType = getEditableType(dataType, dataSource, isForeign);
			const listsData = getListsData(dataType, dataSource, lists);

			const sortFormatLocaleObject = getSortFormatLocaleInfo(dataType, sourceProperty);

			const header = {
				field: fieldKey,
				label: headerLabel,
				styles: styles,
				columnCssStyles: {
					'text-align': columnAlign
				},
				width: width,
				layoutIndex: (index + 1),
				headerStyles: 'text-align:center;',

				sort: sortFormatLocaleObject.sort,
				inputformat: sortFormatLocaleObject.inputformat,
				locale: sortFormatLocaleObject.locale,

				cellType: Aras.Client.Controls.Experimental.TypeEditCell,
				editable: false, // L column is never editable; can be true for relship grid
				icon: null, // '../images/ClaimColumn.svg' for L column
				classes: getClasses(dataType, dataSource, isForeign), // 'InputHelper' for editable
				editableType: editableType,
				bginvert: null,
				dataSourceName: getDataSourceName(dataType, sourceProperty),
				searchType: getSearchType(dataType, dataSource, isForeign),
				options: listsData.options,
				optionsLables: listsData.optionsLabels // legacy typo!
			};

			acc.set(fieldKey, header);
			columnsOrder.push(fieldKey);

			return acc;
		}, headMap);

		return {
			headMap: head,
			columnsOrder: columnsOrder
		};
	};

	const getValue = function(property) {
		return typeof property === 'object' ? property['@value'] : property;
	};

	const getHeadLists = function(visibleProperties) {
		const requiredLists = [];

		visibleProperties.forEach(function(xmlProperty) {
			const property = ArasModules.xmlToJson(xmlProperty);
			const isForeign = getValue(property.data_type) === 'foreign';
			const sourceProperty = isForeign ?
				ArasModules.xmlToJson(aras.uiMergeForeignPropertyWithSource(xmlProperty, true)) :
				property;
			const dataType = getValue(sourceProperty.data_type);
			if (listSet.has(dataType)) {
				const dataSource = getValue(sourceProperty.data_source);
				requiredLists.push({
					id: dataSource,
					relType: (dataType === 'filter list') ? 'Filter Value' : 'Value'
				});
			}
		});

		const xmlLists = aras.getSeveralListsValues(requiredLists);
		const lists = Object.keys(xmlLists).reduce(function(acc, listId) {
			const listNodes = xmlLists[listId];
			const values = [''];
			const labels = [''];
			listNodes.forEach(function(listNode) {
				const value = aras.getItemProperty(listNode, 'value');
				const label = aras.getItemProperty(listNode, 'label') || value;
				values.push(value);
				labels.push(label);
			});
			acc[listId] = {
				options: values,
				optionsLabels: labels
			};
			return acc;
		}, {});

		return lists;
	};

	window.adaptGridHeader = adaptGridHeader;

})();
