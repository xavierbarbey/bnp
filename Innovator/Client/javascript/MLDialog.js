﻿var kEnter = 13;
var kEsc = 27;
var toolbar;

document.onkeydown = function processKey(e) {
	var cmd;
	e = e || window.event;
	if (e.keyCode == kEsc) {
		cmd = 'exit';
	}
	if (e.keyCode == kEnter) {
		cmd = 'ready_btn';
	}
	if (cmd) {
		document.activeElement.blur();
		parent.focus();
		setTimeout(function() {
			processCommand(cmd);
		}, 0);
	}
	return true;
};

onload = function onloadHdlr() {
	var div = document.createElement('div');
	div.setAttribute('id', 'toolbar_container');
	div.setAttribute('position', 'absolute');
	div.setAttribute('style', 'width:100%; height222:30px;');
	document.body.appendChild(div);
	clientControlsFactory.createControl('Aras.Client.Controls.Public.ToolBar', {id: 'top_toolbar', connectId: 'toolbar_container'}, function(control) {
		toolbar = control;
		clientControlsFactory.on(toolbar, {
			'onClick': onToolbarButtonClick
		});
		loadToolbar();
		document.querySelector('input').focus();
	});
};

function setControlEnabled(ctrlName, b) {
	b = b === undefined ? true : Boolean(b);
	var activeToolbar = toolbar.getActiveToolbar();
	var tbi = activeToolbar.getItem(ctrlName);
	if (tbi) {
		tbi.setEnabled(b);
	}
}

function loadToolbar() {
	var s = parent.aras.getI18NXMLResource('mldialog_toolbar.xml');
	toolbar.loadXml(s);
	toolbar.show();
	if (!document.isEditMode) {
		var arr = parent.aras.newArray('ready_btn', 'set_nothing');
		for (var i = 0; i < arr.length; i++) {
			setControlEnabled(arr[i], false);
		}
	}
}

function onToolbarButtonClick(btn) {
	processCommand(btn.getId());
}

function processCommand(cmdId) {
	var returnData;

	switch (cmdId) {
		case 'ready_btn':
			returnData = document.item.xml;
			break;
		case 'set_nothing':
			returnData = null;
			break;
	}

	parent.dialogArguments.dialog.close(returnData);
}
