﻿function initializeItemsGridCommandsFunctions(container) {
	container.onInitialize = function() {
		return ItemTypeGrid.onInitialize();
	};

	container.onReinitialize = function(itemTN, itemTID, _savedSearchId) {
		previewPane.clearForm();

		if (itemTypeID == itemTID && savedSearchId == _savedSearchId) {
			return;
		}

		if (columnSelectionMediator) {
			columnSelectionMediator.closeColumnSelectionWindow();
			columnSelectionMediator.closeXClassBarWindow();
		}

		stopSearch(false);
		saveSetups(true);

		itemTypeID = itemTID;
		savedSearchId = _savedSearchId;
		ItemTypeGrid = MainGridFactory.Create(itemTN);

		if (container.onInitialize()) {
			InitPropertiesContainer();
			container.initSearch();
			container.reinitXClassBar();
		}
	};

	container.initSearch = function() {
		searchReady = false;
		initToolbar();
		setupPageNumber();
		setupGrid(true);

		if (searchContainer) {
			searchContainer.removeIFramesCollection();
		}
		const searchToolbar = document.querySelector('#searchview-toolbars .aras-commandbar');
		searchContainer = new SearchContainer(
			itemTypeName,
			null,
			grid,
			null,
			searchLocation,
			document.getElementById('searchPlaceholder'),
			undefined,
			searchToolbar
		);
		searchContainer.initSearchContainer(true);
		searchContainer.onStartSearchContainer();

		if (savedSearchId) {
			searchContainer._onSavedSearchChange(savedSearchId);
		}
		searchReady = true;
		// run autosearch if required
		if (aras.getItemProperty(currItemType, 'auto_search') == '1' || Boolean(savedSearchId) || Boolean(autoSearch)) {
			const statusId = aras.showStatusMessage('status', aras.getResource('', 'itemsgrid.populating_grid_with_it_items', itemTypeLabel));
			const searchResult = doSearch();

			aras.clearStatusMessage(statusId);
			return searchResult;
		}
	};

	container.initColumnSelectionBlock = function() {
		const xClassBarNode = document.getElementById('xClassBarPlaceholder');
		columnSelectionMediator = ColumnSelectionMediatorFactory.CreateBaseMediator(xClassBarNode);
		columnSelectionControl.initResources();
		xClassSearchWrapper.initResources();
	};

	container.reinitXClassBar = function() {
		const xClassBarNode = document.getElementById('xClassBarPlaceholder');
		columnSelectionMediator.xClassBarWrapper = new XClassBar(itemTypeName, xClassBarNode);
	};

	container.startCellEditIG = function(rowId, field, cleanIfNeed) {
		startCellEditCommon.call(this, rowId, field);
	};

	container.applyCellEditIG = function(rowId, field, cleanIfNeed) {
		if ('input_row' == rowId && searchReady) {
			removeFilterListValueIfNeed(rowId, field);
		}

		applyCellEditCommon.call(this, rowId, field);
	};

	container.showReleaseEffectiveDateRows = function(isShow) {
		if (!document.getElementById('release_date_row') || !document.getElementById('effective_date_row')) {
			return;
		}

		document.getElementById('release_date_row').style.display = isShow ? '' : 'none';
		document.getElementById('effective_date_row').style.display = isShow ? '' : 'none';
	};

	container.generateXML4Item = function(itemNode) {
		const itemType = itemNode.getAttribute('type');
		const tmpDom = createEmptyResultDom();
		let gridXml;
		let tableNode;
		let nodes;
		let currentNode;
		let i;

		currentNode = tmpDom.selectSingleNode(aras.XPathResult());
		currentNode.appendChild(itemNode.cloneNode(true));

		const columnObjects = aras.uiPrepareDOM4XSLT(tmpDom, itemTypeID);
		const params = {
			only_rows: true,
			columnObjects: columnObjects
		};
		gridXml = aras.uiGenerateItemsGridXML(tmpDom, visiblePropNds, itemTypeID, params);
		tmpDom.loadXML(gridXml);

		tableNode = tmpDom.documentElement;
		nodes = tmpDom.selectNodes('/table/*[local-name(.)!=\'tr\']');

		for (i = 0; i < nodes.length; i++) {
			currentNode = nodes[i];
			tableNode.removeChild(currentNode);
		}

		if (itemType == 'RelationshipType') {
			const tdNodes = tmpDom.selectNodes('/table/tr/td');

			const correctNames = function(element, param, cell) {
				if (element != null && element.selectSingleNode('keyed_name') != null) {
					cell.firstChild.data = element.selectSingleNode('keyed_name').text;
				} else if (itemNode.selectSingleNode(param) != null && itemNode.selectSingleNode(param).text != '') {
					let res = aras.getItemById(element, itemNode.selectSingleNode(param).text, 0, '', 'keyed_name');

					if (!res) {
						res = aras.getItemById('ItemType', itemNode.selectSingleNode(param).text, 0, '', 'keyed_name');
					}

					if (res) {
						cell.firstChild.data = res.selectSingleNode('keyed_name').text;
					}
				}
			};

			correctNames(itemNode.selectSingleNode('related_id/Item'), 'related_id', tdNodes[4]);
			correctNames(itemNode.selectSingleNode('source_id/Item'), 'source_id', tdNodes[3]);
		}

		return tmpDom;
	};

	container.updateGridCell = function(cell, tdNode) {
		if (cell && tdNode) {
			const textColor = tdNode.getAttribute('textColor');
			const itemLink = tdNode.getAttribute('link');
			const bgColor = tdNode.getAttribute('bgColor');
			const font = tdNode.getAttribute('font');

			if (textColor) {
				try {
					cell.setTextColor(textColor);
				} catch (excep) {}
			}

			if (itemLink) {
				cell.setLink(itemLink);
			}

			if (bgColor) {
				try {
					cell.setBgColor_Experimental(bgColor);
				} catch (excep) {}
			}

			if (font) {
				cell.setFont(font);
			}

			cell.setValue(tdNode.text);
		}
	};

	container.updateItemInQueryCache = function(itemNode) {
		const queryItem = currQryItem.getResult();
		const oldItem = queryItem.selectSingleNode('Item[@id="' + itemNode.getAttribute('id') + '"]');

		if (oldItem) {
			queryItem.replaceChild(itemNode.cloneNode(true), oldItem);
		} else {
			queryItem.appendChild(itemNode.cloneNode(true));
		}
	};

	container.insertRow = function(itemNode, skipMenuUpdate) {
		if (itemNode && itemNode.getAttribute('type') == itemTypeName) {
			let generatedDom;

			container.updateItemInQueryCache(itemNode);
			generatedDom = container.generateXML4Item(itemNode);

			xml_ready_flag = false;
			addRowInProgress_Number++;
			grid.addXMLRows(generatedDom.xml);

			return true;
		}

		return false;
	};

	container.checkBeforeUpdateRow = function() {
		const columnWidths = aras.getPreferenceItemProperty('Core_ItemGridLayout', itemTypeID, 'col_widths', null);

		if (!columnWidths) {
			container.onInitialize();
			container.initSearch();

			return false;
		}

		return true;
	};

	container.updateRow = function(itemNode, skipMenuUpdate) {
		if (itemNode && container.checkBeforeUpdateRow()) {
			const itemId = itemNode.getAttribute('id');

			if (grid.getRowIndex(itemId) == -1) {
				return container.insertRow(itemNode, skipMenuUpdate);
			} else {
				const tmpDom = container.generateXML4Item(itemNode);
				const tdNodes = tmpDom.selectNodes('/table/tr/td');
				let tdNode;
				let gridCell;
				let i;

				container.updateItemInQueryCache(itemNode);

				const visibleColumnCount = grid._grid.settings.indexHead.length;
				for (i = 0; i < visibleColumnCount; i++) {
					tdNode = tdNodes[i];

					gridCell = grid.cells(itemId, i);
					container.updateGridCell(gridCell, tdNode);
				}

				if (itemId == grid.getSelectedId()) {
					container.onSelectItem(itemId, undefined, skipMenuUpdate);
				}

				previewPane.updateForm(itemId, itemTypeName);
				return true;
			}
		}

		return false;
	};

	container.deleteRow = function(deleteTarget, skipQuery) {
		if (deleteTarget) {
			let itemId;

			if (typeof (deleteTarget) == 'string') {
				itemId = deleteTarget;
			} else {
				if (deleteTarget.getAttribute('type') == itemTypeName) {
					itemId = deleteTarget.getAttribute('id');
				} else {
					return false;
				}
			}

			if (itemId == grid.getSelectedId()) {
				aras.uiPopulateInfoTableWithItem(null, document);
			}

			grid.deleteRow(itemId);
			previewPane.clearForm(itemId);
			if (!skipQuery) {
				const itemNode = currQryItem.getResult().selectSingleNode('Item[@id="' + itemId + '"]');

				if (itemNode) {
					// If we delete versionable item, all previous versions should be removed to.
					// IR-008031 "The previous version is shown after manual version".
					const config_id = aras.getItemProperty(itemNode, 'config_id');

					if (isVersionableIT && config_id) {
						const nodesToDelete = currQryItem.getResult().selectNodes('Item[@type="' + itemTypeName + '"][config_id="' + config_id + '"]');
						let i;

						for (i = 0; i < nodesToDelete.length; i++) {
							nodesToDelete[i].parentNode.removeChild(nodesToDelete[i]);
						}
					} else {
						itemNode.parentNode.removeChild(itemNode);
					}
				}
			}
		} else {
			aras.uiPopulateInfoTableWithItem(null, document);
			return false;
		}
	};

	container.updateRowSearchGrids = function(itemNode) {
		return container.syncGrids('updateRow', itemNode);
	};

	container.deleteRowSearchGrids = function(deleteTarget) {
		if (!deleteTarget) {
			aras.uiPopulateInfoTableWithItem(null, document);
			return false;
		}
		return container.syncGrids('deleteRow', deleteTarget);
	};

	container.insertRowSearchGrids = function(itemNode) {
		return container.syncGrids('insertRow', itemNode);
	};

	container.syncGrids = function(syncFunction, itemNode) {
		if (!itemNode) {
			return false;
		}
		const topWin = aras.getMainWindow();
		const itemsGridArray = topWin.arasTabs.getSearchGridTabs(window.itemTypeID);
		const changedGrids = itemsGridArray.filter(function(itemsGrid) {
			return itemsGrid[syncFunction](itemNode);
		});

		return changedGrids.length > 0;
	};

	container.emptyGrid = function() {
		container.deleteRow(grid.getSelectedId(), true);
		grid.RemoveAllRows();
	};

	container.setFrozenColumns = function() {
		if (this._grid.view.defaultSettings.freezableColumns) {
			const frozenColumns = aras.getPreferenceItemProperty('Core_ItemGridLayout', itemTypeID, 'frozen_columns', '1');
			grid._grid.settings.frozenColumns = parseInt(frozenColumns, 10);
		}
	};

	container.onXmlLoaded = function() {
		if (addRowInProgress_Number == 0) {
			// jscs:disable
			with (aras) { // jshint ignore:line
				// jscs:enable
				if (!sGridsSetups[itemTypeName].selectedRows) {
					sGridsSetups[itemTypeName].selectedRows = [];
				}

				var selectedRows = sGridsSetups[itemTypeName].selectedRows;
				var rowToSelId = '';
				if (grid.getRowCount() > 0) {
					var currSelRows = [];

					if (selectedRows.length == 0) {
						rowToSelId = grid.getRowId(0);
						currSelRows[0] = rowToSelId;
					} else {
						var highestRow = 1000000;
						var rowId;
						var rowIndex;
						var i;

						for (i = 0; i < selectedRows.length; i++) {
							rowId = selectedRows[i];
							rowIndex = grid.getRowIndex(rowId);

							if (rowIndex > -1) {
								if (rowToSelId == '' || rowIndex < highestRow) {
									highestRow = rowIndex;
									rowToSelId = rowId;
								}

								currSelRows[currSelRows.length] = rowId;
								grid.setSelectedRow(rowId, true, false);
							}
						}

						if (currSelRows.length == 0) {
							rowToSelId = grid.getRowId(0);
							currSelRows[0] = rowToSelId;
							grid.setSelectedRow(rowToSelId, true, false);
						}
					}
					grid.setSelectedRow(rowToSelId, true, true);
					sGridsSetups[itemTypeName].selectedRows = currSelRows;

					container.onSelectItem(rowToSelId);
				} else {
					container.updateInfoTableWithItem(rowToSelId);
				}
			}
		} else {
			addRowInProgress_Number--;
			if (addRowInProgress_Number == 0) {
				if (callbackF_afterAddRow) {
					callbackF_afterAddRow();
				}
			}
		}

		xml_ready_flag = true;
	};

	container.updateInfoTableWithItem = function(rowId) {
		const currentQueryItemResult = currQryItem.getResult();
		const sourceItem = currentQueryItemResult.selectSingleNode('Item[@id="' + rowId + '"]');

		aras.uiPopulateInfoTableWithItem(sourceItem, document, function(soapBody, callback) {
			asyncController.sendRequest(rowId, 'updateInfoTableWithItem', soapBody, callback);
		}, function(soapBody, callback) {
			asyncController.sendRequest(rowId, 'requestLCStateHanlder', soapBody, callback);
		});

		if (columnSelectionMediator) {
			columnSelectionMediator.updateXClassBar();
		}
	};

	container.onSelectItem = function(rowId, col, notupdateMenu, isGridEvent) {
		const idsArray = grid.getSelectedItemIds();
		const rowIsNotSelected = (idsArray.indexOf(rowId) === -1);

		aras.sGridsSetups[itemTypeName].selectedRows = idsArray;
		if (idsArray.indexOf(rowId) >= 0) {
			container.updateInfoTableWithItem(rowId);
			container.onClickRow(rowId);
		} else {
			container.updateInfoTableWithItem(idsArray[0]);
			container.onClickRow(idsArray[0] || rowId);
		}

		if (rowIsNotSelected && !isGridEvent) {
			grid.setSelectedRow(rowId, true, false);
		}

		if (!notupdateMenu) {
			container.setMenuState(rowId, col);
		}

		columnSelectionMediator.updateXClassBar();
	};

	container.setMenuState = function(rowId, col) {
		ItemTypeGrid.setMenuState(rowId, col);
	};

	container.initItemMenu = function(rowId) {
		ItemTypeGrid.initItemMenu(rowId);
	};

	container.onDoubleClick = function(rowId, columnIndex, altMode) {
		ItemTypeGrid.onDoubleClick(rowId, altMode);
	};

	container.onClickRow = function(rowId) {
		if (previewPane.getType() !== 'Form') {
			return;
		}

		if (ItemTypeGrid.onClick && rowId) {
			ItemTypeGrid.onClick(rowId);
		} else {
			previewPane.clearForm();
		}
	};

	container.fillPopupMenu = function(rowId, col) {
		ItemTypeGrid.fillPopupMenu(rowId, col);
	};

	container.onHeaderCellContextMenu = function(e) {
		return topWnd.cui.onGridHeaderContextMenu(e, grid, true);
	};

	container.onHeaderContextMenu = function(e) {
		return topWnd.cui.onGridHeaderContextMenu(e, grid);
	};

	container.hideColumn = function(col) {
		grid.SetColumnVisible(col, false);
	};

	container.showColumn = function(col) {
		const colOrderArr = grid.getLogicalColumnOrder().split(';');
		const propsToShow = [];
		let propertyName;
		let propertyLabel;
		let propertyWidth;
		let columnName;
		let i;
		let j;

		for (i = 0; i < colOrderArr.length; i++) {
			if (grid.getColWidth(i) == 0) {
				propertyLabel = '';
				propertyWidth = 100;
				columnName = grid.GetColumnName(i);

				if (columnName === 'L') {
					propertyLabel = aras.getResource('', 'common.claimed');
					propertyWidth = 32;
				} else {
					propertyName = columnName.substr(0, columnName.length - 2);

					for (j = 0; j < visiblePropNds.length; j++) {
						if (aras.getItemProperty(visiblePropNds[j], 'name') == propertyName) {
							const tempWidth = parseInt(aras.getItemProperty(visiblePropNds[j], 'column_width'));

							propertyLabel = aras.getItemProperty(visiblePropNds[j], 'label') || propertyName;

							if (!isNaN(tempWidth)) {
								propertyWidth = tempWidth;
							}
							break;
						}
					}
				}
				propsToShow.push({colNumber: i, label: propertyLabel, width: propertyWidth});
			}
		}

		if (propsToShow.length) {
			window.parent.ArasModules.Dialog.show('iframe', {
				title: aras.getResource('', 'showcolumndlg.title'),
				aras: aras,
				propsToShow: propsToShow,
				dialogWidth: 350,
				dialogHeight: 500,
				resizable: true,
				content: 'SitePreference/showColumnDialog.html'
			}).promise.then(function(resultArray) {
				if (resultArray) {
					for (let j = 0; j < resultArray.length; j++) {
						for (let i = 0; i < propsToShow.length; i++) {
							if (propsToShow[i].label === resultArray[j]) {
								grid.SetColumnVisible(propsToShow[i].colNumber, true, propsToShow[i].width);
								break;
							}
						}
					}
				}
			});
		} else {
			aras.AlertError(aras.getResource('', 'itemsgrid.no_additional_columns_available'));
		}
	};

	container.onMenuClicked = function(commandId, rowId, col) {
		ItemTypeGrid.onMenuClicked(commandId, rowId, col);
	};

	container.onLink = function(typeName, id, altMode) {
		ItemTypeGrid.onLink(typeName, id, altMode);
	};

	container.GetDatePattern = function(queryType) {
		let propertyName;
		let datePattern;

		switch (queryType) {
			case 'Released':
				propertyName = 'release_date';
				break;
			case 'Effective':
				propertyName = 'effective_date';
				break;
			default:
				propertyName = 'modified_on';
				break;
		}

		const selector = 'Relationships/Item[@type=\'Property\' and name=\'' + propertyName + '\']';
		const pattern = aras.getItemProperty(
			currItemType.selectSingleNode(selector),
			'pattern'
		);
		datePattern = pattern || 'short_date_time';
		return aras.getDotNetDatePattern(datePattern);
	};

	container.execInTearOffWin = function(itemId, commandId, param) {
		if (commandId) {
			const itemWindow = aras.uiFindWindowEx(itemId);
			let execResult = null;

			if (itemWindow) {
				if (!aras.isWindowClosed(itemWindow)) {
					if (itemWindow.name != 'work') {
						const tearoff_menu = itemWindow.tearOffMenuController;

						aras.browserHelper.setFocus(itemWindow);

						if (tearoff_menu && tearoff_menu.onClickMenuItem) {
							execResult = tearoff_menu.onClickMenuItem(commandId, param);

							if (!execResult || !execResult.result) {
								execResult = true;
							}
						}
					}
				} else {
					aras.uiUnregWindowEx(itemId);
				}

				if (execResult == null) {
					execResult = true;
				}
			}

			return execResult;
		}

		return false;
	};

	container.onNewCommand = function() {
		if (itemTypeName === 'File') {
			parent.ArasModules.vault.selectFile().then(function(item) {
				const fileNode = aras.newItem('File', item);
				aras.uiShowItemEx(fileNode, 'new');
				aras.itemsCache.addItem(fileNode);
				container.insertRowSearchGrids(fileNode);
			});
		} else {
			const itemType = aras.getItemTypeForClient(itemTypeName).node;

			if (aras.isPolymorphic(itemType) && !window.showModalDialog) {
				aras.newItem(itemTypeName).then(function(node) {
					if (node) {
						aras.itemsCache.addItem(node);
						aras.uiShowItemEx(node, 'new');
						container.insertRowSearchGrids(node);
					}
				});
			} else {
				const newItem = aras.uiNewItemEx(itemTypeName);

				if (newItem) {
					container.insertRowSearchGrids(newItem);
				}
			}
		}
	};

	container.onViewCommand = function() {
		const itemId = grid.getSelectedId();

		if (itemId) {
			if (!container.execInTearOffWin(itemId, 'view')) {
				const itemNode = aras.getItemById(itemTypeName, itemId, 0);

				if (itemNode) {
					aras.uiShowItemEx(itemNode);
				} else {
					aras.AlertError(aras.getResource('', 'itemsgrid.failed2get_itemtype', itemTypeLabel));
					return false;
				}
			}

			return true;
		} else {
			aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
			return false;
		}
	};

	container.onEditCommand = function() {
		const itemId = grid.getSelectedId();

		return ItemTypeGrid.onEditCommand(itemId);
	};

	container.onSaveCommand = function() {
		let itemNode;
		const itemTypeName = window.itemTypeName;
		const isVersionableIT = window.isVersionableIT;
		const updateGridAfterSave = function(oldItemId, saveResult) {
			if (!saveResult || itemTypeName !== window.itemTypeName) {
				return;
			}
			let newId = oldItemId;
			if (isVersionableIT) {
				itemNode = aras.getItemLastVersion(itemTypeName, oldItemId);

				if (!itemNode) {
					return;
				}
				newId = itemNode.getAttribute('id');
			}

			itemNode = aras.getItemById(itemTypeName, newId, 0);
			if (itemNode) {
				if (isVersionableIT) {
					const oldItem = currQryItem.getResult().selectSingleNode('Item[@id="' + oldItemId + '"]');
					container.deleteRowSearchGrids(oldItem);
				}

				if (container.updateRowSearchGrids(itemNode)) {
					container.onSelectItem(newId);
				}
			}
		};
		const itemIds = grid.getSelectedItemIds();

		if (!itemIds.length) {
			aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
			return Promise.resolve(false);
		}

		let itemId;
		let i;
		let itemsSavingChain = Promise.resolve();
		for (i = 0; i < itemIds.length; i++) {
			itemId = itemIds[i];

			if (!container.execInTearOffWin(itemId, 'save')) {
				itemNode = aras.getItemById('', itemId, 0);

				if (itemNode && (aras.isTempEx(itemNode) || aras.isDirtyEx(itemNode))) {
					if (itemTypeName == 'Form') {
						itemNode.setAttribute('levels', 3);
					}
					itemsSavingChain = itemsSavingChain.then(aras.saveItemExAsync.bind(aras, itemNode)).then(updateGridAfterSave.bind(null, itemId));
				}
			}
		}

		itemsSavingChain.then(function() {
			if (itemTypeName !== window.itemTypeName) {
				return;
			}
			switch (itemTypeName) {
				case 'ItemType':
					topWnd.updateTree(itemIds);
					break;
				case 'Preference':
					topWnd.mainLayout.observer.notify('UpdatePreferences');
					break;
			}

			if (itemIds.length === 1) {
				aras.uiReShowItemEx(itemId, itemNode);
			}
		});
	};

	container.onPurgeCommand = function() {
		return container.onPurgeDeleteCommand('purge');
	};

	container.onDeleteCommand = function() {
		return container.onPurgeDeleteCommand('delete');
	};

	container.onPurgeDeleteCommand_executeCommand = function(command, itemId, itemNode, unselectedIds) {
		const res = command(itemTypeName, itemId, true);
		if (!res) {
			return;
		}

		container.deleteRowSearchGrids(itemNode);
		if (grid.getRowIndex(itemId) > -1) {
			grid.setSelectedRow(itemId, true, false);
			unselectedIds.push(itemId);
		}
	};

	container.onPurgeDeleteCommand_updateUiAfterPurgeDelete = function(itemIds, unselectedIds) {
		if (itemTypeName === 'ItemType') {
			topWnd.updateTree(itemIds);
		}
		topWnd.mainLayout.updateCuiLayoutOnItemChange(itemTypeName);
		if (unselectedIds.length > 0) {
			for (let i = 1; i < unselectedIds.length; i++) {
				grid.setSelectedRow(unselectedIds[i], true, false);
			}

			grid.setSelectedRow(unselectedIds[0], true, true);

			if (itemIds.length > 1) {
				container.onSelectItem(unselectedIds[0]);
			}
		} else {
			const rowCount = grid.getRowCount();
			if (rowCount > 0) {
				const rowIndex = grid.getRowIndex(itemIds[0]);
				let idToSelect = grid.getRowId(0);

				if (rowIndex > -1) {
					idToSelect = (rowCount - 1 >= rowIndex) ? grid.getRowId(rowIndex) : grid.getRowId(rowCount - 1);
				}

				grid.setSelectedRow(idToSelect, false, true);
				container.onSelectItem(idToSelect);
			} else {
				setupGrid(false);
			}
		}
	};

	container.onPurgeDeleteCommand = function(commandId) {
		const itemIds = grid.getSelectedItemIds();

		if (itemIds.length === 0) {
			aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
			return false;
		}

		// binding is necessary, otherwise context will be lost
		const targetCommand = (commandId === 'purge') ? aras.purgeItem.bind(aras) : aras.deleteItem.bind(aras);

		const clientCache = currQryItem.getResult();
		if (!clientCache) {
			return;
		}

		const unselIds = [];
		let continueExecutionSilently = false;
		let skipDialog = false;

		let indexFor = 0;
		const nextFor = function() {
			indexFor++;
			if (indexFor < itemIds.length) {
				startExecute(itemIds[indexFor], itemIds, indexFor);
			} else {
				container.onPurgeDeleteCommand_updateUiAfterPurgeDelete(itemIds, unselIds);
			}
		};

		var startExecute = function(itemId, array, index) {
			const itemNode = clientCache.selectSingleNode('Item[@id="' + itemId + '"]');
			if (!itemNode) {
				container.deleteRowSearchGrids(itemId);
				nextFor();
				return;
			}

			const tearOffAnsw = container.execInTearOffWin(itemId, commandId);
			if (tearOffAnsw) {
				if (tearOffAnsw.result === 'Deleted') {
					container.deleteRow(itemNode);
					focus();
				}
				nextFor();
				return;
			}

			if (skipDialog) {
				if (continueExecutionSilently) {
					container.onPurgeDeleteCommand_executeCommand(targetCommand, itemId, itemNode, unselIds);
				} else {
					grid.setSelectedRow(itemId, true, false);
					unselIds.push(itemId);
				}
				nextFor();
				return;
			}

			const messageId = (commandId === 'purge') ? 'itemsgrid.purge_confirmation' : 'itemsgrid.delete_confirmation';
			const message = aras.getResource('', messageId, itemTypeLabel, aras.getKeyedNameEx(itemNode));
			if ((array.length - 1) === index) {
				const confirmDialogParams = {
					buttons: {
						btnYes: aras.getResource('', 'common.ok'),
						btnCancel: aras.getResource('', 'common.cancel')
					},
					defaultButton: 'btnCancel',
					aras: aras,
					dialogWidth: 300,
					dialogHeight: 200,
					center: true,
					content: 'groupChgsDialog.html',
					message: message
				};
				window.parent.ArasModules.Dialog.show('iframe', confirmDialogParams).promise.then(function(res) {
					if (res === 'btnYes') {
						container.onPurgeDeleteCommand_executeCommand(targetCommand, itemId, itemNode, unselIds);
					}
					nextFor();
				});
			} else {
				const dialogParams = {
					aras: aras,
					buttons: {
						'btnYes': aras.getResource('', 'itemsgrid.purgedlg_yes'),
						'btnYes4All': aras.getResource('', 'itemsgrid.purgedlg_yes_for_all'),
						'btnSkip': aras.getResource('', 'itemsgrid.purgedlg_skip'),
						'btnCancel': aras.getResource('', 'itemsgrid.purgedlg_cancel')
					},
					defaultButton: 'btnCancel',
					dialogWidth: 400,
					dialogHeight: 200,
					message: message,
					content: 'groupChgsDialog.html'
				};
				window.parent.ArasModules.Dialog.show('iframe', dialogParams).promise.then(function(res) {
					switch (res) {
						case 'btnYes4All': {
							container.onPurgeDeleteCommand_executeCommand(targetCommand, itemId, itemNode, unselIds);
							skipDialog = true;
							continueExecutionSilently = true;
							break;
						}
						case 'btnYes': {
							container.onPurgeDeleteCommand_executeCommand(targetCommand, itemId, itemNode, unselIds);
							break;
						}
						case 'btnSkip': {
							break;
						}
						default: {
							grid.setSelectedRow(itemId, true, false);
							unselIds.push(itemId);
							skipDialog = true;
						}
					}
					nextFor();
				});
			}
		};
		startExecute(itemIds[indexFor], itemIds, indexFor);
	};

	container.onSaveAsCommand = function() {
		const itemId = grid.selection_Experimental.get('id');

		if (!itemId) {
			aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
			return false;
		}

		if (container.execInTearOffWin(itemId, 'saveAs')) {
			return true;
		} else {
			const copiedItem = aras.copyItem(itemTypeName, itemId);

			if (copiedItem) {
				const rowId = copiedItem.getAttribute('id');

				if (itemTypeName == 'ItemType') {
					topWnd.updateTree(itemId.split(';'));
				}
				grid.selection_Experimental.clear();

				callbackF_afterAddRow = function() {
					callbackF_afterAddRow = null;
					container.onSelectItem(rowId);

					if ('File' !== itemTypeName) {
						container.onEditCommand();
					}
				};

				container.insertRowSearchGrids(copiedItem);
			} else {
				return false;
			}
		}
	};

	container.onPrintCommand = function() {
		topWnd.aras.showStatusMessage('status', 'Generation pdf file', '../images/Progress.gif');
		function printGrid(pdf) {
			// return array of object about header (header label, widht)
			function getHeaderData() {
				const headerData = [];
				this.grid._grid.settings.indexHead.forEach(function(columnName) {
					const columnIndex = this.grid.getColumnIndex(columnName);

					headerData.push({
						label: grid.getHeaderCol(columnIndex),
						width: grid.getColWidth(columnIndex) + 'px'
					});
				});
				return headerData;
			}

			// return array of object about search bar (search label, widget class name)
			function getSearchBarData() {
				const searchBarData = [];
				this.grid._grid.settings.indexHead.forEach(function(columnName) {
					searchBarData.push({
						label: grid._grid.head.get(columnName, 'searchValue'),
						widget: 'dijit.form.TextBox'
					});
				});
				return searchBarData;
			}

			// fetch items in grid, if items have not been loaded
			function fetchItems() {
				let result = [];
				const onComplete = function(items) {
					result = items;
				};
				grid.grid_Experimental.store.fetch({
					start: 0,
					count: grid.getRowCount(),
					sort: grid.grid_Experimental.getSortProps(),
					onComplete: onComplete
				});
				return result;
			}

			// return array of records Ids stored in grid
			function fetchRecordsIds(items) {
				const result = [];
				let i;
				const length = items.length;
				for (i = 0; i < length; i += 1) {
					result.push(grid.grid_Experimental.store.getIdentity(items[i]));
				}
				return result;
			}

			// forced load items to grid
			function fetchAllItems() {
				grid.grid_Experimental._clearData();
				grid.grid_Experimental.store.fetch({
					start: 0,
					count: grid.getRowCount(),
					sort: grid.grid_Experimental.getSortProps(),
					onComplete: grid.grid_Experimental._onFetchComplete
				});
			}

			// return array of objects about all rows in grid
			function getRowData() {
				const data = [];
				const selectedRowIds = grid.getSelectedItemIds(';').split(';');
				let records = [];
				let itemsExist = true;

				// load records Ids (used if records have not been loaded)
				function loadRecordsIds() {
					itemsExist = false;
					const items = fetchItems();
					records = fetchRecordsIds(items);
					fetchAllItems();
				}

				// if grid does not contain rows, then we are loading them
				if (!grid.getRowId(grid.getRowCount() - 1)) {
					loadRecordsIds();
				}

				for (let j = 0; j < grid.getRowCount(); j++) {
					var rowId = itemsExist ? grid.getRowId(j) : records[j];
					if (!rowId) { // if grid does not contain rows, then we are loading them
						loadRecordsIds();
						rowId = records[j];
					}
					var rowData = [];

					this.grid._grid.settings.indexHead.forEach(function(columnName) { // jshint ignore:line
						const columnIndex = this.grid.getColumnIndex(columnName);
						const cell = grid.cells_Experimental(rowId, columnIndex, true);
						if (cell) {
							rowData.push({
								label: cell.getText(),
								link: this.grid._grid.rows.get(rowId, columnName + 'link'),
								align: cell.getHorAlign(),
								style: cell.getCellStyle(),
								selected: selectedRowIds.indexOf(rowId) > -1 ? true : false
							});
						}
					});
					data.push(rowData);
				}
				return data;
			}

			// method calculate how many records can be print on one page of pdf document
			// depending on format paper and orientation
			function getRecordsPerPage(doc, format, orientation, pdf) {
				const pageFormat = doc.getPageFormat(format);
				const pageHeight = orientation === 'p' ? pageFormat[1] : pageFormat[0];
				const margin = pdf.getMargin();

				const headerHeight = margin + pdf.getHeaderHeight() + (grid._grid.view.defaultSettings.search ? pdf.getSearchBarHeight() : 0);
				const totalRecordsHeight = pageHeight - headerHeight - margin;
				const oneRecordHeight = pdf.getRowHeight();

				const recordsPerPage = Math.floor(totalRecordsHeight / oneRecordHeight);
				return recordsPerPage;
			}

			// start of printing
			return function(jsPDF) {
				// jscs:disable
				const doc = new jsPDF('l', 'pt', 'a4', true);
				// jscs:enable
				const langDir = dojoConfig.arasContext.languageDirection;
				doc.setLangDir(langDir);
				const recordsPerPage = getRecordsPerPage(doc, 'a4', 'l', pdf);
				const pageCount = Math.ceil(grid.getRowCount() / recordsPerPage) || 1;

				// searhbar can be invisible, so we don't need to print it
				const printSearchBar = grid.isInputRowVisible();

				// print header
				const headerInfo = pdf.printHeader(getHeaderData(), doc, pageCount);
				headerInfo.printSearchBar = printSearchBar;

				// print searhBar
				if (printSearchBar) {
					pdf.printSearchBar(getSearchBarData(), headerInfo, doc, pageCount);
				}

				// print rows
				pdf.printGridData(getRowData(), headerInfo, doc, recordsPerPage);

				// save to file
				doc.save(itemTypeName ? itemTypeName + 'Grid.pdf' : 'print_result.pdf');
				topWnd.aras.clearStatusMessage('status');
			};
		}
		require(['Printing/Scripts/Classes/JsPdfLoader', 'Printing/Scripts/Classes/DataGridToPdf'], function(loader, DataGridToPdf) {
			loader.loadScripts(printGrid(DataGridToPdf), ['jspdf.plugin.text.js', 'jspdf.plugin.bidi.js', 'jspdf.plugin.graphics.js']);
		});
	};

	container.getRealItemTypeNames = function(itemIdsOrId) {
		let itemIds = itemIdsOrId;
		const resultNames = {};
		let itemNode;
		let items;
		let idList;
		let response;
		let i;

		if (typeof (itemIds) === 'string') {
			itemIds = [itemIds];
		}
		if (!itemIds.length) {
			throw new Error(1, 'Item ids are not specified');
		}

		if (aras.isPolymorphic(currItemType)) {
			idList = itemIds.join(',');

			if (!idList) {
				throw new Error(1, 'IDs list is empty');
			} else {
				response = aras.soapSend('ApplyItem', '<Item type=\'' + itemTypeName + '\' action=\'get\' select=\'itemtype\' idlist=\'' + idList + '\'/>');

				if (response.getFaultCode() != 0) {
					aras.AlertError(response);
					return resultNames;
				}

				items = response.getResult().selectNodes('Item');
				for (i = 0; i < items.length; i++) {
					itemNode = items[i];
					resultNames[itemNode.getAttribute('id')] = aras.getItemTypeName(aras.getItemProperty(itemNode, 'itemtype'));
				}
			}
		} else {
			for (i = 0; i < itemIds.length; i++) {
				resultNames[itemIds[i]] = itemTypeName;
			}
		}

		return resultNames;
	};

	container.onLockCommand = function(ignorePolymophicWarning, itemIDs) {
		return ItemTypeGrid.onLockCommand(ignorePolymophicWarning, itemIDs);
	};

	container.onUnlockCommand = function(ignorePolymophicWarning, itemIDs) {
		return ItemTypeGrid.onUnlockCommand(ignorePolymophicWarning, itemIDs);
	};

	container.onRevisionsCommand = function() {
		const itemId = grid.getSelectedId();

		if (!itemId) {
			aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
			return false;
		}

		if (container.execInTearOffWin(itemId, 'revisions')) {
			return true;
		} else {
			const minDialogWidth = 500;
			const maxDialogWidth = document.getElementById('grid_table').offsetWidth - document.getElementById('itemProperties').offsetWidth;
			let width = maxDialogWidth;
			const colWidths = aras.getPreferenceItemProperty('Core_ItemGridLayout', itemTypeID, 'col_widths', null);
			const dialogParams = {
				aras: aras,
				itemID: itemId,
				itemTypeName: itemTypeName,
				dialogWidth: (width < minDialogWidth) ? minDialogWidth : ((width > maxDialogWidth) ? maxDialogWidth : width),
				resizable: true,
				title: aras.getResource('', 'revisiondlg.item_versions'),
				type: 'RevisionsDialog'
			};

			if (colWidths) {
				const widthsArray = colWidths.split(';');
				let i;

				width = 0;
				for (i = 0; i < widthsArray.length; i++) {
					width += parseInt(widthsArray[i]);
				}
			}
			window.parent.ArasModules.Dialog.show('iframe', dialogParams);
		}
	};

	container.onUndoCommand = function() {
		const itemIDs = topWnd.work.grid.getSelectedItemIds();
		if (itemIDs.length === 0) {
			aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
			return;
		}

		const dialogParams = {
			aras: aras,
			dialogWidth: 400,
			dialogHeight: 200,
			center: true,
			buttons: {
				btnYes: aras.getResource('', 'itemsgrid.undodlg.yes'),
				btnYes4All: aras.getResource('', 'itemsgrid.undodlg.yes4all'),
				btnSkip: aras.getResource('', 'itemsgrid.undodlg.skip'),
				btnCancel: aras.getResource('', 'itemsgrid.undodlg.cancel')
			},
			defaultButton: 'btnSkip',
		};
		let yes4All = false;
		let index = 0;
		const underCommand = function(itemId) {
			aras.removeFromCache(itemId);
			const itemNode = aras.getItemById(itemTypeName, itemId, 0);

			if (!(itemNode && container.updateRow(itemNode) === false)) {
				index++;
				nextFor();
			}
		};
		var nextFor = function() {
			if (index < itemIDs.length) {
				const itemId = itemIDs[index];
				const itemNode = aras.getFromCache(itemId);

				if (itemNode) {
					if (container.execInTearOffWin(itemId, 'undo')) {
						index++;
						nextFor();
					}

					if (!yes4All) {
						const confirmMessage = aras.getResource('', 'itemsgrid.undo_will_discard_changes', itemTypeLabel, aras.getKeyedNameEx(itemNode));
						if (index === itemIDs.length - 1) {
							topWnd.aras.confirm(confirmMessage, window.parent,
								function(res) {
									if (res === 'btnYes') {
										underCommand(itemId);
									} else {
										container.onSelectItem(itemIDs[0]);
									}
								}
							);
						} else {
							dialogParams.message = confirmMessage;
							dialogParams.content = 'groupChgsDialog.html';
							window.parent.ArasModules.Dialog.show('iframe', dialogParams).promise.then(function(res) {
								if (res === 'btnYes4All') {
									yes4All = true;
									underCommand(itemId);
								} else if (res === 'btnYes') {
									window.setTimeout(function() {
										underCommand(itemId);
									}, 0);
								} else if (res === 'btnSkip') {
									index++;
									window.setTimeout(function() {
										nextFor();
									}, 0);
								} else {
									container.onSelectItem(itemIDs[0]);
								}
							});
						}
					} else {
						underCommand(itemId);
					}
				}
			} else {
				container.onSelectItem(itemIDs[0]);
			}
		};
		nextFor();
	};

	container.onOpenCommand = function() {
		if (itemTypeName == 'File') {
			const itemID = grid.getSelectedId();

			if (!itemID) {
				aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
				return false;
			}

			if (container.execInTearOffWin(itemID, 'open')) {
				return true;
			} else {
				const file = aras.getItemById('File', itemID, 0);

				if (file) {
					aras.uiShowItemEx(file, 'openFile');
				}
			}
		} else {
			return false;
		}
	};

	container.onDownloadCommand = function() {
		if (itemTypeName === 'File') {
			const itemIDs = grid.getSelectedItemIds();

			if (!itemIDs || !itemIDs.length) {
				aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
				return false;
			}

			for (let i = 0; i < itemIDs.length; i++) {
				const file = aras.getItemById('File', itemIDs[i], 0);
				if (file) {
					aras.downloadFile(file);
				}
			}

			return true;
		}
		return false;
	};

	container.onPromoteCommand = function() {
		const itemIds = grid.getSelectedItemIds();
		switch (itemIds.length) {
			case 0:
				aras.AlertError(aras.getResource('', 'itemsgrid.select_item_type_first', itemTypeLabel));
				return;
			case 1:
				singlePromote(itemIds[0]);
				return;
			default:
				ItemTypeGrid.onMassPromote(itemIds);
		}

		function singlePromote(itemId) {
			const queryItem = currQryItem.getResult();
			let itemNode;

			itemNode = aras.getFromCache(itemId) || queryItem.selectSingleNode('Item[@id="' + itemId + '"]');
			if (itemNode) {
				if (container.execInTearOffWin(itemId, 'promote')) {
					return;
				} else {
					window.parent.ArasModules.Dialog.show('iframe', {
						title: aras.getResource('', 'promotedlg.propmote', aras.getKeyedNameEx(itemNode)),
						item: itemNode,
						aras: aras,
						dialogHeight: 300,
						dialogWidth: 400,
						content: 'promoteDialog.html',
						resizable: true
					}).promise.then(function(res) {
						if (typeof (res) == 'string' && res == 'null') {
							return;
						}

						if (!res) {
							return false;
						}
						if (isVersionableIT) {
							const lastItemVersion = aras.getItemLastVersion(itemTypeName, itemId);
							const oldId = itemId;

							if (lastItemVersion) {
								itemId = lastItemVersion.getAttribute('id');

								if (oldId != itemId) {
									container.deleteRowSearchGrids(itemNode);
									res = lastItemVersion;
								}
							}
						}

						if (container.updateRowSearchGrids(res) !== false) {
							container.onSelectItem(itemId);
						}
					});
				}
			}
		}
	};

	container.onCopy2clipboardCommand = function() {
		const itemIds = topWnd.work.grid.getSelectedItemIds();
		const resultList = [];
		let itemId;
		let copiedItem;
		let i;

		for (i = 0; i < itemIds.length; i++) {
			itemId = itemIds[i];
			copiedItem = aras.copyRelationship(itemTypeName, itemId);

			if (copiedItem) {
				resultList.push(copiedItem);
			} else {
				aras.AlertError(aras.getResource('', 'itemsgrid.failed2get_itemtype_with_id', itemTypeLabel, itemId));
			}
		}

		aras.clipboard.copy(resultList);
		container.onSelectItem(itemIDs[0]);
	};

	container.onPasteCommand = function() {
		const itemIds = topWnd.work.grid.getSelectedItemIds();
		const itemArr = aras.clipboard.paste();
		let itemNode;
		let i;
		let j;

		for (i = 0; i < itemIds.length; i++) {
			itemNode = aras.getItemById(itemTypeName, itemIds[i]);

			if (!itemNode) {
				aras.AlertError(aras.getResource('', 'itemsgrid.failed2get_itemtype_with_id', itemTypeLabel, itemIds[i]));
				return;
			}

			for (j = 0; j < itemArr.length; j++) {
				if (!aras.pasteRelationship(itemNode, itemArr[j])) {
					aras.AlertError(aras.getResource('', 'itemsgrid.pasting_failed'));
					return;
				}
			}
			itemNode.setAttribute('isDirty', '1');

			if (container.updateRow(itemNode) === false) {
				return;
			}
		}

		aras.AlertSuccess(aras.getResource('', 'itemsgrid.pasting_success'));
	};

	container.onPaste_specialCommand = function() {
		const itemIds = topWnd.work.grid.getSelectedItemIds();
		const itemsList = [];
		let dialogParams;
		let result;
		let itemNode;
		let i;
		let j;

		for (i = 0; i < itemIds.length; i++) {
			itemNode = aras.getItemById(itemTypeName, itemIds[i]);

			if (!itemNode) {
				aras.AlertError(aras.getResource('', 'itemsgrid.failed2get_itemtype_with_id', itemTypeLabel, itemIds[i]));
				continue;
			}

			itemsList.push(itemNode);
		}

		dialogParams = {
			title: aras.getResource('', 'clipboardmanager.clipboard_manager'),
			aras: aras,
			dialogWidth: 700,
			dialogHeight: 450,
			itemsArr: itemsList,
			srcItemTypeId: itemTypeID,
			content: 'ClipboardManager.html'
		};

		window.parent.ArasModules.Dialog.show('iframe', dialogParams).promise.then(function(result) {
			if (result && result.ids) {
				const clipboardItems = aras.clipboard.clItems;
				let clipboardItem;

				for (i = 0; i < result.ids.length; i++) {
					for (j = 0; j < itemsList.length; j++) {
						itemNode = itemsList[j];
						clipboardItem = clipboardItems[result.ids[i]];

						if (!aras.pasteRelationship(itemNode, clipboardItem, result.as_is, result.as_new)) {
							aras.AlertError(aras.getResource('', 'itemsgrid.pasting_failed'));
							return;
						}

						itemNode.setAttribute('isDirty', '1');
						if (container.updateRow(itemNode) === false) {
							return;
						}
					}
				}
				aras.AlertSuccess(aras.getResource('', 'itemsgrid.pasting_success'));
			}
			container.onSelectItem(itemIds[0]);
		});
	};

	container.onShow_clipboardCommand = function() {
		const itemIds = topWnd.work.grid.getSelectedItemIds();

		window.parent.ArasModules.Dialog.show('iframe', {
			title: aras.getResource('', 'clipboardmanager.clipboard_manager'),
			aras: aras,
			content: 'ClipboardManager.html',
			dialogWidth: 700,
			dialogHeight: 450
		}).promise.then(function() {
			container.onSelectItem(itemIds[0]);
		});
	};

	container.InitPropertiesContainer = function() {
		const propertiesTr = document.getElementById('itemProperties');

		if (propertiesTr && currItemType) {
			propertiesTr.lastElementChild.innerHTML = aras.uiDrawItemInfoTable4ItemsGrid(currItemType, ItemTypeGrid.propertiesHelper);
		}
	};
}
