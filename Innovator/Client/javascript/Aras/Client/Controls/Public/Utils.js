﻿function Utils(isIE, utils, fakeMethodHandler) {
	/// <summary>
	/// "aras.utils" instance of the class can be used in custom JavaScript code.
	/// Provides a set of useful methods.
	/// </summary>

	this.utils = utils;
	this.isIE = isIE;
	this.systemInfo = (utils && utils.systemInfo) ? {currentTimeZoneName: this.utils.systemInfo.currentTimeZoneName} : {currentTimeZoneName: new UtilsTimezoneHelper().getTimezoneNameFromJs()};

	this.init = function Utils_init() {
		var doFakeUtils = false, fakeUtilsMethod;
		if (!this.utils) {
			doFakeUtils = true;
			this.utils = {};
			fakeUtilsMethod = function() {
				if (fakeMethodHandler) {
					fakeMethodHandler();
				}
			};
		}

		for (var funcName in this) {
			var obj = this[funcName];
			var firstChar = funcName.charAt(0);

			if (typeof(obj) == 'function' && !this.hasOwnProperty(funcName) && firstChar.toLowerCase() == firstChar) {
				var newFuncName = firstChar.toUpperCase() + funcName.slice(1);
				this[newFuncName] = obj;
				if (doFakeUtils) {
					this.utils[newFuncName] = this.utils[funcName] = fakeUtilsMethod;
				}
			}
		}

		if (doFakeUtils) {
			this.utils.createXmlHttpRequestManager = this.utils.CreateXmlHttpRequestManager = function() {
				return null;
			};

			this.utils.stopHookingMouseInputInScript = this.utils.StopHookingMouseInputInScript = function() {};
			this.utils.startHookingMouseInputInScript = this.utils.StartHookingMouseInputInScript = function() {};
			this.utils.isClipboardSupported = function() {
				//it's the only way to check whether a browser supports clipboard operations or not
				//for those browsers that do not support clipboard operations the queryCommandEnabled throws an exception
				try {
					document.queryCommandEnabled('copy');
					return true;
				} catch (e) {
					return false;
				}
			};
			this.utils.setClipboardData = function(dataType, value, aWindow) {
				aWindow = aWindow || window;
				if (dataType === 'Text') {
					var textArea = aWindow.document.createElement('textarea');
					aWindow.document.body.appendChild(textArea);
					textArea.style.position = 'absolute';
					textArea.style.left = '-9999px';
					textArea.value = value;
					aWindow.getSelection().removeAllRanges();
					textArea.select();
					aWindow.document.execCommand('copy');
					aWindow.getSelection().removeAllRanges();
					textArea.parentNode.removeChild(textArea);
				}
			};

			if (this.isIE) {
				this.utils.setClipboardData = function(dataType, value) {
					window.clipboardData.setData(dataType, value);
				};

				this.utils.getClipboardData = function(dataType) {
					return window.clipboardData.getData(dataType) || '';
				};
			}
		}
	};
}

// <editor-fold defaultstate="collapsed" desc="Common utils methods">

Utils.prototype.setClipboardData = function Utils_setClipboardData(dataType, value, aWindow) {
	/// <summary>
	/// Set content, thats stored in clipboard
	/// </summary>
	this.utils.setClipboardData(dataType, value, aWindow);
};

Utils.prototype.getClipboardData = function Utils_getClipboardData(dataType) {
	/// <summary>
	/// Returns html content, thats stored in clipboard
	/// </summary>
	/// <returns>object</returns>
	if (!dataType) {
		dataType = 'Text';
	}
	return this.utils.getClipboardData('Text');
};

Utils.prototype.isClipboardSupported = function Utils_isClipboardSupported() {
	/// <summary>
	/// Checks if clipboard operations are supported 
	/// </summary>
	return this.utils.isClipboardSupported();
};

Utils.prototype.printPreviewDocument = function() {
	/// <summary>
	/// Print preview of HTMLDocument.
	/// Is not supported.
	/// </summary>
	throw new Error('The method "printPreviewDocument" from Utils API is not supported');
};

Utils.prototype.isValueNumber = function() {
	/// <summary>
	/// If val string represents a number (decimal or double) in the specified locale then true is returned.
	/// Is not supported.
	/// </summary>
	throw new Error('The method "isValueNumber" from Utils API is not supported');
};

Utils.prototype.hideKeyboardInput = function() {
	/// <summary>
	/// Hides/Unhides the key's hit in the window.
	/// Is not supported.
	/// </summary>
	throw new Error('The method "hideKeyboardInput" from Utils API is not supported');
};

Utils.prototype.createXmlHttpRequestManager = function Utils_createXmlHttpRequestManager() {
	/// <summary>
	/// Creates instance of XmlHttpRequestManager
	/// </summary>
	/// <returns></returns>
	return new XmlHttpRequestManager(this.utils);
};

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Only IE utils methods">

Utils.prototype.createShellDataObject = function() {
	/// <summary>
	/// For Internet Explorer only. Create ShellDataObject instance
	/// Is not supported.
	/// </summary>
	throw new Error('The method "createShellDataObject" from Utils API is not supported');
};

Utils.prototype.isWindowVisible = function() {
	/// <summary>
	/// For Internet Explorer only.
	/// Is to fix incorrect behavior of window.closed in IE.
	/// Is not supported.
	/// </summary>
	throw new Error('The method "isWindowVisible" from Utils API is not supported');
};

Utils.prototype.setBrowserWindowStyle = function() {
	/// <summary>
	/// For Internet Explorer only. Sets the style of the browser window.
	/// Is not supported.
	/// </summary>
	throw new Error('The method "setBrowserWindowStyle" from Utils API is not supported');
};

Utils.prototype.closeRootWindow = function() {
	/// <summary>
	/// Closes the root window of the childWindow
	/// Is not supported.
	/// </summary>
	throw new Error('The method "closeRootWindow" from Utils API is not supported');
};

Utils.prototype.stopHookingMouseInputInScript = function Utils_stopHookingMouseInputInScript(window, name) {
	/// <summary>
	/// Stops hooking the mouse hit in the window.
	/// </summary>
	/// <param name="window" type="object">DHTML window object</param>
	/// <param name="name" type="string">Name of the HTML object for which to stop hooking</param>
	if (this.isIE) {
		this.utils.stopHookingMouseInputInScript(window, name);
	}
};

Utils.prototype.startHookingMouseInputInScript = function Utils_startHookingMouseInputInScript(doHide, window, name, checkFunctionInScript) {
	/// <summary>
	/// Starts hooking the mouse hit in the window.
	/// </summary>
	/// <param name="doHide" type="bool">flag to hide/unhide</param>
	/// <param name="window" type="object">DHTML window object</param>
	/// <param name="name" type="string">Name of the HTML object for which to start hooking</param>
	/// <param name="checkFunctionInScript" type="object">Script function object</param>
	if (this.isIE) {
		this.utils.startHookingMouseInputInScript(doHide, window, name, checkFunctionInScript);
	}
};

Utils.prototype.toHideMouseInputInScript = function() {
	/// <summary>
	/// For Internet Explorer only. Hides/Unhides the mouse hit.
	/// Is not supported.
	/// </summary>
	throw new Error('The method "toHideMouseInputInScript" from Utils API is not supported');
};

Utils.prototype.createSplashScreen = function() {
	/// <summary>
	/// For Internet Explorer only. Creates new splashscreen and return reference to it.
	/// Is not supported.
	/// </summary>
	throw new Error('The method "createSplashScreen" from Utils API is not supported');
};

Utils.prototype.openIEWindowInNewProcess = function Utils_openIEWindowInNewProcess(sURL, sName, sFeatures, bReplace) {
	/// <summary>
	/// OpenIEWindowInNewProcess is the analogue of the window.open()
	/// </summary>
	/// <param name="sURL" type="string">String that specifies the URL of the document to display</param>
	/// <param name="sName" type="string">String that specifies the name of the window. String is ignored when used as TARGET</param>
	/// <param name="sFeatures" type="string">String that contains a list of items separated by commas. Each item consists of an option and a value, separated by an equals sign</param>
	/// <param name="bReplace" type="string">Ignorable, just to save window.open analogue</param>
	/// <returns>window</returns>
	return window.open(sURL, sName, sFeatures, bReplace);
};

// </editor-fold>
/*@cc_on
@if (@register_classes == 1)
Type.registerNamespace("Aras.Client.Controls.Public");
Aras.Client.Controls.Public.Utils = Utils;
Utils.registerClass("Aras.Client.Controls.Public.Utils");
@end
@*/