﻿(function() {
	function BaseDateTimeFormatInfo() {
		this.defaultLocale = 'en-us';
		this.ISOPattern = /yyyy-MM-ddTHH:mm:ss(\.SSS)?/;

		Object.defineProperty(this, 'tzInfo', {
			get: function() {
				var topWnd = TopWindowHelper.getMostTopWindowWithAras(window);
				return topWnd.aras.browserHelper.tzInfo;
			}
		});

		Object.defineProperty(this, '_dateLocale', {
			get: function() {
				return dojo.require('dojo.date.locale');
			}
		});

		Object.defineProperty(this, '_dateStamp', {
			get: function() {
				return dojo.require('dojo.date.stamp');
			}
		});
	}

	var _formatCharMappingArray = [
		//http://cldr.unicode.org/translation/date-time-patterns
		{key: 'z', value: 'Z'},
		{key: 'F', value: 's'},
		{key: 'K', value: 'vz'},
		{key: 'g', value: 'G'},
		{key: 'tt', value: 'a'},
		{key: 't', value: 'a'},
		{key: 'dddd', value: 'EEEE'},
		{key: 'ddd', value: 'EEE'}
	];

	function _convertToCLDRDateFormat(pattern) {
		var index = 0;
		var mappingItem;

		for (index; index < _formatCharMappingArray.length; index += 1) {
			mappingItem = _formatCharMappingArray[index];
			pattern = pattern.replace(mappingItem.key, mappingItem.value);
		}
		return pattern;
	}
	/**
	 * @return {number}  offset between time zones
	 */
	BaseDateTimeFormatInfo.prototype.OffsetBetweenTimeZones = function(date, tzname1, tzname2) {
		tzname1 = tzname1 || 'UTC';
		tzname2 = tzname2 || 'UTC';
		return Math.round(this.tzInfo.getTimeZoneOffset(date, tzname1) - this.tzInfo.getTimeZoneOffset(date, tzname2));
	};

	BaseDateTimeFormatInfo.prototype.HasTimeZone = function(tzname) {
		if (!tzname || tzname.trim().length === 0) {
			return true;
		}
		try {
			this.tzInfo.getTimeZoneOffset(new Date(), tzname);
			return true;
		} catch (ex) {
			return false;
		}
	};

	BaseDateTimeFormatInfo.prototype.Parse = function(dateStr, datePattern, locale) {
		var isoDate = this._dateStamp.fromISOString(dateStr);
		if (isoDate) {
			return isoDate;
		}
		var options = {};

		if (datePattern) {
			var selector = this.getSelector(datePattern);
			if (selector !== null) {
				options.selector = selector;
			}
			options.datePattern = _convertToCLDRDateFormat(datePattern);
		}
		options.locale = (locale) ? locale : this.defaultLocale;
		return this._dateLocale.parse(dateStr, options);
	};

	BaseDateTimeFormatInfo.prototype.Format = function(date, datePattern, locale) {
		var options = {};
		if (datePattern) {
			options.selector = this.getSelector(datePattern);
			options.datePattern = _convertToCLDRDateFormat(datePattern);
		}
		options.locale = (locale) ? locale : this.defaultLocale;
		if (this.ISOPattern.test(datePattern)) {
			var isoStr = this.toISOString(date, {selector: ''});
			if (isoStr) {
				return isoStr;
			}
		}
		return this._dateLocale.format(date, options);
	};

	//computes selecter from datePattern (see http://dojotoolkit.org/reference-guide/dojo/date/locale/format.html#dojo-date-locale-format)
	BaseDateTimeFormatInfo.prototype.getSelector = function(datePattern) {
		var timeReg = /[hHmstfF]/;
		var dateReg = /[Mdy]/;
		var isTime = timeReg.test(datePattern);
		var isDate = dateReg.test(datePattern);
		return ((isTime && isDate) || (!isTime && !isDate)) ? 'date' : (isTime) ? 'time' : 'date';
	};

	BaseDateTimeFormatInfo.prototype.toISOString = function(dateObject, options) { //dateObject - Date, options - dojo.date.stamp.__Options?
		//	summary:
		//		Format a Date object as a string according a subset of the ISO-8601 standard
		//
		//	description:
		//		When options.selector is omitted, output follows [RFC3339](http://www.ietf.org/rfc/rfc3339.txt)
		//		The local time zone is included as an offset from GMT, except when selector=="time" (time without a date)
		//		Does not check bounds.  Only years between 100 and 9999 are supported.
		//
		//	dateObject:
		//		A Date object
		var _ = function(n) { return (n < 10) ? '0' + n : n; };
		options = options || {};
		var formattedDate = [];
		getter = 'get';
		date = '';
		if (options.selector !== 'time') {
			var year = dateObject[getter + 'FullYear']();
			date = ['0000'.substr((year + '').length) + year, _(dateObject[getter + 'Month']() + 1), _(dateObject[getter + 'Date']())].join('-');
		}
		formattedDate.push(date);
		if (options.selector !== 'date') {
			var time = [_(dateObject[getter + 'Hours']()), _(dateObject[getter + 'Minutes']()), _(dateObject[getter + 'Seconds']())].join(':');
			var millis = dateObject[getter + 'Milliseconds']();
			if (options.milliseconds) {
				time += '.' + (millis < 100 ? '0' : '') + _(millis);
			}
			formattedDate.push(time);
		}
		return formattedDate.join('T'); // String
	};

	DateTimeFormatInfo = function(locale) {
		if (!(this instanceof BaseDateTimeFormatInfo)) {
			throw 'The class DateTimeFormatInfo doesn\'t initialize!';
		}
		var localeBundle = this._dateLocale._getGregorianBundle(locale);

		this.ShortDatePattern = localeBundle['dateFormat-short'];
		this.LongDatePattern = localeBundle['dateFormat-long'];
		this.ShortTimePattern = localeBundle['timeFormat-short'];
		this.LongTimePattern = localeBundle['timeFormat-long'];
		this.FullDateTimePattern = this.LongDatePattern.concat(' ', this.LongTimePattern);
		this.UniversalSortableDateTimePattern = 'yyyy-MM-ddTHH:mm:ssZ';
	};

	DateTimeFormatInfo.initClass = function() {
		DateTimeFormatInfo.prototype = new BaseDateTimeFormatInfo();
		delete DateTimeFormatInfo.initClass;
	};

	window.DateTimeFormatInfo = DateTimeFormatInfo;
})();
