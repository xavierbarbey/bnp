﻿function CultureInfo(locale) {
	if (!CultureInfo.initialized) {
		throw 'The class CultureInfo doesn\'t initialize!';
	}
	var _name = locale;
	var _dateFormat = null;

	Object.defineProperty(this, 'Name', {writable: false, configurable: false, enumerable: true, value: locale});
	Object.defineProperty(this, 'DateTimeFormat', {
		get: function() {
			if (!_dateFormat) {
				_dateFormat = new DateTimeFormatInfo(_name);
			}
			return _dateFormat;
		}
	});
}

CultureInfo.initClass = function() {
	delete CultureInfo.initClass;
	DateTimeFormatInfo.initClass();

	CultureInfo.CreateSpecificCulture = function(name) {
		return new CultureInfo(name);
	};

	Object.defineProperty(CultureInfo, 'initialized', {writable: false, configurable: false, enumerable: true, value: true});
	Object.defineProperty(CultureInfo, 'InvariantCulture', {writable: false, configurable: false, enumerable: true, value: new CultureInfo('en-us')});
};
