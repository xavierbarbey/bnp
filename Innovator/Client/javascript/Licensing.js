﻿// © Copyright by Aras Corporation, 2013.

Licensing.ActionType = {
	Activate: 0,
	Update: 1,
	Deactivate: 2
};

function Licensing(aras) {
	this.arasObject = aras;
	this.actionNamespace = aras.arasService.actionNamespace;
	this.iServiceName = aras.arasService.serviceName;
	this.url = aras.arasService.serviceUrl;
	this.propgressBarImage = '../images/Progress.gif';
	this.wnd = window;
	this.error = {};
	this.state = '';

	this._getFeatureLicenseSecureId = function(featureLicenseId) {
		if (featureLicenseId) {
			var qry = this.arasObject.newIOMItem('Feature License', 'get');
			qry.setID(featureLicenseId);
			qry.setAttribute('select', 'secure_id');
			qry = qry.apply();
			if (!qry.isError() && qry.getItemCount() === 1) {
				return qry.getProperty('secure_id');
			}
		}
		return '';
	};

	this._validateActivationKey = function(activationKey) {
		if (!activationKey) {
			this.arasObject.AlertError(this.arasObject.getResource('', 'licensing.activation_key_is_empty'), '', '', this.wnd);
			return false;
		}
		return true;
	};

	this._getNonFailedDoc = function(arasObject, result) {
		if (!result) {
			this.error = {
				details: arasObject.getResource('', 'licensing.service_not_available'),
				title: arasObject.getResource('', 'licensing.service_not_available_title'),
			};
			return false;
		} else if (result.search(/<DOCTYPE/) !== -1) {
			this.error = {details: arasObject.getResource('', 'licensing.not_found')};
			return false;
		}

		var doc = arasObject.createXMLDocument();
		doc.loadXML(result);
		if (doc.selectSingleNode('//faultstring')) {
			this.error = {details: doc.selectSingleNode('//faultstring').text};
			return false;
		}
		return doc;
	};

	this._checkErrors = function(arasObject, result) {
		var doc = this._getNonFailedDoc(arasObject, result);
		if (!doc) {
			return doc;
		}
		var query = arasObject.newIOMInnovator().newItem();
		query.loadAML(doc.firstChild.text || '<Empty/>');
		var envelopeNd = query.dom.selectSingleNode('//*[local-name()=\'Envelope\']');
		if (envelopeNd) {
			query.loadAML(envelopeNd.xml);
		}
		if (query.isError()) {
			this.error = {alert: true, query: query};
			return false;
		}
		return query;
	};

	this._showErrorPage = function(win, activationKey) {
		var error = this.error;
		var params;

		this.error = null;
		var mainWindow = this.arasObject.getMainWindow();
		if (error && error.customError) {
			params = {
				aras: this.arasObject,
				subjectHide: error.subjectHide,
				errorDetails: error.details,
				title: error.title,
				dialogWidth: 365,
				dialogHeight: 140,
				content: 'Licensing/ActivateError.html'
			};
			mainWindow.ArasModules.Dialog.show('iframe', params);
		} else if (error && error.alert) {
			this.arasObject.AlertError(error.query);
		} else if (error) {
			var result = this._getRequiredServerInfo();
			if (activationKey) {
				result += '<activation_key>' + activationKey + '</activation_key>';
			}
			params = {
				title: error.title,
				aras: this.arasObject,
				result: result,
				errorDetails: error.details,
				dialogWidth: 400,
				dialogHeight: 295,
				content: 'Licensing/ServiceError.html'
			};
			mainWindow.ArasModules.Dialog.show('iframe', params);
		} else {
			return;
		}

		if (win) {
			win.close();
		}
	};

	this._callMethodOnLicensingService = function(methodName, paramName, paramValue) {
		// don't used soap because request body isn't XML
		var xhr = new XMLHttpRequest();
		var url = this.arasObject.getServerBaseURL() + 'Licensing.asmx/' + methodName;
		xhr.open('POST', url, false);

		var additionalHeaders = this.arasObject.getHttpHeadersForSoapMessage(methodName);
		Object.keys(additionalHeaders).forEach(function(header) {
			xhr.setRequestHeader(header, additionalHeaders[header]);
		});

		var body = paramName + '=' + encodeURIComponent(paramValue);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
		xhr.send(body);

		if (xhr.status !== 200) {
			this._errorCallback(xhr.statusText, xhr.responseText);
			return false;
		}
		return true;
	};

	this._getFrameworkLicenseKey = function() {
		var serverInfo = this._getRequiredServerInfo();
		if (serverInfo) {
			frameworkLicenseKey = serverInfo.match(/<framework_license_key[^>]*>([^<]+)<\/framework_license_key>/)[1];
			return frameworkLicenseKey ? frameworkLicenseKey : '';
		}

		return '';
	};

	this._getRequiredServerInfo = function() {
		var result;
		var self = this;
		ArasModules.soap('', {
			url: this.arasObject.getServerBaseURL() + 'Licensing.asmx/',
			appendMethodToURL: true,
			async: false,
			method: 'GetServerInfo',
			methodNm: this.actionNamespace,
			restMethod: 'POST'
		})
					.then(function(responseText) {
						result = responseText;
					}, function(rq) {
						result = self._errorCallback(rq.statusText, rq.responseText);
					});
		if (!result) { return ''; }

		var doc = this.arasObject.createXMLDocument();
		doc.loadXML(result);
		return doc.documentElement ? doc.documentElement.text : '';
	};

	this._displayFeatureRequirementsForms = function(getMetaInfoResult, activationKey, featureLicenseId, action) {
		var self = this;
		function showFeatureRequirementDialog(params) {
			var aras = params.aras;
			var container = getLicenseActionContainer(self, activationKey);
			if (container) {
				var postData = container;
				var queryParameters = '';
				queryParameters = appendParameter(queryParameters, 'actionName=' + params.action);
				queryParameters = appendParameter(queryParameters, 'activationKey=' + params.activationKey);
				var topWndAras = aras.getMostTopWindowWithAras(window).aras;
				var formUrl = 'VirtualGetLicenseForm' + (queryParameters ? '?' + queryParameters : '');
				var postUrl = topWndAras.getBaseURL() + '/Modules/aras.innovator.core.License/PostLicenseForm';
				var xmlHttp = topWndAras.XmlHttpRequestManager.CreateRequest();
				xmlHttp.open('POST', postUrl, false);
				xmlHttp.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xmlHttp.send(
					JSON.stringify({
						actionName: params.action,
						activationKey: params.activationKey,
						postData: postData
					}));
				return formUrl;
			} else {
				//false
				return container;
			}

			function getLicenseActionContainer(context) {
				var result = false;
				var requestBody = '' +
					'<data>' +
					'<parameterData>' +
					'<version>' + aras.commonProperties.clientRevision + '</version>' +
					'</parameterData>' +
					'</data>';
				try {
					var method = 'GetLicenseActionContainer';
					var methodNm = context.actionNamespace;
					var requestResponse;
					ArasModules.soap(requestBody, {
						url: context.url,
						async: false,
						method: method,
						methodNm: methodNm,
						SOAPAction: methodNm + context.iServiceName + '/' + method,
						headers: {}
					})
						.then(function(responseText) {
							requestResponse = responseText;
						}, function(req) {
							var dialogErrorCallback = context._GenerateActivateErrorCallback(
								context.arasObject.getResource('', 'licensing.could_not_load_feature_requirements'),
								activationKey
							);
							dialogErrorCallback(req.statusText, req.responseText, req.status);
						});

					var doc = aras.createXMLDocument();
					doc.loadXML(requestResponse);
					var envelope = doc.selectSingleNode('//*[local-name()=\'Envelope\']');
					if (envelope) {
						result = envelope.text;
					}
				}
				finally {
					return result;
				}
			}

			function appendParameter(param, value) {
				if (param !== '') {
					param += '&';
				}
				param += value;
				return param;
			}
		}

		var query = this._checkErrors(this.arasObject, getMetaInfoResult);
		if (!query) {
			this._showErrorPage(null, activationKey);
			return false;
		}

		var params = this.arasObject.newObject();
		params.aras = this.arasObject;
		var formArray = [];
		var itemTypeArray = [];
		var instanceArray = [];
		var listArray = [];
		var width = 350;
		var height = 150;
		var forms = query.getItemsByXPath('/AML/Item[@type=\'Form\']');
		var formsCount = forms.getItemCount();
		if (formsCount === 0) {
			if (action !== Licensing.ActionType.Deactivate) {
				return featureLicenseId ? this.Update(featureLicenseId) : this.Activate(activationKey);
			}
			return this.Deactivate(featureLicenseId);
		} else {
			for (var i = 0; i < formsCount; i++) {
				var formItem = forms.getItemByIndex(i);

				var w = parseInt(formItem.getProperty('width'));
				var h = parseInt(formItem.getProperty('height'));
				width = w > width ? w : width;
				height = h > height ? h : height;

				formArray.push(formItem);
				listArray.push(query.getItemsByXPath('/AML/Item[@type=\'List\']'));
				itemTypeArray.push(query.getItemsByXPath('/AML/Item[@type=\'ItemType\' and name=\'' + formItem.getProperty('name') + '\']'));
				instanceArray[i] = query.getItemsByXPath('/AML/Item[@type=\'' + formItem.getProperty('name') + '\']');
			}

			height = parseInt(height) + 66;
			params.licensingObject = this;
			params.forms = formArray;
			params.itemTypes = itemTypeArray;
			params.listArray = listArray;
			params.instances = instanceArray;
			params.activationKey = activationKey;
			params.featureLicenseId = featureLicenseId;
			params.action = action;
			self = this;
			params.dialogWidth = width;
			params.dialogHeight = height;
			var formUrl = showFeatureRequirementDialog(params);
			if (formUrl) {
				params.content = formUrl;
				var mainWindow = this.arasObject.getMainWindow();
				mainWindow.ArasModules.Dialog.show('iframe', params);
			} else {
				return false;
			}
		}
		return true;
	};

	this._getMetaInfoFromArasService = function(action, activationKey, featureLicenseId) {

		var methodName;
		var callbackMethod;
		var parameterName = 'featureLicenseSecureId';
		var parameterValue = this._getFeatureLicenseSecureId(featureLicenseId);

		if (Licensing.ActionType.Activate === action) {
			parameterName = 'activationKey';
			parameterValue = activationKey;
			methodName = 'GetMetaInfoForActivate';
			callbackMethod = this._GenerateActivateErrorCallback(null, activationKey);
		} else if (Licensing.ActionType.Update === action) {
			methodName = 'GetMetaInfoForUpdate';
		} else if (Licensing.ActionType.Deactivate === action) {
			methodName = 'GetMetaInfoForDeactivate';
		}

		var metaInfoResult;
		var statusId;
		try {
			statusId = this.arasObject.showStatusMessage('status', this.arasObject.getResource('', 'licensing.request_aras'), this.propgressBarImage);

			var methodNamespace = this.actionNamespace;
			var requestBody = '<' + parameterName + '>' + parameterValue + '</' + parameterName + '>';

			ArasModules.soap(requestBody, {
				url: this.url,
				async: false,
				method: methodName,
				methodNm: methodNamespace,
				SOAPAction: methodNamespace + this.iServiceName + '/' + methodName,
				headers: {}
			})
				.then(function(responseText) {
					metaInfoResult = responseText;
				}, function(req) {
					callbackMethod(req.statusText, req.responseText, req.status);
				});
		} finally {
			this.arasObject.clearStatusMessage(statusId);
		}
		this._displayFeatureRequirementsForms(metaInfoResult, activationKey, featureLicenseId, action);
	};

	this._showInformationAboutImportedFeatureLicense = function(result, title) {

		var self = this;
		var mainWindow = this.arasObject.getMainWindow();
		mainWindow.ArasModules.Dialog.show('iframe', {
			aras: this.arasObject,
			result: result,
			title: this.arasObject.getResource('', 'licensing.success_import_title'),
			dialogWidth: 600,
			dialogHeight: 270,
			center: true,
			content: 'Licensing/SuccessMessage.html'
		});
	};

	this._showActivateFeatureDialog = function LicensingShowActivateFeatureDialog(activationKey, action, featureLicenseId) {

		var aras = this.arasObject;
		var params = this.arasObject.newObject();
		params.aras = aras;
		params.licensingObject = this;
		params.action = action;
		params.activationKey = activationKey;
		params.featureLicenseId = featureLicenseId;
		params.dialogWidth = 350;
		params.dialogHeight = 140;
		params.center = true;
		params.content = 'Licensing/ActivateFeature.html';
		switch (action || Licensing.ActionType.Activate) {
			case 0:
				params.title = aras.getResource('', 'licensing.action_type_activate');
				break;
			case 1:
				params.title = aras.getResource('', 'licensing.action_type_update');
				break;
			case 2:
				params.title = aras.getResource('', 'licensing.action_type_deactivate');
				break;
		}

		var mainWindow = aras.getMainWindow();
		mainWindow.ArasModules.Dialog.show('iframe', params);
	};

	this._getFeatureLicenseByLicenseData = function(encryptedFeatureLicense) {
		var qry = this.arasObject.newIOMItem('Feature License', 'get');
		if (encryptedFeatureLicense && encryptedFeatureLicense.length > 3999) {
			encryptedFeatureLicense = encryptedFeatureLicense.substring(0, 3999) + '%';
		}
		qry.setProperty('license_data', encryptedFeatureLicense);
		return qry.apply();
	};

	this._prepareLicenseRequestBody = function(propertyName, propertyValue, additionalData) {
		var serverInfo = this._getRequiredServerInfo();
		additionalData = additionalData ? additionalData : '<additonal_data />';
		var result = '' +
		'<' + propertyName + '>' + propertyValue + '</' + propertyName + '>' +
		'<data>' +
		'	<FeatureLicense>' +
		serverInfo +
		additionalData +
		'	</FeatureLicense>' +
		'</data>';
		return result;
	};

	this._getFeatureTreeFromInnovatorServer = function() {
		var method = 'GetFeatureTree';
		var methodNm = this.actionNamespace;
		var self = this;
		var res;

		ArasModules.soap('', {
			url: this.arasObject.getServerBaseURL() + 'Licensing.asmx/GetFeatureTree',
			async: false,
			method: method,
			methodNm: methodNm,
			SOAPAction: methodNm + this.iServiceName + '/' + method,
			headers: this.arasObject.getHttpHeadersForSoapMessage()
		})
			.then(function(result) {
				res = result;
			}, function(req) {
				self._errorCallback(req.statusText, req.responseText, req.status);
			});

		return res;
	};

	this._errorCallback = function(errorMessage, technicalMessage, stackTrace) {
		technicalMessage = !technicalMessage ? '' : technicalMessage;
		stackTrace = !stackTrace ? technicalMessage : stackTrace;
		this.arasObject.AlertError(errorMessage, technicalMessage, stackTrace);
		return false;
	};

	this._GetResponseFaultstring = function(responseText) {
		var doc = this.arasObject.createXMLDocument();
		doc.loadXML(responseText);

		var errorDetails = '';

		if (doc.selectSingleNode('//faultstring')) {
			errorDetails = doc.selectSingleNode('//faultstring').text;
		}
		return errorDetails;
	};

	this._GenerateActivateErrorCallback = function(messagePrefix, activationKey) {
		var prefix = messagePrefix;
		if (!prefix) {
			prefix = '';
		}
		return function(statusText, responseText, status) {
			if (status !== 404) {
				var errorDetails = this._GetResponseFaultstring(responseText);
				this._ShowActivateErrorDialog(prefix + errorDetails);
			}
		}.bind(this);
	};

	this._ShowActivateErrorDialog = function(errorDetails) {
		var params = this.arasObject.newObject();
		params.aras = this.arasObject;
		params.errorDetails = errorDetails;
		params.title = this.arasObject.getResource('', 'licensing.activation_error_tilte');
		params.dialogWidth = 365;
		params.dialogHeight = 160;
		params.content = 'Licensing/ActivateError.html';
		var mainWindow = this.arasObject.getMainWindow();
		mainWindow.ArasModules.Dialog.show('iframe', params);
		return false;
	};
}

Licensing.prototype.GetLicenseAgreement = function LicensingGetLicenseAgreement(activationKey) {
	var requestBody = this._prepareLicenseRequestBody('activationKey', activationKey);
	var query = false;
	var statusId = '';
	try {
		statusId = this.arasObject.showStatusMessage('status', this.arasObject.getResource('', 'licensing.request_aras'), this.propgressBarImage);

		var method = 'GetLicenseAgreement';
		var methodNm = this.actionNamespace;
		var self = this;
		var requestResponse;
		ArasModules.soap(requestBody, {
			url: this.url,
			async: false,
			method: method,
			methodNm: methodNm,
			SOAPAction: methodNm + this.iServiceName + '/' + method,
			headers: {}
		})
			.then(function(responseText) {
				requestResponse = responseText;
			}, function(req) {
				self._GenerateActivateErrorCallback(null, activationKey)(req.statusText, req.responseText, req.status);
			});

		query = this._checkErrors(this.arasObject, requestResponse);
	} finally {
		this.arasObject.clearStatusMessage(statusId);
	}
	return query;
};

Licensing.prototype.Activate = function LicensingActivate(activationKey, additionalData, additionalOptions) {
	if (!this._validateActivationKey(activationKey)) {
		return false;
	}

	var requestBody = this._prepareLicenseRequestBody('activationKey', activationKey, additionalData);
	var statusId = '';
	try {
		statusId = additionalOptions ? additionalOptions.statusId : this.arasObject.showStatusMessage('status',
		this.arasObject.getResource('', 'licensing.request_aras'), this.propgressBarImage);

		var method = 'Activate';
		var methodNm = this.actionNamespace;
		var self = this;
		var requestResponse;

		ArasModules.soap(requestBody, {
			url: this.url,
			async: false,
			method: method,
			methodNm: methodNm,
			SOAPAction: methodNm + this.iServiceName + '/' + method,
			headers: {}
		})
			.then(function(responseText) {
				requestResponse = responseText;
			}, function(req) {
				self._GenerateActivateErrorCallback(null, activationKey)(req.statusText, req.responseText, req.status);
			});

		if (!requestResponse) {
			return false;
		}

		var query = this._checkErrors(this.arasObject, requestResponse);
		if (!query) {
			this._showErrorPage(additionalOptions ? additionalOptions.win : null);
			return false;
		}

		if (additionalOptions) {
			additionalOptions.featureAction = 'activate';
		}

		return this.ImportFeatureLicense(query.getResult(), additionalOptions);
	} finally {
		this.arasObject.clearStatusMessage(statusId);
	}
};

Licensing.prototype.Update = function LicensingUpdate(featureLicenseId, additionalData, additionalOptions) {
	var featureLicenseSecureId = this._getFeatureLicenseSecureId(featureLicenseId);
	var requestBody = this._prepareLicenseRequestBody('featureLicenseSecureId', featureLicenseSecureId, additionalData);
	var statusId = '';
	try {
		statusId = additionalOptions ? additionalOptions.statusId : this.arasObject.showStatusMessage('status',
		this.arasObject.getResource('', 'licensing.request_aras'), this.propgressBarImage);

		var method = 'Update';
		var methodNm = this.actionNamespace;
		var requestResponse;

		ArasModules.soap(requestBody, {
			url: this.url,
			async: false,
			method: method,
			methodNm: methodNm,
			SOAPAction: methodNm + this.iServiceName + '/' + method,
			headers: {}
		})
			.then(function(responseText) {
				requestResponse = responseText;
			});

		var query = this._checkErrors(this.arasObject, requestResponse);
		if (!query) {
			this._showErrorPage(additionalOptions ? additionalOptions.win : null);
			return false;
		}

		if (additionalOptions) {
			additionalOptions.win.close();
		}

		var mainWindow = this.arasObject.getMainWindow();
		var self = this;
		mainWindow.ArasModules.Dialog.show('iframe', {
			aras: this.arasObject,
			featureAction: 'update',
			title: arasObject.getResource('', 'licensing.success_update_title'),
			dialogWidth: 350,
			dialogHeight: 120,
			center: true,
			content: 'Licensing/SuccessMessage.html'
		});

		return true;
	} finally {
		this.arasObject.clearStatusMessage(statusId);
	}
};

Licensing.prototype.Deactivate = function LicensingDeactivate(featureLicenseId, additionalData, additionalOptions) {
	var featureLicenseSecureId = this._getFeatureLicenseSecureId(featureLicenseId);
	var requestBody = this._prepareLicenseRequestBody('featureLicenseSecureId', featureLicenseSecureId, additionalData);
	var statusId = '';
	try {
		statusId = additionalOptions ? additionalOptions.statusId : this.arasObject.showStatusMessage('status',
		this.arasObject.getResource('', 'licensing.request_aras'), this.propgressBarImage);

		var method = 'Deactivate';
		var methodNm = this.actionNamespace;
		var requestResponse;

		ArasModules.soap(requestBody, {
			url: this.url,
			async: false,
			method: method,
			methodNm: methodNm,
			SOAPAction: methodNm + this.iServiceName + '/' + method,
			headers: {}
		})
			.then(function(responseText) {
				requestResponse = responseText;
			});

		var query = this._checkErrors(this.arasObject, requestResponse);
		if (!query) {
			this._showErrorPage(additionalOptions ? additionalOptions.win : null);
			return false;
		}

		this.arasObject.deleteItem('Feature License', featureLicenseId, true);
	} finally {
		this.arasObject.clearStatusMessage(statusId);
	}
	return true;
};

Licensing.prototype.showState = function LicensingShowState() {
	if (this.state) {
		this.arasObject.AlertSuccess(this.state, window);
		this.state = null;
	}
};

Licensing.prototype.UpdateFeatureTreeUI = function LicensingUpdateFeatureTreeUI() {
	var statusId = '';
	try {
		statusId = this.arasObject.showStatusMessage('status', this.arasObject.getResource('', 'licensing.request_aras'), this.propgressBarImage);

		if (!this.UpdateFeatureTree()) {
			this._showErrorPage();
			return false;
		}
	} finally {
		this.showState();
		this.arasObject.clearStatusMessage(statusId);
	}
	return true;
};

Licensing.prototype.UpdateFeatureTree = function LicensingUpdateFeatureTree(callback) {
	var self = this;
	var getResponse = function(responseText) {
		var arasObject = self.arasObject;
		if (!responseText) {
			self.error = {
				customError: true,
				subjectHide: true,
				details: arasObject.getResource('', 'licensing.update_feature_tree_service_not_available'),
				title: arasObject.getResource('', 'licensing.update_feature_tree_service_not_available_title')
			};
			if (callback) {
				callback();
			}
			return false;
		} else if (!self._getNonFailedDoc(arasObject, responseText)) {
			self.state = arasObject.getResource('', 'licensing.feature_tree_failed_updated_featureTree');
			if (callback) {
				callback();
			}
			return false;
		}

		var doc = arasObject.createXMLDocument();
		doc.loadXML(responseText);
		var featureTreeText = doc.documentElement ? doc.documentElement.text : '';

		var bResult = self._callMethodOnLicensingService('UpdateFeatureTree', 'encryptedFeatureTree', featureTreeText);
		bResult = !!bResult;
		self.state = bResult ? arasObject.getResource('', 'licensing.feature_tree_successfully_updated') :
		arasObject.getResource('', 'licensing.feature_tree_failed_updated_featureTree');

		if (callback) {
			callback(bResult);
		}

		return bResult;
	};

	var data = '<frameworkLicenseKey>' + this._getFrameworkLicenseKey() + '</frameworkLicenseKey>';
	var method = 'GetFeatureTree';
	var methodNm = this.actionNamespace;
	var getFeatureTreeResult;

	ArasModules.soap(data, {
		async: !!callback,
		url: this.url,
		method: method,
		methodNm: methodNm,
		SOAPAction: methodNm + this.iServiceName + '/' + method,
		headers: {}
	})
		.then(function(result) {
			if (callback) {
				getResponse(result);
			} else {
				getFeatureTreeResult = result;
			}
		}, function() {
			self.state = self.arasObject.getResource('', 'licensing.feature_tree_failed_updated_featureTree');

			if (callback) {
				callback();
			}
		});

	if (!callback) {
		return getResponse(getFeatureTreeResult);
	}
};

Licensing.prototype.ShowLicenseManagerDialog = function LicensingShowLicenseManagerDialog() {

	var params = {
		aras: this.arasObject,
		title: this.arasObject.getResource('', 'licmanager.title'),
		dialogWidth: 1000,
		dialogHeight: 420,
		resizable: true,
		center: true,
		content: 'Licensing/LicManager.html'
	};

	var mainWindow = this.arasObject.getMainWindow();
	mainWindow.ArasModules.Dialog.show('iframe', params);
};

Licensing.prototype.ActivateFeature = function LicensingActivateFeature(activationKey) {
	return this._showActivateFeatureDialog(activationKey);
};

Licensing.prototype.DeactivateFeature = function LicensingDeactivateFeature(activationKey, featureLicenseId) {
	return this._showActivateFeatureDialog(activationKey, Licensing.ActionType.Deactivate, featureLicenseId);
};

Licensing.prototype.ImportFeatureLicense = function LicensingImportFeatureLicense(featureLicenseText, additionalOptions) {
	var importFeature = function(featureLicenseText) {
		var bResult = this._callMethodOnLicensingService('ImportFeatureLicense', 'encryptedFeatureLicense', featureLicenseText);
		if (bResult) {

			if (additionalOptions) {
				additionalOptions.win.close();
			}

			var featureLicense = this._getFeatureLicenseByLicenseData(featureLicenseText);
			if (featureLicense.isError()) {
				this.arasObject.AlertError(featureLicense, this.wnd);
				return false;
			}

			var self = this;

			var params = {
				aras: this.arasObject,
				result: featureLicense,
				featureAction: additionalOptions ? additionalOptions.featureAction : '',
				dialogWidth: 350,
				dialogHeight: 190,
				center: true,
				content: 'Licensing/SuccessMessage.html'
			};
			params.title = this.arasObject.getResource('', (params.featureAction === 'update') ? 'licensing.success_update_title' : 'licensing.success_import_title');

			var mainWindow = this.arasObject.getMainWindow();
			window.setTimeout(function() {
				mainWindow.ArasModules.Dialog.show('iframe', params).promise.then();
			}.bind(this), 0);
		}
	};

	if (!featureLicenseText) {

		var mainWindow = this.arasObject.getMainWindow();
		mainWindow.ArasModules.Dialog.show('iframe', {
			aras: this.arasObject,
			title: this.arasObject.getResource('', 'licensing.import_feature_license'),
			dialogWidth: 350,
			dialogHeight: 140,
			center: true,
			content: 'Licensing/ImportLicense.html'
		}).promise.then(
			function(res) {
				if (res) {
					importFeature.bind(this)(res);
				}
			}.bind(this)

		);
	} else {
		importFeature.call(this, featureLicenseText);
	}
};

Licensing.prototype.PerformActionOverFeature = function LicensingPerformActionOverFeature(action, activationKey, featureLicenseId) {

	if (!this._validateActivationKey(activationKey)) {
		return false;
	}
	this._getMetaInfoFromArasService(action, activationKey, featureLicenseId);
};

Licensing.prototype.ViewFeatureTree = function LicensingViewFeatureTree() {

	var featureTreeXml = this._getFeatureTreeFromInnovatorServer();
	if (!featureTreeXml) {
		return;
	}
	var doc = this.arasObject.createXMLDocument();
	doc.loadXML(featureTreeXml);

	var xmlDocument = this.arasObject.createXMLDocument();
	xmlDocument.validateOnParse = true;
	xmlDocument.loadXML(doc.childNodes[doc.childNodes.length - 1].text);

	var xslt = this.arasObject.createXMLDocument();
	xslt.load(this.arasObject.getScriptsURL() + 'Licensing/FeatureTree.xsl');

	featureTreeXml = xmlDocument.transformNode(xslt);

	var params = {
		aras: this.arasObject,
		featureTreeXml: featureTreeXml,
		title: 'Aras Feature Tree',
		dialogHeight: 620,
		dialogWidth: 500,
		resizable: true,
		center: true,
		scroll: true,
		content: 'Licensing/FeatureTree.html'
	};

	var mainWindow = this.arasObject.getMainWindow();
	mainWindow.ArasModules.Dialog.show('iframe', params);
};
