﻿function ControlWrapperFactory() {

	function fakeMethodHandler(currentAras) {
		var resourceKey;
		if (currentAras.Browser.isIe()) {
			resourceKey = 'aras_object.fake_native_control_method_is_used_warning_ie';
		} else if (currentAras.Browser.isFf()) {
			resourceKey = 'aras_object.fake_native_control_method_is_used_warning_ff';
		} else {
			//do not throw any exceptions to have possibility to use this fake method in different browsers
			//just use the warning of FireFox
			resourceKey = 'aras_object.fake_native_control_method_is_used_warning_ch';
		}
		currentAras.AlertError(currentAras.getResource('', resourceKey, currentAras.getInnovatorUrl()));
	}

	this.createVaultWrapper = function(currentAras, parentAras) {
		if (!parentAras) {
			parentAras = currentAras;
		}
		fileSystemAccess.init(parentAras);
		var res = new Vault(fileSystemAccess, function() {
			fakeMethodHandler(currentAras);
		});
		res.init();
		return res;
	};

	this.createUtilsWrapper = function(currentAras, parentAras) {
		if (!parentAras) {
			parentAras = currentAras;
		}
		var res = new Utils(parentAras.Browser.isIe(), false, function() {
			fakeMethodHandler(parentAras);
		});
		res.init();
		return res;
	};
}

var controlWrapperFactory = new ControlWrapperFactory();
