﻿import Sidebar from '../components/sidebar';
import ContextMenu from '../components/contextMenu';
import utils from '../core/utils';
import { contentTabId } from './sidebarDataConverter';

const pinUrl = 'svg-pinnedoff';
const unPinUrl = 'svg-pinnedon';
const secondaryMenuClass = 'aras-navigation-panel__secondary-menu aras-secondary-menu';
const secondaryMenuHiddenClass = 'aras-secondary-menu_hidden';
const disabledRowHighlightingClass =
	'aras-navigation-panel_row-highlighting-disabled';

function navFormatter(nav, item) {
	const isDashboardItem = item.value.formId || item.value.startPage;

	if (item.value.itemTypeId || isDashboardItem) {
		const leafTemplate = nav.templates.getDefaultTemplate(item);
		const nodeClassList = isDashboardItem
			? 'aras-nav-leaf-ico'
			: 'aras-button aras-button_c aras-nav-leaf-ico';
		const nodeIconUrl = isDashboardItem
			? '../images/OpenInTab.svg'
			: '../images/ExecuteSearch.svg';

		leafTemplate.push(
			Inferno.createVNode(
				Inferno.getFlagsForElementVnode('span'),
				'span',
				nodeClassList,
				ArasModules.SvgManager.createInfernoVNode(nodeIconUrl),
				utils.infernoFlags.unknownChildren
			)
		);

		return leafTemplate;
	}
}

export default class NavigationPanel extends Sidebar {
	nav = document.createElement('aras-nav');
	tabs = document.createElement('aras-navigation-panel-tabs');

	togglePin(pinned) {
		super.togglePin(pinned);
		this.render();
	}

	toggleVisibility(visible) {
		super.toggleVisibility(visible);
		const splitter = this.nextElementSibling;
		splitter.classList.toggle('aras-hide', !this.isVisible);
		const navButtonID =
			'com.aras.innovator.cui_default.mwh_header_navigation_button';
		const toolbar = document.querySelector('aras-toolbar');
		const navButton = toolbar.data.get(navButtonID);
		if (this.isVisible !== navButton.state) {
			toolbar.data.set(
				navButtonID,
				Object.assign({}, navButton, { state: this.isVisible })
			);
			toolbar.render();
		}
	}

	render() {
		const secondaryMenuData = this._getSecondaryMenuData();
		const baseUrl = window.location.href.replace(window.location.hash, '');
		const pinIconUrl = baseUrl + '#' + (this.isPinned ? unPinUrl : pinUrl);

		this.html`
		<div class="aras-navigation-panel__header">
			<button
				class="${'aras-button aras-button_c ' + (secondaryMenuData ? '' : 'aras-hide')}"
				onclick="${() => this._hideSecondaryMenu()}"
			>
				${ArasModules.SvgManager.createHyperHTMLNode('../images/BackFull.svg', {
					class: 'aras-button__icon'
				})}
			</button>
			${ArasModules.SvgManager.createHyperHTMLNode(
				secondaryMenuData && secondaryMenuData.icon,
				{ class: 'aras-navigation-panel__header-icon' }
			)}
			<span class="aras-navigation-panel__header-title">
				${
					secondaryMenuData
						? secondaryMenuData.pluralLabel
						: aras.getResource('', 'common.contents')
				}
			</span>
			<span class="aras-navigation-panel__pin-icon aras-button aras-button_c" onclick="${() =>
				this.togglePin()}">
				<svg class="aras-button__icon"><use href="${pinIconUrl}"></use></svg>
			</span>
		</div>
		${this.nav}
		<div class="${secondaryMenuClass +
			' ' +
			(secondaryMenuData ? '' : secondaryMenuHiddenClass)}">
			<div class="aras-secondary-menu__buttons-container">
				<button
					class="aras-button aras-secondary-menu__create-button"
					disabled=${!secondaryMenuData || !secondaryMenuData.canAdd}
					onclick="${() => this._createNewItem(secondaryMenuData.itemTypeId)}"
				>
					${ArasModules.SvgManager.createHyperHTMLNode(
						'../images/CreateItem.svg',
						{ class: 'aras-button__icon' }
					)}
					<span class="aras-button__text">
						${aras.getResource(
							'',
							'navigation_panel.secondary_menu.create_new',
							secondaryMenuData && secondaryMenuData.singularLabel
						)}
					</span>
				</button>
				<button
					class="aras-button aras-secondary-menu__search-button"
					onclick="${() => this._searchItemType(secondaryMenuData.itemTypeId)}"
				>
					${ArasModules.SvgManager.createHyperHTMLNode(
						'../images/ExecuteSearch.svg',
						{ class: 'aras-button__icon' }
					)}
					<span class="aras-button__text">
						${aras.getResource(
							'',
							'navigation_panel.secondary_menu.search',
							secondaryMenuData && secondaryMenuData.pluralLabel
						)}
					</span>
				</button>
			</div>
		</div>
		${this.tabs}`;
	}

	connectedCallback() {
		super.connectedCallback();
		this.classList.add('aras-navigation-panel', disabledRowHighlightingClass);
		this.popupMenu = new ContextMenu();
		this._initContextMenuListeners();
		const observer = new MutationObserver(mutations => {
			const navPanel = mutations[0].target;
			const panelWidth = navPanel.style.width;
			navPanel.nextElementSibling.style.left = panelWidth;
		});

		observer.observe(this, {
			attributes: true,
			attributeFilter: ['style']
		});
		this.render();
	}

	created() {
		this._initTOC();
		this._initTabs();
	}

	_initContextMenuListeners() {
		const contextMenuDom = this.popupMenu.dom;
		contextMenuDom.addEventListener('contextMenuShow', () => {
			this.classList.remove(disabledRowHighlightingClass);
		});
		contextMenuDom.addEventListener('contextMenuClose', () => {
			this.classList.add(disabledRowHighlightingClass);
			if (!this.isPinned) {
				this._focusOutHandler();
			}
		});
		contextMenuDom.addEventListener('click', () => {
			this._hideIfNotPinned();
		});
	}

	_initTOC() {
		const selectTab = dataItem => {
			const tabs = this.tabs;
			tabs.data.forEach((tabItem, id) => {
				if (tabItem.itemTypeId === dataItem.itemTypeId) {
					tabs.selectTab(id);
				}
			});
		};
		this.nav.classList.add('aras-nav-toc');
		this.nav.formatter = navFormatter.bind(null, this.nav);
		this.nav.on('click', (itemKey, event) => {
			const dataItem = this.nav.data.get(itemKey);
			const isNewTabOpened =
				dataItem.formId ||
				dataItem.startPage ||
				event.target.closest('.aras-button.aras-nav-leaf-ico');
			if (isNewTabOpened) {
				this._hideIfNotPinned();
			} else if (dataItem.itemTypeId) {
				this._showSecondaryMenu(dataItem.itemTypeId);
				selectTab(dataItem);
			}
		});
	}

	_initTabs() {
		this.tabs.on('select', id => {
			if (id === contentTabId) {
				this._hideSecondaryMenu();
				return;
			}

			const item = this.tabs.data.get(id);
			this._showSecondaryMenu(item.itemTypeId);
		});
		this.tabs.on('click', id => {
			if (id === contentTabId) {
				this._hideSecondaryMenu();
			}
		});
	}

	_isElementInSidebar(element) {
		return (
			super._isElementInSidebar(element) ||
			(element && this.popupMenu.dom.contains(element))
		);
	}

	_hideIfNotPinned() {
		if (!this.isPinned) {
			this.toggleVisibility(false);
			document.documentElement.focus(); // for correct work of sidebar in IE and FF
		}
	}

	_getSecondaryMenuData() {
		const itemTypeId = this._secondaryMenuItemTypeId;
		if (!itemTypeId) {
			return null;
		}

		const itemType = aras.getItemTypeDictionary(itemTypeId, 'id');
		if (itemType.isError()) {
			return null;
		}
		const itemTypeName = itemType.getProperty('name');
		const singularLabel = itemType.getProperty('label') || itemTypeName;
		const pluralLabel = itemType.getProperty('label_plural') || singularLabel;
		const canAdd = aras.getPermissions('can_add', itemTypeId);
		const icon =
			itemType.getProperty('open_icon') || '../images/DefaultItemType.svg';

		return {
			icon,
			singularLabel,
			pluralLabel,
			itemTypeId,
			canAdd
		};
	}

	_showSecondaryMenu(itemTypeId) {
		this._secondaryMenuItemTypeId = itemTypeId;
		this.render();
	}

	_hideSecondaryMenu() {
		this._secondaryMenuItemTypeId = null;
		this.tabs.selectTab(contentTabId);
		this.render();
		this._setFocus();
	}

	_createNewItem(itemTypeId) {
		return aras.uiNewItemExAsync(itemTypeId).then(() => {
			this._hideIfNotPinned();
		});
	}

	_searchItemType(itemTypeId) {
		return Promise.resolve(window.arasTabs.openSearch(itemTypeId)).then(() => {
			this._hideIfNotPinned();
		});
	}
}

NavigationPanel.define('aras-navigation-panel');
