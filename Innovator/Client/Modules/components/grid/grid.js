﻿import { HeadWrap, RowsWrap } from './utils';
import GridActions from './actions';
import GridView from './view';
import Keyboard from './keyboard';
/**
 * Grid Component
 *
 * @class
 * @name Grid
 * @param {object}   dom Dom Element used as container
 * @param {object} options
 * @param {boolean} [options.multiSelect=true] options.multiSelect - enable/disable multi select
 * @param {boolean} [options.resizable=true] options.resizable - enable/disable resize of columns
 * @param {boolean} [options.search=false] options.search - show/hide simple search
 * @param {boolean} [options.editable=false] options.editable - enable/disable edit cell
 * @param {boolean} [options.sortable=true] options.sortable - enable/disable sort columns
 * @param {boolean} [options.freezableColumns=false] options.freezableColumns - enable/disable freeze of columns
 *
 * @property {Map} head - Data of heads grid which store in Grid. Returns wrapper on Map on get method for calls render grid after any changes.
 * @property {Map} rows - Data of rows grid which store in Grid. Returns wrapper on Map on get method for calls render grid after any changes.
 *
 * @property {object} settings - grid state
 * @property {number} settings.frozenColumns - amount of frozen columns
 * @property {string[]} settings.selectedRows - ids of selected rows
 * @property {{headId: string, rowId: string, editing: boolean}} settings.focusedCell - focus cell object
 * @property {{headId: string, desc: boolean}[]} settings.orderBy - sortable heads array
 */
function Grid(dom, options) {
	const self = this;

	this.dom = dom;
	this.eventCallbacks = new WeakMap();

	this.settings = {
		frozenColumns: 0,
		selectedRows: [],
		get focusedCell() {
			return this._focusedCell;
		},
		set focusedCell(next) {
			const prev = this._focusedCell;

			if (prev === next) {
				return self.view.showMessageActiveCell();
			}

			if (next && next.editing) {
				const rowId = next.rowId;
				const headId = next.headId;
				const value = self.rows.get(rowId)[headId];
				const type = self._getEditorType(headId, rowId, value, self);
				next.editing =
					self.view.defaultSettings.editable &&
					self.checkEditAvailability(next.headId, next.rowId, self) &&
					type !== 'nonEditable';
			}

			if (prev && prev.editing) {
				const applyEdit = function(value) {
					self.dom.dispatchEvent(
						new CustomEvent('applyEdit', {
							detail: {
								headId: prev.headId,
								rowId: prev.rowId,
								value: value
							}
						})
					);
					this._focusedCell = next;
				}.bind(this);
				if (!self.view.validator().willValidate) {
					applyEdit(self.view.validator().value);
				} else {
					self.view
						.validator()
						.willValidate(self.view.validator().value)
						.then(applyEdit)
						.catch(function(message) {
							self.dom.dispatchEvent(
								new CustomEvent('notValidEdit', {
									detail: {
										headId: prev.headId,
										rowId: prev.rowId,
										message: message,
										value: self.view.validator().value
									}
								})
							);
						});
				}
				return;
			}

			this._focusedCell = next;
			self.render();
		},
		get orderBy() {
			return this._orderBy ? this._orderBy.slice() : [];
		},
		set orderBy(next) {
			this._orderBy = next;
			self.sort().then(self.render.bind(self));
		}
	};
	this.initialization(options);
}

Grid.prototype = {
	constructor: Grid,

	/**
	 * The first initialization of Grid.
	 * Create view, action, keyboard classes and set options
	 *
	 * @protected
	 * @param {object} options Options object from Grid constructor
	 */
	initialization: function(options) {
		this.view = new GridView(this.dom, options);
		this.actions = new GridActions(this);
		this.keyboard = new Keyboard(this);
	},

	get head() {
		return this._head;
	},
	set head(value) {
		this._head = new HeadWrap(value, this);
		const array = [];
		value.forEach(function(val, id) {
			array.push(id);
		});
		this.settings.indexHead = array;
		this.render();
	},

	get rows() {
		return this._rows;
	},
	set rows(value) {
		this._rows = new RowsWrap(value, this);
		const array = [];
		value.forEach(function(val, id) {
			array.push(id);
		});
		this.settings.indexRows = array;
		this.render();
	},
	/**
	 * Private method of get cell type for formatter
	 *
	 * @private
	 * @param   {string} headId - Head id
	 * @param   {string} rowId  - Row id
	 * @param   {*} value - Cell value
	 * @returns {string} - return a type of Cell
	 */
	_getNativeCellType: function(headId, rowId, value) {
		let type = 'text';

		if (typeof value === 'boolean') {
			type = 'boolean';
		}

		return type;
	},
	/**
	 * Method which calls public getCellType method or private if the return getCellType is empty
	 *
	 * @private
	 * @param   {string} headId - Head id
	 * @param   {string} rowId  - Row id
	 * @param   {*} value - Cell value
	 * @param   {Grid} grid - Grid instance
	 * @returns {string} - return a type of Cell
	 */
	_getCellType: function(headId, rowId, value, grid) {
		const nativeType = this._getNativeCellType(headId, rowId, value);

		return this.getCellType(headId, rowId, value, nativeType) || nativeType;
	},
	/**
	 * Handler which get cell Type for call special formatter
	 *
	 * @public
	 * @param   {string} headId - Head id
	 * @param   {string} rowId  - Row id
	 * @param   {*} value - Cell value
	 * @param   {string} type - a type which calculated by grid
	 * @returns {string} - return a type of Cell
	 */
	getCellType: function(headId, rowId, value, type) {
		return type;
	},
	/**
	 * Handler which get cell Styles
	 *
	 * @public
	 * @param   {string} headId - Head id
	 * @param   {string} rowId  - Row id
	 * @returns {Object} - return a styles of Cell
	 */
	getCellStyles: function(headId, rowId) {
		return {};
	},
	/**
	 * Grid method which calculate a edit type cell for calls special editor
	 *
	 * @private
	 * @param   {string} headId - Head id
	 * @param   {string} rowId  - Row id
	 * @param   {*} value - Cell value
	 * @param   {Grid} grid - Grid instance
	 * @returns {string} - return a type of Cell
	 */
	_getEditorType: function(headId, rowId, value, grid) {
		const type = this._getCellType(headId, rowId, value, grid);
		if (type === 'boolean') {
			return 'nonEditable';
		}

		return this.getEditorType(headId, rowId, value, type) || type;
	},
	/**
	 * Handler which get cell Type for call special editor
	 * By default calls getCellType
	 *
	 * @public
	 * @param   {string} headId - Head id
	 * @param   {string} rowId  - Row id
	 * @param   {*} value - Cell value
	 * @param   {string} type - a type which calculated by grid
	 * @returns {string} - return a type of Cell
	 */
	getEditorType: function(headId, rowId, value, type) {
		return type;
	},
	/**
	 * Handler which check a editable of a Cell
	 *
	 * @public
	 * @param   {string} headId - Head id
	 * @param   {string} rowId  - Row id
	 * @param   {Grid} grid - instance of Grid
	 * @returns {boolean} - can a cell be edited or not
	 */
	checkEditAvailability: function(headId, rowId, grid) {
		return true;
	},
	/**
	 * Handler which calculating metadata for a cell
	 * By default returns null
	 *
	 * @public
	 * @param   {string} headId - Head id
	 * @param   {string} rowId - Row id
	 * @param   {string} type - Cell type which was calculated by grid
	 * @returns {?Object} - metadata object for editors or formatters
	 */
	getCellMetadata: function(headId, rowId, type) {
		return null;
	},
	/**
	 * Standard logic of sorting
	 *
	 * @private
	 * @returns {Promise} - the end of operation
	 */
	_sort: function() {
		return Promise.resolve(
			this.settings.indexRows.sort(this.actions._sortFn.bind(this))
		);
	},
	/**
	 * Standard logic of sorting
	 *
	 * @public
	 * @returns {Promise} - the end of operation
	 */
	sort: function() {
		return this._sort();
	},
	/**
	 * Cancel edit operation
	 *
	 * @public
	 */
	cancelEdit: function() {
		if (!this.settings._focusedCell) {
			return;
		}

		this.settings._focusedCell = Object.assign({}, this.settings._focusedCell, {
			editing: false
		});
		this.render();
	},
	/**
	 * Handler which return a css class name of Row
	 *
	 * @public
	 * @param   {string} rowId  - id of Row
	 * @returns {string} - CSS class name
	 */
	getRowClasses: function(rowId) {
		return '';
	},
	/**
	 * Start render of Grid
	 *
	 * @public
	 * @returns {Promise} - end of operation
	 */
	render: function() {
		return this.view.render(this);
	},

	/**
	 * Resize column event
	 *
	 * @event resizeHead
	 * @type {object}
	 * @property {number} index - index a head
	 */

	/**
	 * Select row event
	 *
	 * @event selectRow
	 * @type {object}
	 * @property {number} index - index a row
	 */

	/**
	 * Focus Cell event. If detail is null it is mean out focus
	 *
	 * @event focusCell
	 * @type {object}
	 * @property {number} indexHead - index a head
	 * @property {number} indexRow - index a row
	 */

	/**
	 * Apply editing value in Grid
	 *
	 * @event applyEdit
	 * @type {object}
	 * @property {string} rowId - row id
	 * @property {string} headId - head id
	 * @property {*} value - new value
	 */

	/**
	 * Cancel edit in Grid
	 *
	 * @event cancelEdit
	 * @type {object}
	 */

	/**
	 * Drag and drop column
	 *
	 * @event moveHead
	 * @type {object}
	 * @property {number} startIndex - index a head from
	 * @property {number} endIndex - index a head to
	 */

	/**
	 * Sorting event
	 *
	 * @event sort
	 * @type {object}
	 * @property {number} index - index a head
	 * @property {boolean} ctrlKey - ctrlKey is pressed for multi sorting
	 */

	/**
	 * Add listener for operation
	 *
	 * @public
	 * @param   {string} type - type of event
	 * @param   {function(headId, rowId, event)} callback - callback on event
	 * @param   {string} element  - 'row' or 'cell' or 'head' for native dom event only
	 */
	on: function(type, callback, element) {
		if (!element) {
			return this.dom.addEventListener(type, callback);
		}
		const callbackFunc = function(e) {
			const params = getCallbackParams(this, e.target, element);
			if (params && params.type === element) {
				callback.apply(this, params.ids.concat(e));
			}
		}.bind(this);

		this.eventCallbacks.set(callback, callbackFunc);
		this.dom.addEventListener(type, callbackFunc);
	},
	/**
	 * Remove listener for operation
	 *
	 * @public
	 * @param {string} type - event type
	 * @param {function(event, headId, rowId)} callback - callback function
	 */
	off: function(type, callback) {
		this.dom.removeEventListener(
			type,
			this.eventCallbacks.get(callback) || callback
		);
		if (this.eventCallbacks.has(callback)) {
			this.eventCallbacks.delete(callback);
		}
	}
};

function getCallbackParams(gridInstance, node, type) {
	let headId;
	const cell =
		node.closest('td.aras-grid-head-cell') ||
		node.closest('td.aras-grid-row-cell');
	if (cell) {
		if (cell.dataset.index !== undefined) {
			headId = gridInstance.settings.indexHead[cell.dataset.index];
			return { ids: [headId], type: 'head' };
		}
		const frozen = cell.closest('.aras-grid-body-boundary_frozen');
		const frozenClassName = '.aras-grid-header-boundary_frozen';
		const selector = frozen
			? `${frozenClassName} `
			: `:not(${frozenClassName}) > `;
		const headGrid = gridInstance.dom.querySelector(
			selector + '.aras-grid-head'
		);
		const headCell = headGrid.children[0].children[cell.cellIndex];
		headId = gridInstance.settings.indexHead[headCell.dataset.index];
	}

	const row = node.closest('tr.aras-grid-row');
	if (!row) {
		return;
	}
	const rowId = gridInstance.settings.indexRows[row.dataset.index];
	const isRowAndCell = type !== 'row' && headId !== undefined;
	return {
		ids: isRowAndCell ? [headId, rowId] : [rowId],
		type: isRowAndCell ? 'cell' : 'row'
	};
}

export default Grid;
