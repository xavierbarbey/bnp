﻿import gridFormatters from './formatters';

const dataTypeFormatters = {
	bool: function(headId, rowId, value, grid) {
		value = Boolean(Number(value));
		return gridFormatters.boolean(headId, rowId, value, grid);
	},
	color: function(headId, rowId, value, grid) {
		return {
			style: {
				'background-color': value
			},
			children: null
		};
	},
	'color list': function(headId, rowId, value, grid, metadata) {
		const headOptions = grid.head.get(headId, 'options');
		const headOptionsLables = grid.head.get(headId, 'optionsLables');
		const options = headOptions.map((option, index) => {
			return {
				value: option,
				label: headOptionsLables[index]
			};
		});
		const list = gridFormatters.select(
			headId,
			rowId,
			value,
			grid,
			Object.assign({}, metadata, { options: options })
		);
		const color = gridFormatters.color(headId, rowId, value, grid);

		return Object.assign(color, list);
	},
	date: function(headId, rowId, value, grid) {
		const format = aras
			.getDateFormatByPattern(grid.head.get(headId, 'pattern') || 'short_date')
			.replace('_d', 'D')
			.replace('_t', 'T');
		return gridFormatters.calendar(headId, rowId, value, grid, {
			format: format
		});
	},
	item: function(headId, rowId, value, grid) {
		value = grid.rows.get(rowId, `${headId}@aras.keyed_name`) || value;
		return gridFormatters.link(headId, rowId, value, grid);
	},
	image: function(headId, rowId, value, grid) {
		const regExp = /\ssrc=(?:(?:'([^']*)')|(?:"([^"]*)")|([^\s]*))/i; // match src='a' OR src="a" OR src=a
		value = grid.rows.get(rowId, `${headId}@aras.keyed_name`) || value;
		const srcMatches = value.match(regExp);
		value = srcMatches
			? srcMatches[1] || srcMatches[2] || srcMatches[3] || ''
			: value;

		return gridFormatters.img(headId, rowId, value, grid);
	}
};

export default dataTypeFormatters;
