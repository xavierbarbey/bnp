﻿function Keyboard(grid) {
	this.grid = grid;

	this.grid.dom.addEventListener('keydown', this._keydownHandler.bind(this));
}

Keyboard.prototype = {
	constructor: Keyboard,

	_dispatchFocusEvent: function(indexHead, indexRow, forceEdit) {
		this.grid.dom.dispatchEvent(
			new CustomEvent('focusCell', {
				detail: {
					indexRow: indexRow,
					indexHead: indexHead,
					forceEdit: forceEdit
				}
			})
		);
	},

	_dispatchSearchEvent: function() {
		this.grid.view.body.focus();
		this.grid.dom.dispatchEvent(new CustomEvent('focusCell'));
		this.grid.dom.dispatchEvent(new CustomEvent('search'));
	},

	_dispatchSelectEvent: function(indexRow, event) {
		const type = !this.grid.view.defaultSettings.multiSelect
			? 'single'
			: event.ctrlKey || event.metaKey
			? 'ctrl'
			: event.shiftKey
			? 'shift'
			: 'single';

		this.grid.dom.dispatchEvent(
			new CustomEvent('selectRow', {
				detail: {
					index: indexRow,
					type: type
				}
			})
		);
	},

	_dispatchSelectAllEvent: function(event) {
		if (!this.grid.view.defaultSettings.multiSelect) {
			return;
		}

		this.grid.dom.dispatchEvent(
			new CustomEvent('selectRow', {
				detail: {
					type: 'all'
				}
			})
		);
	},

	_dispatchCancelEditEvent: function() {
		this.grid.dom.dispatchEvent(new CustomEvent('cancelEdit'));
	},

	_keydownHandler: function(event) {
		const focusedCell = this.grid.settings.focusedCell;

		if (!focusedCell) {
			return;
		}

		const rowId = focusedCell.rowId;
		const headId = focusedCell.headId;
		let indexRow = this.grid.settings.indexRows.indexOf(rowId);
		let indexHead = this.grid.settings.indexHead.indexOf(headId);
		const rowsCount = this.grid.settings.indexRows.length - 1;

		const gridView = this.grid.view;
		const visibleRowCount =
			(gridView.bodyBoundary.clientHeight /
				gridView.defaultSettings.rowHeight) |
			0;
		const firstRowIndex =
			(gridView.bodyBoundary.scrollTop / gridView.defaultSettings.rowHeight) |
			0;

		const searchRowCell = event.target.closest('.aras-grid-search-row-cell');

		if (searchRowCell || rowId === 'searchRow') {
			switch (event.code || event.keyCode) {
				case 'Enter':
				case 'NumpadEnter':
				case 13: {
					this._dispatchSearchEvent();
					break;
				}
				case 'Tab':
				case 9: {
					event.preventDefault();
					indexHead = event.shiftKey ? --indexHead : ++indexHead;
					if (
						indexHead < 0 ||
						indexHead === this.grid.settings.indexHead.length
					) {
						return;
					}
					this._dispatchFocusEvent(indexHead, 'searchRow');
					break;
				}
			}
			return;
		}

		switch (event.code || event.keyCode) {
			case 'Enter':
			case 'NumpadEnter':
			case 13: {
				this._dispatchFocusEvent(indexHead, indexRow);
				break;
			}
			case 'Tab':
			case 9: {
				const lastHead = indexHead === this.grid.settings.indexHead.length - 1;
				const firstHead = indexHead === 0;
				const lastRow = indexRow === this.grid.settings.indexRows.length - 1;
				const firstRow = indexRow === 0;

				if (
					(lastHead && lastRow && !event.shiftKey) ||
					(firstHead && firstRow && event.shiftKey)
				) {
					event.preventDefault();
					return;
				}

				if (event.shiftKey) {
					indexRow = firstHead ? --indexRow : indexRow;
					indexHead = firstHead
						? this.grid.settings.indexHead.length - 1
						: --indexHead;
				} else {
					indexRow = lastHead ? ++indexRow : indexRow;
					indexHead = lastHead ? 0 : ++indexHead;
				}

				this._dispatchFocusEvent(indexHead, indexRow, true);
				event.preventDefault();
				break;
			}
			case 'Escape':
			case 27: {
				if (focusedCell.editing) {
					this._dispatchCancelEditEvent();
				}
				break;
			}
		}

		if (event.target.closest('.aras-grid-active-cell')) {
			return;
		}

		switch (event.code || event.keyCode) {
			case 'Home':
			case 'Numpad7':
			case 36: {
				this._dispatchFocusEvent(indexHead, 0);
				break;
			}
			case 'End':
			case 'Numpad1':
			case 35: {
				this._dispatchFocusEvent(indexHead, rowsCount);
				break;
			}
			case 'PageDown':
			case 'Numpad3':
			case 34: {
				const nextFirstRow = firstRowIndex + visibleRowCount;
				const cellTopPosition =
					nextFirstRow * gridView.defaultSettings.rowHeight;
				gridView.bodyBoundary.scrollTop = cellTopPosition;

				if (indexRow < firstRowIndex || indexRow >= nextFirstRow) {
					indexRow = firstRowIndex;
				}
				const newFocusedIndexRow = indexRow + visibleRowCount;
				if (indexRow < rowsCount) {
					this._dispatchFocusEvent(
						indexHead,
						Math.min(newFocusedIndexRow, rowsCount)
					);
				}
				break;
			}
			case 'PageUp':
			case 'Numpad9':
			case 33: {
				const nextFirstRow = firstRowIndex - visibleRowCount;
				const cellTopPosition =
					nextFirstRow * gridView.defaultSettings.rowHeight;
				gridView.bodyBoundary.scrollTop = cellTopPosition;

				let newFocusedIndexRow;
				if (nextFirstRow <= 0 && indexRow - visibleRowCount <= 0) {
					newFocusedIndexRow = indexRow;
				} else {
					newFocusedIndexRow = indexRow - visibleRowCount;
					if (indexRow > firstRowIndex + visibleRowCount) {
						newFocusedIndexRow = nextFirstRow;
					}
				}

				if (indexRow >= 0) {
					this._dispatchFocusEvent(
						indexHead,
						Math.max(0, nextFirstRow, newFocusedIndexRow)
					);
				}
				break;
			}
			case 'ArrowUp':
			case 'Numpad8':
			case 38: {
				if (indexRow > 0) {
					this._dispatchFocusEvent(indexHead, --indexRow);
				}
				break;
			}
			case 'ArrowDown':
			case 'Numpad2':
			case 40: {
				if (indexRow < this.grid.settings.indexRows.length - 1) {
					this._dispatchFocusEvent(indexHead, ++indexRow);
				}
				break;
			}
			case 'ArrowLeft':
			case 'Numpad4':
			case 37: {
				if (indexHead > 0) {
					this._dispatchFocusEvent(--indexHead, indexRow);
				}
				break;
			}
			case 'ArrowRight':
			case 'Numpad6':
			case 39: {
				if (indexHead < this.grid.settings.indexHead.length - 1) {
					this._dispatchFocusEvent(++indexHead, indexRow);
				}
				break;
			}
			case 'Space':
			case 32: {
				this._dispatchSelectEvent(indexRow, event);
				break;
			}
			case 'KeyA':
			case 65: {
				if (event.ctrlKey || event.metaKey) {
					this._dispatchSelectAllEvent(event);
				}
				break;
			}
			default:
				return;
		}

		event.preventDefault();
	}
};

export default Keyboard;
