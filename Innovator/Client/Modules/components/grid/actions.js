﻿function GridActions(grid) {
	this.grid = grid;
	this.handlers = [];
	this._addHandlers([
		{
			target: this.grid.dom,
			action: 'resizeHead',
			handler: this._resizeHandler.bind(this)
		},
		{
			target: this.grid.dom,
			action: 'selectRow',
			handler: this._selectHandler.bind(this)
		},
		{
			target: this.grid.dom,
			action: 'focusCell',
			handler: this._focusHandler.bind(this)
		},
		{
			target: this.grid.dom,
			action: 'applyEdit',
			handler: this._applyEditHandler.bind(this)
		},
		{
			target: this.grid.dom,
			action: 'notValidEdit',
			handler: this._notValidEditHandler.bind(this)
		},
		{
			target: this.grid.dom,
			action: 'cancelEdit',
			handler: this._cancelEditHandler.bind(this)
		},
		{
			target: this.grid.dom,
			action: 'moveHead',
			handler: this._moveHeadHandler.bind(this)
		},
		{
			target: this.grid.dom,
			action: 'sort',
			handler: this._sortHandler.bind(this)
		},
		{
			target: this.grid.dom,
			action: 'freezeColumns',
			handler: this._freezeColumnsHandler.bind(this)
		}
	]);
}

GridActions.prototype = {
	constructor: GridActions,

	_addHandlers: function(handlers) {
		handlers.forEach(function(item) {
			this.handlers.push(item);
			item.target.addEventListener(item.action, item.handler);
		}, this);
	},

	_resizeHandler: function(e) {
		const headId = this.grid.settings.indexHead[e.detail.index];
		const head = this.grid.head.get(headId);
		if (head) {
			head.width = Math.max(
				head.width + e.detail.delta,
				this.grid.view.defaultSettings.headWidth
			);
			this.grid.head.set(headId, head);
		}
	},

	_focusHandler: function(e) {
		const grid = this.grid;
		const prev = grid.settings.focusedCell;
		if (!e.detail) {
			grid.settings.focusedCell = null;
			grid.view.showMessageActiveCell();
			return;
		}

		const indexRow = e.detail.indexRow;
		const indexHead = e.detail.indexHead;

		let rowId = 'searchRow';
		const headId = grid.settings.indexHead[indexHead];
		let editing = false;
		if (indexRow !== 'searchRow') {
			rowId = grid.settings.indexRows[indexRow];
			editing =
				e.detail.forceEdit ||
				(!!prev &&
					!prev.editing &&
					prev.rowId === rowId &&
					prev.headId === headId);
		}

		grid.settings.focusedCell = {
			headId: headId,
			rowId: rowId,
			editing: editing
		};
	},

	_applyEditHandler: function(e) {
		const grid = this.grid;
		const headId = e.detail.headId;
		const rowId = e.detail.rowId;
		const value = e.detail.value;

		const row = grid.rows.get(rowId);
		row[headId] = value;
		grid.rows.set(rowId, row);
	},

	_notValidEditHandler: function(e) {
		const gridSettings = this.grid.settings;
		if (!gridSettings.focusedCell) {
			return;
		}

		gridSettings.focusedCell = Object.assign(gridSettings.focusedCell, {
			valid: false,
			toolTipMessage: e.detail.message
		});
	},

	_cancelEditHandler: function() {
		const grid = this.grid;
		grid.cancelEdit();
	},

	_selectHandler: function(e) {
		const rowIndex = e.detail.index;
		const rowId = this.grid.settings.indexRows[rowIndex];

		switch (e.detail.type) {
			case 'all': {
				this.grid.settings.selectedRows = this.grid.settings.indexRows.slice();
				break;
			}
			case 'single': {
				this.grid.settings.selectedRows = [rowId];
				break;
			}
			case 'ctrl': {
				const indexInSelected = this.grid.settings.selectedRows.indexOf(rowId);
				if (indexInSelected > -1) {
					this.grid.settings.selectedRows.splice(indexInSelected, 1);
				} else {
					this.grid.settings.selectedRows.unshift(rowId);
				}
				break;
			}
			case 'shift': {
				const lastSelectedRow =
					this.grid.settings.selectedRows[0] || this.grid.settings.indexRows[0];
				let indexOfLastSelectedRow = this.grid.settings.indexRows.indexOf(
					lastSelectedRow
				);
				this.grid.settings.selectedRows = [lastSelectedRow];
				const direction = indexOfLastSelectedRow > rowIndex ? -1 : 1;

				while (indexOfLastSelectedRow !== rowIndex) {
					indexOfLastSelectedRow += direction;
					this.grid.settings.selectedRows.push(
						this.grid.settings.indexRows[indexOfLastSelectedRow]
					);
				}
			}
		}

		this.grid.render();
	},

	_moveHeadHandler: function(e) {
		const grid = this.grid;

		const startIndex = e.detail.startIndex;
		const startHeadId = grid.settings.indexHead[startIndex];
		const endIndex = e.detail.endIndex;

		grid.settings.indexHead.splice(startIndex, 1);
		grid.settings.indexHead.splice(endIndex, 0, startHeadId);

		grid.render();
	},

	_sortHandler: function(e) {
		const grid = this.grid;

		const index = e.detail.index;
		const ctrlKey = e.detail.ctrlKey;
		const headId = grid.settings.indexHead[index];
		const orderBy = grid.settings.orderBy;

		let el;
		for (let i = 0; i < orderBy.length; i++) {
			if (orderBy[i].headId === headId) {
				el = orderBy[i];
				break;
			}
		}

		if (!ctrlKey) {
			grid.settings.orderBy = [
				{
					headId: headId,
					desc: el ? !el.desc : false
				}
			];

			return;
		}

		if (el) {
			el.desc = !el.desc;
			grid.settings.orderBy = orderBy;
			return;
		}

		grid.settings.orderBy = orderBy.concat([
			{
				headId: headId,
				desc: false
			}
		]);
	},

	_freezeColumnsHandler: function(e) {
		const frozenColumns = e.detail.frozenColumns;
		this.grid.settings.frozenColumns = frozenColumns;

		this.grid.render();
	},

	_sortFn: function(a, b) {
		const orderBy = this.settings.orderBy;
		for (let i = 0; i < orderBy.length; i++) {
			const prop = orderBy[i].headId;
			const reverse = orderBy[i].desc;
			const result = this.actions._sortRows(a, b, prop, reverse);
			if (result !== 0) {
				return result;
			}
		}
		return 0;
	},

	_sortRows: function(rowAId, rowBId, prop, reverse) {
		const rowA = this.grid.rows._store.get(rowAId);
		const rowB = this.grid.rows._store.get(rowBId);
		if (
			'bigInt' in window &&
			bigInt.isInstance(rowA[prop]) &&
			bigInt.isInstance(rowB[prop])
		) {
			const result = rowA[prop].compare(rowB[prop]);
			if (result !== 0) {
				return reverse ? -1 * result : result;
			}
		}
		if (rowA[prop] < rowB[prop]) {
			return reverse ? 1 : -1;
		}
		if (rowA[prop] > rowB[prop]) {
			return reverse ? -1 : 1;
		}
		return 0;
	}
};

export default GridActions;
