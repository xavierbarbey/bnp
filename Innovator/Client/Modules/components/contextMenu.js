﻿import renderRecursive from './renderRecursive';

function initContextMenu(conMenuInstance, container) {
	conMenuInstance.dom = container.appendChild(document.createElement('div'));
	conMenuInstance.dom.classList.add('aras-contextMenu');
	conMenuInstance.dom.tabIndex = -1;
	conMenuInstance.roots = [];
	const contextMenuCloseHanlder = function() {
		const contextMenuDom = conMenuInstance.dom;
		contextMenuDom.classList.remove('aras-contextMenu_opened');
		contextMenuDom.dispatchEvent(
			new CustomEvent('contextMenuClose', { bubbles: true })
		);
	};

	conMenuInstance.dom.addEventListener('blur', e => {
		const contextMenuDom = conMenuInstance.dom;
		const isChildNodeFocused =
			document.activeElement !== contextMenuDom &&
			contextMenuDom.contains(document.activeElement);
		if (isChildNodeFocused) {
			contextMenuDom.focus();
		} else if (document.activeElement !== contextMenuDom) {
			contextMenuCloseHanlder();
		}
	});
	conMenuInstance.dom.addEventListener('keydown', e => {
		if (e.keyCode === 27) {
			contextMenuCloseHanlder();
			e.preventDefault();
			e.stopPropagation();
		}
	});
}

function updatePosition(docSize, contextMenu, contextMenuProps) {
	if (contextMenuProps.left + contextMenuProps.width > docSize.width) {
		contextMenu.style.right = '0';
		contextMenu.style.left = 'auto';
	}

	if (contextMenuProps.top + contextMenuProps.height < docSize.height) {
		return;
	}

	contextMenu.style.bottom = '0';
	contextMenu.style.top = 'auto';
	contextMenu.style.overflowY = 'auto';
}

class ContextMenu {
	constructor(container) {
		this.data = new Map();
		initContextMenu(this, container || document.body);
	}

	on(event, callback) {
		const handler = e => {
			const node = e.target;
			const targetNode = node.closest('li[data-index]');
			const elementId = targetNode && targetNode.dataset.index;
			callback(elementId, e, this.args);

			if (targetNode && !targetNode.classList.contains('aras-list__parent')) {
				this.dom.classList.remove('aras-contextMenu_opened');
			}
		};
		this.dom.addEventListener(event, handler);
		return () => {
			this.dom.removeEventListener(event, handler);
		};
	}
	show(coords, args) {
		let keys;
		if (this.roots.length) {
			keys = this.roots;
		} else {
			keys = [];
			this.data.forEach(function(element, key) {
				keys.push(key);
			});
		}

		const contextMenuVNode = renderRecursive(this.data, keys);
		if (!contextMenuVNode) {
			return Promise.resolve();
		}

		Inferno.render(contextMenuVNode, this.dom);

		this.args = args;
		this.dom.classList.add('aras-contextMenu_opened');
		this.dom.dispatchEvent(
			new CustomEvent('contextMenuShow', { bubbles: true })
		);
		this.dom.style.top = coords.y + 'px';
		this.dom.style.left = coords.x + 'px';
		this.dom.style.maxHeight = '100%';
		this.dom.style.right = 'auto';
		this.dom.style.bottom = 'auto';
		this.dom.style.overflowY = 'visible';
		const docElement = document.documentElement;
		const docSize = {
			width: docElement.clientWidth,
			height: docElement.clientHeight
		};
		const contextMenuProps = {
			top: coords.y,
			left: coords.x,
			width: this.dom.clientWidth,
			height: this.dom.clientHeight
		};
		updatePosition(docSize, this.dom, contextMenuProps);

		return new Promise(resolve => {
			setTimeout(() => {
				this.dom.focus();
				resolve();
			}, 0);
		});
	}
	applyData(data) {
		const applyRecursive = data => {
			const ids = Object.keys(data);
			ids.forEach(id => {
				const item = data[id];
				if (!Object.keys(item).length) {
					this.data.set(id, { type: 'separator' });
					return;
				}
				if (item.children) {
					const children = applyRecursive.call(this, item.children);
					this.data.set(id, { ...item, children });
					return;
				}
				this.data.set(id, item);
			});
			return ids;
		};

		this.roots = applyRecursive(data);
	}
}

export default ContextMenu;
