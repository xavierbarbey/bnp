﻿export default class Accordion extends HyperHTMLElement {
	constructor(self) {
		self = super(self);
		self._contentNodes = [];
		self._headerNodes = [];
		return self;
	}

	static get booleanAttributes() {
		return ['collapsed'];
	}

	created() {
		if (!this.hasChildNodes()) {
			return;
		}
		const { headerNodes, contentNodes } = Array.from(this.children)
			.concat(this._contentNodes)
			.reduce(
				(data, node) => {
					if (node.matches('[slot="header"]')) {
						data.headerNodes.push(node);
					} else {
						data.contentNodes.push(node);
					}
					return data;
				},
				{ headerNodes: [], contentNodes: [] }
			);
		this._contentNodes = contentNodes;
		this._headerNodes = headerNodes;
	}

	appendChild(child) {
		if (this._isConnected && !this.hasChildNodes()) {
			return super.appendChild(child);
		}

		this._contentNodes.push(child);

		if (this._isConnected) {
			this.render();
		}

		return child;
	}

	removeChild(child) {
		if (this._contentNodes.indexOf(child) === -1) {
			return super.removeChild(child);
		}

		this._contentNodes = this._contentNodes.filter(
			currentChild => currentChild !== child
		);
		this.render();

		return child;
	}

	connectedCallback() {
		this._isConnected = true;
		this.classList.add('aras-accordion');
		this.render();
	}

	attributeChangedCallback(attr) {
		if (attr === 'collapsed') {
			this.render();
		}
	}

	collapse() {
		return this.toggle(true);
	}

	expand() {
		return this.toggle(false);
	}

	render() {
		const className = `aras-accordion__toggle-button aras-icon-arrow aras-icon-arrow_${
			this.collapsed ? 'down' : 'up'
		}`;

		this.classList.toggle('aras-accordion_collapsed', this.collapsed);
		this.html`
			<div class="aras-accordion__header">
				<span
					class="${className}"
					onclick="${() => this.toggle()}"
				></span>
				${this._headerNodes}
			</div>
			<div class="aras-accordion__content">${this._contentNodes}</div>
		`;
	}

	toggle(shouldCollapse) {
		return new Promise(resolve => {
			const toggleButton = this.querySelector('.aras-accordion__toggle-button');
			const accordionContent = this.querySelector('.aras-accordion__content');
			const handler = event => {
				if (event.target !== accordionContent) {
					return;
				}

				this.collapsed = shouldCollapse;
				accordionContent.style.height = '';
				accordionContent.removeEventListener('transitionend', handler);
				resolve();
			};

			shouldCollapse =
				typeof shouldCollapse === 'boolean' ? shouldCollapse : !this.collapsed;

			toggleButton.classList.toggle('aras-icon-arrow_up', !shouldCollapse);
			toggleButton.classList.toggle('aras-icon-arrow_down', shouldCollapse);
			accordionContent.addEventListener('transitionend', handler);

			if (shouldCollapse) {
				accordionContent.style.height = accordionContent.offsetHeight + 'px';
				accordionContent.offsetHeight; // Should reflow
				accordionContent.style.height = '0';
				return;
			}

			this.classList.remove('aras-accordion_collapsed');
			const contentHeight = accordionContent.offsetHeight;

			accordionContent.style.height = '0';
			accordionContent.offsetHeight;
			accordionContent.style.height = contentHeight + 'px';
		});
	}
}
