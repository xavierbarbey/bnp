﻿import { defaultTemplates, toolbarFormatters } from './toolbarTemplates';
import HTMLCustomElement from './htmlCustomElement';
import trimSeparators from './trimSeparators';

export default class Toolbar extends HTMLCustomElement {
	static extendFormatters(formatter) {
		Object.assign(toolbarFormatters, formatter);
	}

	disconnectedCallback() {
		window.removeEventListener('resize', this._resizeHandler);
	}

	init() {
		this.templates = defaultTemplates;
		this.datastore = new Map();
		this._leftContainerItems = [];
		this._rightContainerItems = [];
		this._isToolbarLabeled = false;
		this._renderingPromise = null;
		this._resizeHandler = () => {
			// for correct display of images after resize in IE
			this._updateImages();
			this.render();
		};
		window.addEventListener('resize', this._resizeHandler);
		this.on('click', (parentItemId, event) => {
			this._checkedItemClickHandler(parentItemId, event);
		});
	}

	_checkedItemClickHandler(parentItemId, event) {
		const node = event.target;
		const targetNode = node.closest('li[data-index]');

		if (!targetNode) {
			return Promise.resolve();
		}

		const parentItem = this.data.get(parentItemId);
		const dataMap = parentItem.data;
		const targetNodeIndex = targetNode.dataset.index;
		const targetItem = dataMap.get(targetNodeIndex);

		if (
			!targetItem.disabled &&
			(targetItem.checked !== undefined || targetItem.type === 'checkbox')
		) {
			const groupId = targetItem.group_id;
			if (groupId) {
				dataMap.forEach(item => {
					if (item.group_id !== groupId) {
						return;
					}

					item.checked = false;
					dataMap.set(item.id, Object.assign({}, item));
				});
				targetItem.checked = true;
			} else {
				targetItem.checked = !targetItem.checked;
			}

			dataMap.set(targetItem.id, Object.assign({}, targetItem));
			this.data.set(parentItemId, Object.assign({}, parentItem));
			return this.render();
		}

		return Promise.resolve();
	}

	_render() {
		const root = this.templates.root(this._getVnodeProperties());
		Inferno.render(root, this);
	}

	render() {
		if (this._renderingPromise) {
			return this._renderingPromise;
		}
		this._renderingPromise = Promise.resolve().then(() => {
			this._render();
			this._renderingPromise = null;
		});
		return this._renderingPromise;
	}

	set data(dataMap) {
		const self = this;
		this.datastore = dataMap;
		this._rightContainerItems = [];
		this._leftContainerItems = [];
		this.datastore.forEach(function(value, key) {
			if (value.right) {
				self._rightContainerItems.push(key);
			} else {
				self._leftContainerItems.push(key);
			}
		});
		this.render();
	}

	get data() {
		return this.datastore;
	}

	set container(newContainerItems) {
		this._leftContainerItems = newContainerItems;
		this.render();
	}

	get container() {
		return this._leftContainerItems;
	}

	set rightContainer(newContainerItems) {
		this._rightContainerItems = newContainerItems;
		this.render();
	}

	get rightContainer() {
		return this._rightContainerItems;
	}

	on(eventType, callback) {
		const self = this;
		const handler = function(event) {
			const targetItemId = self._getIdByDomElement(event.target);
			const item = self.data.get(targetItemId);
			if (!item || item.disabled) {
				return;
			}
			if (targetItemId) {
				callback(targetItemId, event);
			}
		};
		this.addEventListener(eventType, handler);
		return function() {
			self.removeEventListener(eventType, handler);
		};
	}

	_getIdByDomElement(element) {
		const targetNodeSelector = '[data-id]';
		const targetNode = element.closest(targetNodeSelector);
		return targetNode ? targetNode.dataset.id : null;
	}

	_getVnodeProperties() {
		const leftContainerItems = trimSeparators(
			this.data,
			this._leftContainerItems
		);
		const rightContainerItems = trimSeparators(
			this.data,
			this._rightContainerItems
		);
		return {
			leftContainerItems,
			rightContainerItems,
			toolbar: this,
			options: {
				isToolbarLabeled: this._isToolbarLabeled
			}
		};
	}

	_updateImages() {
		const images = this.querySelectorAll('.aras-toolbar__image img');
		for (let i = 0; i < images.length; i++) {
			images[i].src = images[i].getAttribute('src');
		}
	}
}
