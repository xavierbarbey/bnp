﻿import GridActions from '../grid/actions';
import utils from '../../core/utils';

const getRemoveCount = function(indexTreeRows, indexRowsItem, expanded) {
	return (indexTreeRows[indexRowsItem] || []).reduce(function(
		removeCount,
		childId
	) {
		const res = removeCount + 1;
		if (expanded.has(childId)) {
			return res + getRemoveCount(indexTreeRows, childId, expanded);
		}
		return res;
	},
	0);
};
const getElementsToAdd = function(indexTreeRows, indexRowsItem, expanded) {
	return (indexTreeRows[indexRowsItem] || []).reduce(function(array, childId) {
		array.push(childId);
		if (expanded.has(childId)) {
			return array.concat(getElementsToAdd(indexTreeRows, childId, expanded));
		}
		return array;
	}, []);
};

function TreeGridActions(grid) {
	GridActions.call(this, grid);
	this._addHandlers([
		{
			target: grid.dom,
			action: 'expand',
			handler: this._expandHandler.bind(this)
		}
	]);
}

const treeGridActionsPrototype = {
	constructor: TreeGridActions,

	_expandHandler: function(e) {
		const rowId = this.grid.settings.indexRows[e.detail.index];
		return this._toggleExpanded(rowId);
	},
	_toggleExpanded: function(rowId) {
		const grid = this.grid;
		const promise = Promise.resolve();
		const sourceItem = grid._rows.get(rowId);
		if (!sourceItem || !sourceItem.children) {
			return promise;
		}
		return promise
			.then(function() {
				if (
					sourceItem.children === true &&
					!grid.settings.expanded.has(rowId)
				) {
					grid.settings.expanded.add(rowId);
					grid.render();
					return grid._buildBranchData(rowId);
				}
				return grid.settings.expanded.has(rowId);
			})
			.then(function(expanded) {
				const rowIndex = grid.settings.indexRows.indexOf(rowId);
				if (rowIndex === -1) {
					return expanded;
				}

				if (expanded) {
					grid.settings.expanded.delete(rowId);
					const removeCount = getRemoveCount(
						grid.settings.indexTreeRows,
						rowId,
						grid.settings.expanded
					);
					grid.settings.indexRows.splice(rowIndex + 1, removeCount);
				} else {
					grid.settings.expanded.add(rowId);
					if (!grid.settings.indexTreeRows[rowId]) {
						grid.settings.indexTreeRows[rowId] = grid._rows
							.get(rowId)
							.children.slice();
						if (grid.settings.orderBy.length) {
							grid.settings.indexTreeRows[rowId].sort(
								grid.actions._sortFn.bind(grid)
							);
						}
					}
					const argumentsArr = [rowIndex + 1, 0].concat(
						getElementsToAdd(
							grid.settings.indexTreeRows,
							rowId,
							grid.settings.expanded
						)
					);
					grid.settings.indexRows.splice(...argumentsArr);
				}
				grid.render();
				return expanded;
			});
	},
	_calcRowsMetadata: function(indexTreeRows, rowId) {
		const metadata = {};
		const calcMetadataFunc = function(key, parent) {
			indexTreeRows[key].forEach(function(child, idx, arr) {
				const lastOne = idx === arr.length - 1;
				metadata[child] = {
					className: lastOne ? 'aras-grid-row_lastChild' : '',
					isLastChildOnLevel: lastOne
				};
				if (parent) {
					const levelLines = (parent.levelLines || []).slice();
					levelLines.push(parent.isLastChildOnLevel ? 0 : 1);
					metadata[child].levelLines = levelLines;
				}

				if (indexTreeRows[child]) {
					calcMetadataFunc(child, metadata[child]);
				}
			});
		};
		const rowMetadata = this.grid.metadata[rowId];
		const rootId = rowMetadata ? rowId : 'roots';
		calcMetadataFunc(rootId, rowMetadata);
		return metadata;
	}
};

TreeGridActions.prototype = utils.mixin(
	Object.create(GridActions.prototype),
	treeGridActionsPrototype
);
export default TreeGridActions;
