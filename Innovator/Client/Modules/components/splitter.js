﻿/**
 * Global object which contains general controls and helpers.
 * The object doesn't contain any business logic
 *
 * @namespace ArasModules
 */
const rowSettings = {
	className: 'aras-splitter_vertical',
	sizeParam: 'width',
	positionCoordinateParam: 'left',
	ghostMoveParam: 'translateX(',
	mouseEventCoordinateParam: 'clientX',
	minSizeParam: 'min-width',
	flexSizeParam: 'flexBasis'
};
const columnSettings = {
	className: 'aras-splitter_horizontal',
	sizeParam: 'height',
	positionCoordinateParam: 'top',
	ghostMoveParam: 'translateY(',
	mouseEventCoordinateParam: 'clientY',
	minSizeParam: 'min-height',
	flexSizeParam: 'height'
};
const DEFAULT_BLOCK_MIN_SIZE = 100;
const DRAGGABLE_CLASS_NAME = 'aras-splitter-ghost_draggable';
// CURSOR_OFFSET need for set cursor upon ':before' element of splitter ghost
const CURSOR_OFFSET = 3;

const getElementStylePropertyValue = function(element, prop) {
	if (element && prop) {
		const inlineStyle = element.style.getPropertyValue(prop);
		return inlineStyle
			? inlineStyle
			: window.getComputedStyle(element).getPropertyValue(prop);
	}
};

/**
 * Get sibling of splitter element, that have fixed width or height and do not have "flex-grow" property.
 * If container has two growing siblings or none, function returns undefined
 *
 * @private
 * @param {DOMElement} element - splitter DOM element
 * @return {DOMElement}, return splitter sibling, that do not have "flex-grow" property
 */
const getFixedSibling = function(element) {
	const prevSiblingGrow = !!Number(
		getElementStylePropertyValue(element.previousElementSibling, 'flex-grow')
	);
	const nextSiblingGrow = !!Number(
		getElementStylePropertyValue(element.nextElementSibling, 'flex-grow')
	);

	if (prevSiblingGrow && nextSiblingGrow) {
		return;
	} else if (nextSiblingGrow) {
		return element.previousElementSibling;
	}
	return element.nextElementSibling;
};

/**
 * Make splitter from received element. Splitter component is based on flex html tags.
 *
 * @function
 * @memberof ArasModules
 * @param {DOMElement} element - DOM element, that needs to be made as splitter
 * @return {boolean} true, if splitter has been made
 */
const splitter = function(element) {
	if (!element) {
		return false;
	}

	const parentNode = element.parentNode;
	const displayType = getElementStylePropertyValue(parentNode, 'display');

	if (displayType !== 'flex' && displayType !== 'inline-flex') {
		return false;
	}

	const direction = getElementStylePropertyValue(parentNode, 'flex-direction');
	let settings;

	if (direction.startsWith('row')) {
		settings = rowSettings;
	} else if (direction.startsWith('column')) {
		settings = columnSettings;
	} else {
		return false;
	}

	const fixedBlock = getFixedSibling(element);

	if (!fixedBlock) {
		return false;
	}

	const parentChildren = parentNode.children;
	const parentChildrenLength = parentChildren.length;
	for (let i = 0; i < parentChildrenLength; i++) {
		const child = parentChildren[i];
		const hasGrow = !!Number(getElementStylePropertyValue(child, 'flex-grow'));
		if (hasGrow) {
			child.style[settings.flexSizeParam] = '0';
		}
	}

	element.classList.add(settings.className);

	const ghost = document.createElement('div');
	ghost.classList.add('aras-splitter-ghost');
	element.appendChild(ghost);

	const minSize =
		parseInt(getElementStylePropertyValue(fixedBlock, settings.minSizeParam)) ||
		DEFAULT_BLOCK_MIN_SIZE;

	const isLeftSibling = fixedBlock === element.previousElementSibling;
	const getSizeOffset = function(ghostPosition) {
		return parseInt(ghostPosition) * (isLeftSibling ? 1 : -1);
	};

	element.addEventListener('mousedown', function(e) {
		ghost.classList.add(DRAGGABLE_CLASS_NAME);
		const splitterBoundingClientRect = element.getBoundingClientRect();
		const splitterCoordinate = Math.round(
			splitterBoundingClientRect[settings.positionCoordinateParam]
		);

		const mouseMoveHandler = function(e) {
			if (!ghost.classList.contains(DRAGGABLE_CLASS_NAME)) {
				return;
			}
			ghost.style.transform =
				settings.ghostMoveParam +
				(e[settings.mouseEventCoordinateParam] -
					CURSOR_OFFSET -
					splitterCoordinate) +
				'px)';
		};

		const mouseUpHandler = function(e) {
			if (ghost.classList.contains(DRAGGABLE_CLASS_NAME)) {
				ghost.classList.remove(DRAGGABLE_CLASS_NAME);

				const oldSize = parseInt(
					getElementStylePropertyValue(fixedBlock, settings.sizeParam)
				);
				const sizeOffset = getSizeOffset(ghost.style.transform.substring(11));
				const newSize = Math.max(oldSize + sizeOffset, minSize);

				fixedBlock.style[settings.sizeParam] = newSize + 'px';

				ghost.style.transform = '';

				document.removeEventListener('mousemove', mouseMoveHandler);
				document.removeEventListener('mouseup', mouseUpHandler);
			}
		};

		document.addEventListener('mousemove', mouseMoveHandler);
		document.addEventListener('mouseup', mouseUpHandler);
		// preventDefault need for disable text selecting when mouse moving
		e.preventDefault();
	});

	return true;
};

export default splitter;
