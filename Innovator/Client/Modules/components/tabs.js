﻿import SvgManager from '../core/SvgManager';
import DragController from './dragTabController';

const tabHelper = {
	controlByScroll: function(elem, movable) {
		const scrollRight =
			movable.scrollWidth - movable.offsetWidth - movable.scrollLeft;
		elem.classList.toggle('aras-tabs_moved-right', scrollRight > 0);
		elem.classList.toggle('aras-tabs_moved-left', movable.scrollLeft > 0);
	},
	scrollLeft: function(elem, movable) {
		movable.scrollLeft = Math.max(0, movable.scrollLeft - movable.clientWidth);
		elem.classList.toggle('aras-tabs_moved-left', movable.scrollLeft !== 0);
		elem.classList.add('aras-tabs_moved-right');
	},
	scrollRight: function(elem, movable) {
		const diffWidth = movable.scrollWidth - movable.clientWidth;
		movable.scrollLeft = Math.min(
			movable.scrollLeft + movable.clientWidth,
			diffWidth
		);
		elem.classList.toggle(
			'aras-tabs_moved-right',
			movable.scrollLeft !== diffWidth
		);
		elem.classList.add('aras-tabs_moved-left');
	},
	scrollIntoView: function(elem, movable, tab) {
		const tabStyle = window.getComputedStyle(tab);
		const leftMargin = parseInt(tabStyle.marginLeft);
		const rightMargin = parseInt(tabStyle.marginRight);
		const tabOffset = tab.offsetLeft - movable.offsetLeft;
		const rightOffset = tabOffset + tab.offsetWidth - movable.clientWidth;

		if (rightOffset + rightMargin - movable.scrollLeft > 0) {
			// Firefox and IE have float tab coordinates: adding 1px to fix offset and count right scrollLeft
			movable.scrollLeft = rightOffset + rightMargin + 1;
		} else if (tabOffset - leftMargin - movable.scrollLeft < 0) {
			movable.scrollLeft = tabOffset - leftMargin;
		}
	},
	scrollByTab: function(elem, movable, tab) {
		if (movable.scrollWidth !== movable.clientWidth) {
			tabHelper.scrollIntoView(elem, movable, tab);
			tabHelper.controlByScroll(elem, movable);
			tabHelper.scrollIntoView(elem, movable, tab);
		}
	}
};

const closeTab = function(tabInstance, tabId) {
	const itemData = tabInstance.data.get(tabId);
	if (itemData && itemData.closable) {
		tabInstance.removeTab(tabId);
	}
};

const close = function(tabInstance, event) {
	const target = event.target;

	if (target.className.includes('aras-icon-close')) {
		const tab = target.closest('li');

		if (tab) {
			const id = tab.getAttribute('data-id');
			closeTab(tabInstance, id);
			event.stopPropagation();
		}
	}
};

const wheelClose = function(tabInstance, event) {
	// Check if wheel(middle button) is clicked
	if (event.button === 1) {
		const tab = event.target.closest('li');
		const id = tab.getAttribute('data-id');
		tab.focus();

		setTimeout(function() {
			closeTab(tabInstance, id);
		}, 0);
	}
};

function getImage(url) {
	SvgManager.load([url]);
	return SvgManager.createInfernoVNode(url) || '';
}

function createListItemContent(item) {
	const content = [];
	if (item.image) {
		content.push(getImage(item.image));
	}
	if (item.closable) {
		content.push(
			<span className="aras-icon-close-block">
				<span className="aras-icon-close" />
			</span>
		);
	}
	return content;
}

function BuildTab(props) {
	const idx = props.idx;
	const item = props.item;
	const classList =
		(props.selected === idx ? 'aras-tabs_active' : '') +
		(item.closable ? ' aras-tabs__closable' : '') +
		(item.disabled ? ' aras-tabs__disabled' : '') +
		(item.cssClass ? ' ' + item.cssClass : '');
	const listItem = {
		'data-id': idx,
		draggable: item.draggable
	};
	let itemContent = props.customizator(idx, props.data) || [];
	itemContent = itemContent.concat(
		createListItemContent(item),
		<span className="aras-tabs__label">{item.label}</span>
	);
	const tooltip = item.tooltip_template || item.label;

	if (tooltip) {
		if (!props.useTooltip) {
			listItem.title = tooltip;
		} else {
			const tooltipSettings = {
				'data-tooltip': tooltip,
				'data-tooltip-pos':
					item.tooltipPosition || props.tooltipSettings.tooltipPosition
			};
			itemContent = (
				<span className="aras-tooltip" {...tooltipSettings}>
					{itemContent}
				</span>
			);
		}
	}

	return (
		<li className={classList} {...listItem}>
			{itemContent}
		</li>
	);
}

const componentLifecycle = {
	onComponentShouldUpdate: function(lastProps, nextProps) {
		return (
			(lastProps.selected !== nextProps.selected &&
				(lastProps.selected === lastProps.idx ||
					nextProps.selected === nextProps.idx)) ||
			lastProps.item !== nextProps.item
		);
	}
};

export default class Tabs extends HyperHTMLElement {
	constructor(props) {
		super(props);
		this.data = new Map();
		this.tabs = [];
		this._elem = this;
		this.draggableTabs = false;
		this.initialized = false;
		this.useTooltip = false;
		this.tooltipSettings = {
			tooltipPosition: 'top'
		};
	}

	static get booleanAttributes() {
		return ['useTooltip', 'vertical'];
	}

	_getImage(url) {
		return getImage(url);
	}
	makeScroll() {
		// Binded handlers
		const controlByScrollBinded = tabHelper.controlByScroll.bind(
			null,
			this._elem,
			this._movable
		);
		const moveScrollLeftBinded = tabHelper.scrollLeft.bind(
			null,
			this._elem,
			this._movable
		);
		const moveScrollRightBinded = tabHelper.scrollRight.bind(
			null,
			this._elem,
			this._movable
		);

		// Attach Events on elements
		window.addEventListener('resize', controlByScrollBinded);
		this._elem.firstElementChild.addEventListener(
			'click',
			moveScrollLeftBinded
		);
		this._elem.lastElementChild.addEventListener(
			'click',
			moveScrollRightBinded
		);

		tabHelper.controlByScroll(this._elem, this._movable);
	}
	removeTab(id) {
		const listItem = this.data.get(id);
		if (!listItem) {
			return Promise.resolve();
		}

		this.data.delete(id);
		const itemIndex = this.tabs.indexOf(id);
		this.tabs.splice(itemIndex, 1);

		if (this.selectedTab === id) {
			const siblingItemId = this.data.has(listItem.parentTab)
				? listItem.parentTab
				: null;
			this.selectTab(siblingItemId || this.tabs[this.tabs.length - 1]);
			const selectEvent = new CustomEvent('select', {
				detail: { id: this.selectedTab }
			});
			this.dispatchEvent(selectEvent);
		}

		return this.render().then(() => {
			if (this._movable) {
				tabHelper.controlByScroll(this._elem, this._movable);
			}
		});
	}
	makeSelectable() {
		const select = event => {
			if (event.button !== 0) {
				return;
			}

			const target = event.target;
			const tab = target.closest('li');
			if (!tab) {
				return;
			}

			const previousSelectedTab = this.selectedTab;
			const id = tab.dataset.id;
			this.selectTab(id);
			if (this.selectedTab === previousSelectedTab) {
				return;
			}

			const selectEvent = new CustomEvent('select', {
				detail: { id: this.selectedTab }
			});
			this.dispatchEvent(selectEvent);
		};
		this._elem.addEventListener('click', select);
	}
	makeDraggable() {
		new DragController(this, tabHelper);
		this.draggableTabs = true;
	}
	selectTab(id) {
		this.selectedTab = id;
		return this.render().then(
			function() {
				const tab = this._elem.querySelector('.aras-tabs_active');
				if (tab) {
					this.scrollIntoView(tab);
				}
			}.bind(this)
		);
	}
	setTabContent(id, props) {
		if (id) {
			const item = this.data.get(id);
			if (item) {
				this.data.set(id, Object.assign({}, item, props));
			}
			return this.render();
		}
		return this.renderPromise || Promise.resolve();
	}
	addTab(id, props) {
		if (id) {
			this.tabs.push(id);
			this.data.set(
				id,
				Object.assign(
					{
						closable: false,
						parentTab: this.selectedTab,
						draggable: this.draggableTabs
					},
					props
				)
			);
			return this.render();
		}
		return this.renderPromise || Promise.resolve();
	}
	scrollIntoView(tab) {
		if (this._movable) {
			tabHelper.scrollByTab(this._elem, this._movable, tab);
		}
	}
	render() {
		if (this.renderPromise) {
			return this.renderPromise;
		}

		this.renderPromise = Promise.resolve().then(() => {
			const defaultProps = {
				customizator: this.tabCustomizator,
				data: this.data,
				selected: this.selectedTab,
				tooltipSettings: this.tooltipSettings,
				useTooltip: this.useTooltip
			};
			const listItems = this.tabs.map(id => {
				const props = {
					...defaultProps,
					idx: id,
					item: this.data.get(id)
				};
				return <BuildTab {...props} ref={{ ...componentLifecycle }} />;
			});

			const list = (
				<ul
					onclick={close.bind({}, this)}
					onmousedown={wheelClose.bind({}, this)}
				>
					{listItems}
				</ul>
			);

			Inferno.render(list, this._movable || this._elem);
			this.renderPromise = null;
		});

		return this.renderPromise;
	}

	tabCustomizator(id, data) {
		return null;
	}

	on(eventType, callback) {
		const handler = event => {
			let tabId;
			if (event.detail && event.detail.id) {
				tabId = event.detail.id;
			} else {
				const target = event.target.closest('li[data-id]');
				tabId = target && target.dataset.id;
			}

			if (tabId) {
				callback(tabId, event);
			}
		};
		this._elem.addEventListener(eventType, handler);
		return () => {
			this._elem.removeEventListener(eventType, handler);
		};
	}
	connectedCallback() {
		if (!this.initialized) {
			if (this.hasAttribute('movable')) {
				this.html`
					<span class="aras-tabs-arrow"></span>
						<div></div>
					<span class="aras-tabs-arrow"></span>
				`;

				this._movable = this._elem.querySelector('div');
				this.makeScroll();
			}
			this.classList.add('aras-tabs');

			this.makeSelectable();
			if (this.hasAttribute('draggable')) {
				this.makeDraggable();
			}
			this.initialized = true;
		}
		this.render();
	}

	attributeChangedCallback(name, oldValue, newValue) {
		if (name === 'vertical') {
			this.classList.toggle('aras-tabs_vertical', !!newValue);
		}

		this.render();
	}
}
