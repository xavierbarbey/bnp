﻿define([
	'dojo/_base/declare'
],
function(declare) {
	return declare('GraphNavigation.GraphTooltip', null, {
		isActive: false,
		tooltipData: null,
		domNode: null,
		_showTimer: null,

		constructor: function(initialParameters) {
			initialParameters = initialParameters || {};

			this._createDomNode(initialParameters.containerNode);
		},

		_createDomNode: function(containerNode) {
			containerNode = containerNode || document.body;
			let tooltipNode = containerNode.ownerDocument.createElement('div');
			tooltipNode.setAttribute('class', 'tooltipControl');
			containerNode.appendChild(tooltipNode);

			this.domNode = tooltipNode;
		},

		showTooltip: function(tooltipData) {
			if (tooltipData) {
				if (!this.isActive) {
					this._cleanupTimer();

					return new Promise(function(resolve) {
						this._showTimer = setTimeout(function() {
							this.tooltipData = tooltipData;
							this._renderTooltip();

							this.domNode.style.left = this.tooltipData.positionX + 'px';
							this.domNode.style.top = this.tooltipData.positionY + 'px';
							this.domNode.classList.add('active');

							this._showTimer = null;
							this.isActive = true;
							resolve();
						}.bind(this), 500);
					}.bind(this));
				} else {
					this._renderTooltip();
				}
			} else {
				this.hideTooltip();
			}
		},

		hideTooltip: function() {
			if (this.isActive) {
				this.tooltipData = null;
				this.domNode.classList.remove('active');

				this.isActive = false;
			} else {
				this._cleanupTimer();
			}
		},

		_cleanupTimer: function() {
			if (this._showTimer) {
				clearTimeout(this._showTimer);
				this._showTimer = null;
			}
		},

		_renderTooltip: function() {
			let dataItems = this.tooltipData.tooltipItems;
			let tooltipContent = '';

			for (let i = 0; i < dataItems.length; i++) {
				let rowData = dataItems[i];

				if (rowData) {
					let rowContent = '';
					rowData = typeof rowData === 'object' ? rowData : {value: rowData};

					if (rowData.value) {
						rowContent += '<span class="rowValue">' + rowData.value + '</span>';
					}

					if (rowContent) {
						tooltipContent += '<div class="tooltipRow">' + rowContent + '</div>';
					}
				}
			}

			this.domNode.innerHTML = tooltipContent;
		}
	});
});
