﻿const paramsObject = window.paramObjectName ? window.opener[paramObjectName] : parent;
var item = paramsObject.item;
var itemId = paramsObject.itemID;
var itemType = paramsObject.itemType;
var itemTypeName = paramsObject.itemTypeName;

function getWindowParameter(parameterName) {
	let allWindowParameters = decodeURIComponent(window.location.search.substring(1)).split('&');
	for (let i = 0; i < allWindowParameters.length; i++) {
		let currentArg = allWindowParameters[i].split('=');
		if (currentArg[0] === parameterName) {
			return currentArg[1];
		}
	}
}

window.addEventListener('load', function() {
	require(['GraphView/Scripts/GraphViewApplicationCore'],
		function(GraphViewApplicationCore) {
			const arasObject = window.aras || parent.aras;

			let applicationCore = new GraphViewApplicationCore({
				contextItem: item,
				arasObject: arasObject,
				gvdId: getWindowParameter('gvdId')
			});
			applicationCore.init();

			applicationCore.loadView(itemId, itemTypeName);
			arasObject.browserHelper.toggleSpinner(document, false);
		}
	);
});
