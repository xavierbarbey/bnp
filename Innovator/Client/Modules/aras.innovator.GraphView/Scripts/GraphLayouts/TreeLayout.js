﻿define('GraphView/Scripts/GraphLayouts/TreeLayout', [
	'dojo/_base/declare',
	'GraphView/Scripts/GraphLayouts/GraphLayoutBase'
],
function(declare, GraphLayoutBase) {
	return declare('GraphNavigation.Layouts.TreeLayout', [GraphLayoutBase], {
		treeLayout: null,
		dataStratifier: null,
		isVertical: null,
		contextItemId: null,

		constructor: function(initialArguments) {
			this.isVertical = initialArguments.isVertical;

			this.treeLayout = this.d3.tree().nodeSize([2, 500]).separation(function(a, b) {
				return (this.isVertical ? a.data.width + b.data.width : a.data.height + b.data.height) / 2;
			}.bind(this));
			this.dataStratifier = this.d3.stratify().parentId(function(node) {
				return (node.id !== this.contextItemId && node._parents.length) ?
					node._parents[0].id : null;
			}.bind(this));
		},

		updateGraphLayout: function(layerData) {
			if (layerData && layerData.nodes && layerData.nodes.length) {
				this.contextItemId = this.ownerControl.getRootNode(layerData).id;

				const graphData = this._buildParentRelationships({
					nodes: layerData.nodes,
					connectors: (this.ownerControl.layerData || layerData).connectors
				});

				const hierarchyData = this.dataStratifier(graphData.nodes)
					.sort(function(a, b) {
						return a.id < b.id ? -1 : 1;
					});
				this.treeLayout(hierarchyData);
				this._applyNodePositions(hierarchyData);
			}
		},

		_buildParentRelationships: function(layerData) {
			Array.prototype.forEach.call(layerData.nodes, function(node) {
				node._parents = [];
			});

			Array.prototype.forEach.call(layerData.connectors, function(connector) {
				if (connector.source.id !== connector.target.id) {
					connector.target._parents = connector.target._parents || [];
					connector.target._parents.push(connector.source);
				}
			});

			return layerData;
		},

		_applyNodePositions: function(computedRoot) {
			const computedNodes = computedRoot.descendants();

			Array.prototype.forEach.call(computedNodes, function(node) {
				node.data.x = (this.isVertical ? node.x : node.y) || 0;
				node.data.y = (this.isVertical ? node.y : node.x) || 0;
			}.bind(this));
		}
	});
});
