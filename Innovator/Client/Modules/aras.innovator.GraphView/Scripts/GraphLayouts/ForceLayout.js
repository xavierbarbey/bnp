﻿define([
	'dojo/_base/declare',
	'GraphView/Scripts/GraphLayouts/GraphLayoutBase'
],
function(declare, GraphLayoutBase) {
	return declare('GraphNavigation.Layouts.ForceLayout', [GraphLayoutBase], {
		_graphLayout: null,
		_forceSettings: {
			alphaMin: 0.1,
			charge: -1000,
			theta: 2,
			nodeDistanceMin: 100
		},

		constructor: function() {
			this._graphLayout = this.d3.forceSimulation()
				.alphaMin(this._forceSettings.alphaMin)
				.force('collide', this.d3.forceCollide().radius(this._nodeDistance.bind(this)))
				.force('link', this.d3.forceLink().id(function(d) { return d.id; }).distance(this._connectorDistance.bind(this)))
				.force('charge', this.d3.forceManyBody().strength(this._forceSettings.charge).theta(this._forceSettings.theta))
				.force('center', this.d3.forceCenter())
				.stop();
		},

		_nodeDistance: function(nodeData) {
			nodeData = nodeData || {};
			return Math.max(nodeData.width, nodeData.height) || this._forceSettings.nodeDistanceMin;
		},

		_connectorDistance: function(connectorData) {
			connectorData = connectorData || {};
			return this._nodeDistance(connectorData.source) + this._nodeDistance(connectorData.target);
		},

		updateGraphLayout: function(layerData) {
			layerData = layerData || {nodes: [], connectors: []};
			this._graphLayout.alpha(1);

			this._graphLayout
				.nodes(layerData.nodes)
				.force('link').links(layerData.connectors);

			for (let i = 0, n = Math.ceil(Math.log(this._graphLayout.alphaMin()) / Math.log(1 - this._graphLayout.alphaDecay())); i < n; ++i) {
				this._graphLayout.tick();
			}
		}
	});
});
