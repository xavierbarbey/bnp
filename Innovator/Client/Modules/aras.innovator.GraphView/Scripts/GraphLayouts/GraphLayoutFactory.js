﻿define([
	'dojo/_base/declare',
	'GraphView/Scripts/GraphLayouts/GraphLayoutsEnum',
	'GraphView/Scripts/GraphLayouts/ForceLayout',
	'GraphView/Scripts/GraphLayouts/TreeLayout'
],
function(declare, GraphLayoutsEnum, ForceLayout, TreeLayout) {
	return declare('GraphNavigation.Layouts.GraphLayoutFactory', null, {
		constructor: function(initialParameters) {
		},

		getLayoutInstance: function(layoutType, constructorParams) {
			constructorParams = constructorParams || {};
			let layoutConstructor;

			switch (layoutType) {
				case GraphLayoutsEnum.LayoutType.HorizontalTree:
					constructorParams.isVertical = false;
					layoutConstructor = new TreeLayout(constructorParams);
					break;
				case GraphLayoutsEnum.LayoutType.VerticalTree:
					constructorParams.isVertical = true;
					layoutConstructor = new TreeLayout(constructorParams);
					break;
				case GraphLayoutsEnum.LayoutType.Force:
					layoutConstructor = new ForceLayout(constructorParams);
					break;
				default:
					layoutConstructor = new ForceLayout(constructorParams);
					break;
			}

			return layoutConstructor;
		}
	});
});
