(function(externalParent) {
	var cryptohash = {};

	function hex(buffer) {
		var hexCodes = [];
		var view = new DataView(buffer);
		for (var i = 0; i < view.byteLength; i += 4) {
			// Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
			var value = view.getUint32(i);
			// toString(16) will give the hex representation of the number without padding
			var stringValue = value.toString(16);
			// We use concatenation and slice for padding
			var padding = '00000000';
			var paddedValue = (padding + stringValue).slice(-padding.length);
			hexCodes.push(paddedValue);
		}

		// Join all the hex strings into one
		return hexCodes.join('');
	}

	function stringToArrayBuffer(str) {
		return new Uint8Array(str.split('').map(function(sym) {
			return sym.charCodeAt();
		}));
	}

	cryptohash.SHA256 = function(str) {
		var buffer;
		if (typeof window.crypto !== 'undefined' && crypto.subtle && (!aras.Browser.isCh() || window.protocol === 'https:')) {
			if (window.TextEncoder) {
				buffer = new TextEncoder('utf-8').encode(str);
			} else {
				buffer = stringToArrayBuffer(str);
			}

			return crypto.subtle.digest('SHA-256', buffer).then(function(hash) {
				return hex(hash);
			});
		} else if (typeof window.msCrypto !== 'undefined') {
			buffer = stringToArrayBuffer(str);
			var sha256 = msCrypto.subtle.digest('SHA-256', buffer);
			return new Promise(function(resolve, reject) {
				sha256.oncomplete = function(event) {
					resolve(hex(event.target.result));
				};
			});
		} else {
			return Promise.resolve(CryptoJS.SHA256(str).toString(CryptoJS.enc.Hex));
		}
	};

	cryptohash.MD5 = function(message, config) {
		return CryptoJS.MD5(message, config);
	};

	cryptohash.xxHash = function() {
		throw new Exception('Cryptohash.xxHash not implemented');
	};

	externalParent.cryptohash = cryptohash;

	window.ArasModules = window.ArasModules || externalParent;
})(window.ArasModules || {});
