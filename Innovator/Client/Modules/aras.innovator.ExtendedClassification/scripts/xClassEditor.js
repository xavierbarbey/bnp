﻿(function() {
	var xClassEditor = {};

	var baseUnlock = window.onUnlockCommand;
	window.onUnlockCommand = function(saveChanges) {
		var baseUnlockResult = baseUnlock(saveChanges);
		var xClasses = aras.getItemRelationships('xClassificationTree', aras.getItemProperty(item, 'id'), 'xClass');
		var xClassEditorContainer = document.querySelector('.xClassEditorContainer');
		if (!xClassEditorContainer.classList.contains('hiddenContainer')) {
			window.xClassEditorTree.init(document.querySelector('.class-tree'), item);
			window.xClassEditorGrid.init(document.querySelector('.properties-grid'), xClasses[0]);
			window.xClassEditorTree.tree.select(aras.getItemProperty(xClasses[0], 'ref_id'));
		}
		window.xClassEditorTree.disableButtonsToolbar();
		return baseUnlockResult;
	};

	var baseLock = window.onLockCommand;
	window.onLockCommand = function(saveChanges) {
		var baseLockResult = baseLock(saveChanges);
		var xClasses = aras.getRelationships(item, 'xClass');
		if (xClasses.length === 0) {
			xClasses = aras.getItemRelationships('xClassificationTree', aras.getItemProperty(item, 'id'), 'xClass');
		}
		window.xClassEditorTree.item = item;
		var xClassEditorContainer = document.querySelector('.xClassEditorContainer');
		if (!xClassEditorContainer.classList.contains('hiddenContainer')) {
			window.xClassEditorGrid.init(document.querySelector('.properties-grid'), xClasses[0]);
			window.xClassEditorTree.tree.select(aras.getItemProperty(xClasses[0], 'ref_id'));
		}
		window.xClassEditorTree.disableButtonsToolbar();
		return baseLockResult;
	};

	var baseSave = window.onSaveCommand;
	window.onSaveCommand = function(saveChanges) {
		var permissions = window.item.selectNodes('Relationships/Item/Relationships/Item[@type="xClass_xPropValue_Perm" or @type="xClass_Classification_Perm"]');
		Array.prototype.forEach.call(permissions, function(perm) {
			var related = aras.getItemProperty(perm, 'related_id');
			if (!related) {
				perm.parentNode.removeChild(perm);
			}
		});
		var baseSaveResult = baseSave(saveChanges);
		return baseSaveResult.then(function() {
			aras.getItemRelationships('xClassificationTree', aras.getItemProperty(item, 'id'), 'xClass');
			window.xClassEditorTree.item = item;
			var loadedClassId = window.xClassEditorGrid.getCurrentLoadedClassId();
			if (loadedClassId) {
				var xClassItem = window.item.selectSingleNode('Relationships/Item[@id="' + window.xClassEditorGrid.getCurrentLoadedClassId() + '"]');
				window.xClassEditorGrid.loadRelationships(xClassItem);
				window.xClassEditorTree.tree.select(aras.getItemProperty(xClassItem, 'ref_id'));
			}
		});
	};

	xClassEditor.init = function(node) {
		window.ArasModules.splitter(document.querySelector('.splitter'));
		var xClasses = aras.getItemRelationships('xClassificationTree', aras.getItemProperty(item, 'id'), 'xClass');
		window.xClassEditorTree.init(document.querySelector('.class-tree'), item);
		window.xClassEditorGrid.init(document.querySelector('.properties-grid'), xClasses[0]);
		window.xClassEditorTree.tree.select(aras.getItemProperty(xClasses[0], 'ref_id'));
		window.xClassEditorTree.disableButtonsToolbar();

		window.xClassEditorTree.onSelectNode = function(xClassId) {
			if (window.xClassEditorGrid.getCurrentLoadedClassId() != xClassId) {
				var xClassItem = window.item.selectSingleNode('Relationships/Item[@id="' + xClassId + '"]');
				window.xClassEditorGrid.loadRelationships(xClassItem);
				window.xClassEditorTree.tree.select(aras.getItemProperty(xClassItem, 'ref_id'));
			}
		};
		window.xClassEditorTree.onDeleteNode = function(deletedId, parentClass) {
			if (window.xClassEditorGrid.getCurrentLoadedClassId() === deletedId) {
				window.xClassEditorGrid.loadRelationships(parentClass);
				window.xClassEditorTree.tree.select(aras.getItemProperty(parentClass, 'ref_id'));
			}
		};
	};

	function toggleContainers(showForm) {
		var formContainer = document.getElementById('CenterBorderContainer');
		if (formContainer) {
			if (showForm) {
				formContainer.classList.remove('hiddenContainer');
			} else {
				formContainer.classList.add('hiddenContainer');
			}
		}

		var xClassEditorContainer = document.querySelector('.xClassEditorContainer');
		if (xClassEditorContainer) {
			if (showForm) {
				xClassEditorContainer.classList.add('hiddenContainer');
			} else {
				xClassEditorContainer.classList.remove('hiddenContainer');
			}
		}
	}

	xClassEditor.showForm = function() {
		toggleContainers(true);
	};

	xClassEditor.showEditor = function() {
		toggleContainers(false);
		xClassEditor.init();
	};

	window.xClassEditor = xClassEditor;
})();
