﻿const soapConfig = {
	method: 'ApplyMethod',
	async: true
};
const cacheFavoriteItems = (dataMap, favoriteItemJson) => {
	const favoriteItems = JSON.parse(favoriteItemJson);
	favoriteItems.forEach(favoriteItem => {
		if (favoriteItem.id) {
			dataMap.set(favoriteItem.id, favoriteItem);
		}
	});
};
const sortFavoriteItems = (a, b) => {
	if (a.label > b.label) {
		return 1;
	}

	if (a.label < b.label) {
		return -1;
	}

	return 0;
};
class FavoritesManager {
	constructor(ArasModules, arasMainWindowInfo) {
		this.ArasModules = ArasModules;
		this.arasMainWindowInfo = arasMainWindowInfo;
		this.data = new Map();

		cacheFavoriteItems(this.data, arasMainWindowInfo.favoriteItems);
		cacheFavoriteItems(this.data, arasMainWindowInfo.favoriteItemTypes);
		cacheFavoriteItems(this.data, arasMainWindowInfo.favoriteSearches);
	}

	async add(favoriteCategory, favoriteItemData) {
		if (!favoriteCategory || !favoriteItemData) {
			return null;
		}

		if (favoriteItemData.savedSearchItemAml) {
			favoriteItemData.savedSearchItem = this.ArasModules.xmlToJson(
				favoriteItemData.savedSearchItemAml
			);
			delete favoriteItemData.savedSearchItemAml;
		}

		const requestXml = this.ArasModules.jsonToXml({
			Item: {
				'@attrs': {
					type: 'Method',
					action: 'Fav_AddFavoriteItem'
				},
				category: favoriteCategory,
				...favoriteItemData
			}
		});
		const favoriteItemResult = await this.ArasModules.soap(
			requestXml,
			soapConfig
		);

		const favoriteItem = JSON.parse(favoriteItemResult.text);
		this.data.set(favoriteItem.id, favoriteItem);
		return favoriteItem;
	}
	async delete(favoriteId) {
		if (!favoriteId) {
			return null;
		}

		const requestXml = this.ArasModules.jsonToXml({
			Item: {
				'@attrs': {
					type: 'Method',
					action: 'Fav_DeleteFavoriteItem'
				},
				id: favoriteId
			}
		});

		const favoriteDeleteResult = await this.ArasModules.soap(
			requestXml,
			soapConfig
		);
		this.data.delete(favoriteId);
		return favoriteDeleteResult;
	}
	async removeFromQuickAccess(favoriteId) {
		if (!favoriteId) {
			return null;
		}

		const storedFavoriteItem = this.data.get(favoriteId);
		if (storedFavoriteItem && storedFavoriteItem.quickAccessFlag === '0') {
			return null;
		}

		await this.ArasModules.soap(
			`<Item type="Favorite" action="edit" id="${favoriteId}">
				<quick_access_flag>0</quick_access_flag>
			</Item>`,
			{ async: true }
		);
		const updatedFavoriteItem = {
			...storedFavoriteItem,
			quickAccessFlag: '0'
		};
		this.data.set(favoriteId, updatedFavoriteItem);
		return updatedFavoriteItem;
	}
	get(favoriteId) {
		if (!favoriteId) {
			return null;
		}

		return this.data.get(favoriteId) || null;
	}
	getDataMap(favoriteCategory, filterCriteria) {
		if (!favoriteCategory) {
			return this.data;
		}

		filterCriteria = {
			category: favoriteCategory,
			...filterCriteria
		};

		const sortedItems = [];
		this.data.forEach(favoriteItem => {
			for (const criterion in filterCriteria) {
				if (favoriteItem[criterion] !== filterCriteria[criterion]) {
					return;
				}
			}

			sortedItems.push(favoriteItem);
		});

		const resultMap = new Map();
		sortedItems.sort(sortFavoriteItems).forEach(favoriteItem => {
			resultMap.set(favoriteItem.id, favoriteItem);
		});

		return resultMap;
	}
}

export default FavoritesManager;
