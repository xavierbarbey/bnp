﻿import Tabs from '../../components/tabs';

export default class HeaderTabs extends Tabs {
	constructor(props) {
		super(props);
		this.data = new Map();
		this.tabs = [];
		this.draggableTabs = false;
		this.initialized = false;
	}
	connectedCallback() {
		if (!this.initialized) {
			this.html`
				<div class="aras-tabs aras-tabs_a aras-flex-grow">
					<span class="aras-tabs-arrow"></span>
					<div>
					</div>
					<span class="aras-tabs-arrow"></span>
				</div>
				<aras-dropdown closeonclick position="bottom-right" id="tabs-dropdown" class="aras-dropdown-container">
					<div class="tabs-button" dropdown-button></div>
					<div class="aras-dropdown">
					</div>
				</aras-dropdown>
			`;

			const dropdownContainer = document.getElementById('tabs-dropdown');
			dropdownContainer.addEventListener(
				'dropdownbeforeopen',
				function() {
					this.dropdownTabRender();
				}.bind(this)
			);

			this._dropdownBox = dropdownContainer.querySelector('div.aras-dropdown');
			this._frameCssClass = 'tabs_content-iframe';
			this._elem = this.firstElementChild;
			this._movable = this._elem.querySelector('div');

			this.makeScroll();
			this.makeSelectable();
			this.makeExpandable();
			this.makeDraggable();
			if (!aras.Browser.isEdge()) {
				this.tabCustomizator = function(tabName) {
					const tabType = tabName.split('_').shift();
					const typesWithoutExpand = ['search', 'form', 'page', 'es'];
					if (typesWithoutExpand.indexOf(tabType) !== -1) {
						return null;
					}
					return [<span className="aras-tab-expand" />];
				};
			}

			this.initialized = true;
		}
		this.render();
	}
	openForm(formId, icon, label) {
		if (!formId) {
			return null;
		}
		const formName = aras.MetadataCache.GetFormName(formId);
		const tab = this.tabs.find(function(tab) {
			return tab.indexOf('form_' + formId + formName + '_') !== -1;
		});
		if (tab) {
			this.selectTab(tab);
			return;
		}
		const url = aras.getScriptsURL(
			'ShowFormInFrame.html?formId=' + formId + '&formType=edit&item=undefined'
		);
		const winName = 'form_' + formId + formName + '_' + Date.now();
		const win = this.open(url, winName, false, ' tabs_content-iframe-page');
		this.updateTitleTab(winName, { label: label, image: icon });

		return win;
	}
	openPage(url, itemTypeId, icon, label) {
		if (!url) {
			return null;
		}
		const itemTypeName = aras.getItemTypeName(itemTypeId) || '';
		const tab = this.tabs.find(function(tab) {
			return tab.indexOf('page_' + itemTypeName + '_' + url) !== -1;
		});
		if (tab) {
			this.selectTab(tab);
			return;
		}
		const winName = 'page_' + itemTypeName + '_' + url + '_' + Date.now();
		url = aras.getScriptsURL(url);
		const win = this.open(url, winName, false, ' tabs_content-iframe-page');
		this.updateTitleTab(winName, { label: label, image: icon });

		return win;
	}
	openSearch(itemtypeID, savedSearchID) {
		if (!itemtypeID) {
			return null;
		}
		const itemTypeDescriptor = aras.getItemTypeForClient(itemtypeID, 'id');
		if (itemTypeDescriptor.getItemCount() < 1) {
			return null;
		}
		const itemTypeName = itemTypeDescriptor.getProperty('name');
		const tabLabel =
			itemTypeDescriptor.getProperty('label_plural') || itemTypeName;
		const url = aras.getScriptsURL(
			'itemsGrid.html?itemtypeID=' +
				itemtypeID +
				'&itemtypeName=' +
				itemTypeName +
				(savedSearchID ? '&savedSearchId=' + savedSearchID : '')
		);

		const winName = 'search_' + itemtypeID + itemTypeName + '_' + Date.now();

		const win = this.open(url, winName, false);
		const frameElement = win.frameElement;

		const frameLoadHandler = function() {
			const shortcutSettings = {
				windows: [win],
				context: window
			};
			window.registerShortcutsAtMainWindowLocation(shortcutSettings);
			frameElement.removeEventListener('load', frameLoadHandler);
		};

		win.addEventListener('resize', () => {
			window.dispatchEvent(new CustomEvent('resize'));
			this.render().then(() => {
				const tab = this._elem.querySelector('.aras-tabs_active');
				if (tab) {
					this.scrollIntoView(tab);
				}
			});
		});

		frameElement.addEventListener('load', frameLoadHandler);

		this.updateTitleTab(winName, {
			label: tabLabel,
			image: '../images/GridSearch.svg'
		});

		return win;
	}
	_hideAll() {
		Array.prototype.forEach.call(
			document.getElementsByClassName(this._frameCssClass),
			function(iframe) {
				iframe.style.opacity = 0;
				iframe.style['z-index'] = '-1';
			}
		);
	}
	makeExpandable() {
		const expand = function(event) {
			const target = event.target;
			if (target.className === 'aras-tab-expand') {
				event.stopPropagation();
				const tab = target.parentNode;
				this.clickOpenInTearOff(tab.getAttribute('data-id'));
			}
		}.bind(this);
		this._elem.addEventListener('click', expand, true);
	}
	selectTab(id) {
		if (!id) {
			return;
		}

		super.selectTab(id);
		this._hideAll();
		document.getElementById(id).style.opacity = 1;
		document.getElementById(id).style['z-index'] = 'auto';
	}
	removeTab(id, ignorePageCloseHooks = false) {
		const frame = document.getElementById(id);
		if (!frame) {
			return Promise.resolve();
		}

		const remove = () => {
			super.removeTab(frame.id);
			const itemId = frame.id.replace(aras.mainWindowName + '_', '');
			aras.uiUnregWindowEx(itemId);
			frame.src = 'about:blank';
			frame.parentNode.removeChild(frame);
		};
		if (frame.contentWindow.close.toString().indexOf('[native code]') > -1) {
			remove();
			return Promise.resolve();
		}

		return new Promise(function(resolve) {
			frame.contentWindow.close(function(isClosed) {
				if (isClosed) {
					remove();
				}
				resolve(isClosed);
			}, ignorePageCloseHooks);
		});
	}
	clickOpenInTearOff(id) {
		if (id) {
			const win = document.getElementById(id).contentWindow;
			if (!win.windowType) {
				const selectItem = win.item;
				const itemId = selectItem.getAttribute('id');
				const itemType = selectItem.getAttribute('type');
				const ignorePageCloseHooks = true;

				return this.removeTab(id, ignorePageCloseHooks).then(function(
					isClosed
				) {
					if (isClosed) {
						const gettedItem = aras.getItemById(itemType, itemId);
						const openInTearOff = true;
						aras.uiShowItemEx(gettedItem, 'tab view', openInTearOff);
					}
				});
			}
			return this.openNonItemWindow(win, id);
		}
	}
	openNonItemWindow(win, id) {
		const itemId = win.itemId;
		const itemTypeName = win.itemTypeName;
		const windowType = win.windowType;
		return this.removeTab(id).then(function(isClosed) {
			if (isClosed) {
				if (windowType === 'whereUsed' || windowType === 'structureBrowser') {
					window.Dependencies.view(
						itemTypeName,
						itemId,
						windowType === 'whereUsed',
						aras,
						true
					);
				}
			}
		});
	}
	setTitleTabWithFrame(frameWindow) {
		let itemTypeName;
		let item;

		if (frameWindow.item && frameWindow.itemTypeName) {
			item = frameWindow.item;
			itemTypeName = frameWindow.itemTypeName;
		} else {
			const win = window[frameWindow.paramObjectName];
			itemTypeName = window[frameWindow.paramObjectName].itemTypeName;
			item = win.item;
		}

		const winName = frameWindow.paramObjectName.replace('_params', '');
		const itemTypeNd = aras.getItemTypeDictionary(itemTypeName).node;
		const itemTypeImgSrc =
			aras.getItemProperty(itemTypeNd, 'open_icon') ||
			'../images/DefaultItemType.svg';
		const keyedName = aras.getKeyedNameEx(item);
		const props = {
			label: keyedName,
			image: itemTypeImgSrc
		};
		this.updateTitleTab(winName, props);
	}
	updateTitleTab(id, props) {
		this.setTabContent(id, props).then(
			function() {
				const tab = this.querySelector('li[data-id="' + id + '"]');
				if (tab) {
					this.scrollIntoView(tab);
				}
			}.bind(this)
		);
	}
	open(src, winName, isUnfocused, className) {
		const topWin = window.main || window;
		const iframe = topWin.document.createElement('IFRAME');
		const isCh = aras.Browser.isCh();
		if (isCh) {
			topWin.document.body
				.querySelector('#main-container #center')
				.appendChild(iframe);
		}
		this.addTab(winName, { closable: true });
		if (!winName.startsWith('innovator_')) {
			const addedTab = this.data.get(winName);
			addedTab.parentTab = null;
		}
		const topPosition = this.offsetHeight;
		const paddings = className ? 24 : 0;
		iframe.style.top = topPosition + 'px';
		iframe.style.height = 'calc(100% - ' + (topPosition + paddings) + 'px)';
		iframe.id = winName;
		iframe.className = className
			? this._frameCssClass + className
			: this._frameCssClass;
		iframe.src = src;
		if (!isCh) {
			topWin.document.body
				.querySelector('#main-container #center')
				.appendChild(iframe);
		}
		if (isUnfocused) {
			iframe.style.opacity = 0;
			iframe.style['z-index'] = '-1';
		} else {
			this.selectTab(winName);
		}
		const win = iframe.contentWindow;
		win.opener = topWin;
		win.name = iframe.name = winName;
		return win;
	}
	dropdownTabRender() {
		const select = function(event) {
			const target = event.target;
			const listTab = target.closest('li');

			if (listTab) {
				this.selectTab(listTab.getAttribute('list-data-id'));
			}
		}.bind(this);

		const items = this.tabs.map(
			function(tab) {
				const data = this.data.get(tab);
				const icon = this._getImage(data.image);
				if (icon) {
					icon.className = 'aras-list-item__icon';
				}
				const className =
					'aras-list-item aras-list-item_iconed' +
					(this.selectedTab === tab ? ' selected' : '');

				return (
					<li className={className} list-data-id={tab}>
						<span className="condition-icon aras-icon-radio" />
						{icon}
						{data.label}
					</li>
				);
			}.bind(this)
		);
		const list = (
			<ul className="aras-list" onmousedown={select}>
				{items}
			</ul>
		);
		Inferno.render(list, this._dropdownBox);
	}
	updateTabInformation(currentID, newID) {
		const index = this.tabs.indexOf(currentID);
		if (index !== -1) {
			this.tabs[index] = newID;
		}

		const dataObj = this.data.get(currentID);
		this.data.delete(currentID);
		this.data.set(newID, Object.assign({}, dataObj));

		this.selectedTab = newID;

		const frame = document.getElementById(currentID);
		frame.id = frame.name = newID;
		this.render();
	}
	getSearchGridTabs(id) {
		return this.tabs
			.filter(function(item) {
				return item.indexOf('search_' + id) > -1;
			})
			.map(function(item) {
				return window.document.getElementById(item).contentWindow;
			});
	}
	render() {
		const isTabOpen = this.tabs.length > 0;
		this.classList.toggle('content-block__main-tabs_hidden', !isTabOpen);
		return super.render();
	}
	forceCloseAllTabs() {
		this.tabs.forEach(id => {
			this.data.delete(id);
			const frame = window.document.getElementById(id);
			const itemId = frame.id.replace(aras.mainWindowName + '_', '');
			aras.uiUnregWindowEx(itemId);
			frame.src = 'about:blank';
		});
	}
}
