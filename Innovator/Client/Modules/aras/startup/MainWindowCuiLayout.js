﻿import {
	contentTabId,
	getSidebarData,
	updateLockedStatus
} from '../../NavigationPanel/sidebarDataConverter';
import cuiContextMenu from '../../cui/cuiContextMenu';
import CuiLayout from '../../cui/CuiLayout';
import cuiMethods from '../../cui/cuiMethods';
import cuiToc from '../../cui/cuiToc';
import cuiToolbar from '../../cui/cuiToolbar';

export default class MainWindowCuiLayout extends CuiLayout {
	_initTocPromise = null;
	navigationPanel = document.getElementById('navigationPanel');

	async init() {
		sessionStorage.setItem('defaultDState', 'defaultDState');

		const initHeader = this._initializeHeader();
		const initToc = this._initializeToc();
		const initContextMenu = this._initializeContextMenu();

		const headerControl = await initHeader;
		this._registerHeaderHandlers(headerControl);

		await initToc;
		window.selectStartPage();
		this._registerTocHandlers();

		const contextMenu = await initContextMenu;
		this._registerContextMenuHandlers(contextMenu);

		const initSidebar = this._initializeSidebar();
		this._registerSidebarHandlers();

		return Promise.all([initHeader, initToc, initSidebar, initContextMenu]);
	}

	updateCuiLayoutOnItemChange(itemTypeName) {
		const itemTypesRequiringCuiUpdate = aras.getMorphaeList(
			aras.getItemTypeForClient('CuiDependency', 'name').node
		);
		const needToCuiUpgrade = itemTypesRequiringCuiUpdate.some(({ name }) => {
			return name === itemTypeName;
		});
		if (needToCuiUpgrade) {
			this.observer.notify('reInitByCUIDependencies');
		}
	}

	_initializeContextMenu() {
		return cuiContextMenu(
			this.navigationPanel.popupMenu,
			'PopupMenuMainWindowTOC',
			this.options
		);
	}

	async _initializeHeader() {
		const toolbarComponent = document.getElementById('headerCommandsBar');
		await cuiToolbar(toolbarComponent, 'MainWindowHeader', this.options);
		toolbarComponent.firstChild.classList.add('aras-header');

		return toolbarComponent;
	}

	_initializeSidebar() {
		const tabs = this.navigationPanel.tabs;
		const navData = this.navigationPanel.nav.data;
		const favoriteItemTypes = window.favorites.getDataMap('ItemType');
		const sidebarData = getSidebarData(favoriteItemTypes, navData);
		sidebarData.forEach((item, id) => {
			tabs.addTab(id, item);
		});
		tabs.selectTab(contentTabId);

		return tabs.render();
	}

	async _initializeToc() {
		if (this._initTocPromise) {
			return this._initTocPromise;
		}

		const nav = this.navigationPanel.nav;
		this._initTocPromise = cuiToc(nav, 'TOC', this.options);

		await this._initTocPromise;
		this._initTocPromise = null;

		return this._initTocPromise;
	}

	_registerContextMenuHandlers(contextMenu) {
		const navigationPanel = this.navigationPanel;
		const sidebar = navigationPanel.tabs;
		const pinnedItemTypeMethods = {
			pinItemType: (id, tabData) => sidebar.addTab(id, tabData),
			unpinItemType: id => {
				const tabsData = sidebar.data;
				const removedTab = tabsData.get(id);
				sidebar.removeTab(id);
				return removedTab;
			},
			getItemTypeByItemTypeId: itemTypeId => {
				let tabId;
				const tabsData = sidebar.data;
				tabsData.forEach((data, id) => {
					if (data.itemTypeId === itemTypeId) {
						tabId = id;
					}
				});
				return tabsData.get(tabId);
			},
			favorites: window.favorites
		};
		const nav = navigationPanel.nav;
		nav.on('contextmenu', async (itemKey, event) => {
			event.preventDefault();
			await nav.select(itemKey);
			const dataItem = nav.data.get(itemKey);
			contextMenu.show(
				{ x: event.clientX, y: event.clientY },
				{ currentTarget: dataItem, ...pinnedItemTypeMethods }
			);
		});
		sidebar.on('contextmenu', (itemKey, event) => {
			event.preventDefault();
			const dataItem = sidebar.data.get(itemKey);
			contextMenu.show(
				{ x: event.clientX, y: event.clientY },
				{ currentTarget: dataItem, ...pinnedItemTypeMethods }
			);
		});
	}

	_registerHeaderHandlers(headerControl) {
		this.observer.subscribe(event => {
			switch (event) {
				case 'reInitByCUIDependencies':
					this._initializeHeader();
					break;
				case 'UpdatePreferences':
					cuiMethods.reinitControlItems(headerControl, event, this.options);
					break;
			}
		});
		aras.registerEventHandler('PreferenceValueChanged', window, () => {
			this.observer.notify('UpdatePreferences');
		});
	}

	_registerSidebarHandlers() {
		this.observer.subscribe(async event => {
			if (event !== 'UpdateTOC') {
				return;
			}

			await this._initializeToc();
			const nav = this.navigationPanel.nav;
			const tabs = this.navigationPanel.tabs;
			updateLockedStatus(tabs.data, nav.data);
			tabs.render();
		});
	}

	_registerTocHandlers() {
		this.observer.subscribe(async event => {
			if (event !== 'UpdateTOC') {
				return;
			}

			aras.MetadataCache.DeleteConfigurableUiDatesFromCache();
			await this._initializeToc();
			this.navigationPanel.render();
		});
	}
}
