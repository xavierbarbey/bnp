﻿import notCertifiedBrowserDialog from '../dialogs/notCertifiedBrowser';
import getLocalStorage from './getLocalStorage';

const storageKey = 'ArasAppUrlsKeys';
const getUrlsKeys = () => {
	const localStorage = getLocalStorage();
	const strArray = localStorage.getItem(storageKey);
	let array = [];
	try {
		if (strArray) {
			array = JSON.parse(strArray);
		}
	} catch (exp) {
		return array;
	}

	return array;
};

const getBaseUrl = () =>
	window.location.href.replace(/Client\/.*/i, 'Client').toLowerCase();
const isInLocalStorage = () => getUrlsKeys().indexOf(getBaseUrl()) > -1;
const isInSessionStorage = () =>
	sessionStorage.getItem('skipBrowserCertificationDialog');
const addToLocalStorage = () => {
	const baseUrl = getBaseUrl();
	const arrayUrls = getUrlsKeys();

	if (arrayUrls.indexOf(baseUrl) === -1) {
		arrayUrls.push(baseUrl);
	}

	const localStorage = getLocalStorage();
	localStorage.setItem(storageKey, JSON.stringify(arrayUrls));
};

const validateBrowserCertified = () => {
	if (
		aras.Browser.isCertified() ||
		isInLocalStorage() ||
		isInSessionStorage()
	) {
		return Promise.resolve();
	}

	const dialogPromise = notCertifiedBrowserDialog();
	dialogPromise.then(rememberChoice => {
		if (rememberChoice) {
			addToLocalStorage();
		} else {
			sessionStorage.setItem('skipBrowserCertificationDialog', true);
		}
	});

	return dialogPromise;
};

export default validateBrowserCertified;
