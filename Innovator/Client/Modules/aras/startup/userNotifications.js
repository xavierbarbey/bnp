﻿export default class UserNotificationContainer {
	constructor(control) {
		if (!control) {
			throw new Error(
				"ArgumentException: notification_control can't be undefined"
			);
		}
		const messageCheckInterval = aras.commonProperties.MessageCheckInterval;

		if (!messageCheckInterval) {
			ArasModules.Dialog.alert(
				aras.getResource('', 'user_notifications.failed_get_interval_variable')
			);
			return;
		}
		const clickHandler = (targetId, evt) => {
			if (targetId !== this.controlButtonName) {
				return;
			}
			const listOption = evt.target.closest('li[data-index]');
			if (!listOption) {
				return;
			}

			const existedItem = this.notificationControl.datastore.get(
				this.controlButtonName
			);
			const messageId = listOption.dataset.index;
			const message = existedItem.data.get(messageId);

			const itemNode = aras.newItem(message.type);
			itemNode.setAttribute('id', message.id);
			aras.uiShowItemEx(itemNode, 'notification');
		};

		control.on('click', clickHandler);

		this.notificationControl = control;
		this.controlButtonName =
			'com.aras.innovator.cui_default.mwh_header_notifications_button';
		this.checkInterval = null;
		this.checkTimeout = null;
		this.acknowledge = 'Acknowledge';
		this.сloseText = 'Close';
		this.url =
			aras.getInnovatorUrl() +
			'NotificationServer/UserNotifications/ProxyPage.aspx';
		this.updateInfoDom = null;
		this.popupParametersArray = [];
		this.checkInterval = messageCheckInterval;
		this.checkTimeout = setTimeout(
			this.startIterativeUpdateMessageCollection.bind(this),
			this.checkInterval
		);
		this.updateMessageCollection(true, function() {});
	}
	updateControlDataset(handlerFunction) {
		const existedItem = this.notificationControl.datastore.get(
			this.controlButtonName
		);
		const newItem = handlerFunction(existedItem);
		this.notificationControl.datastore.set(
			this.controlButtonName,
			Object.assign({}, newItem)
		);
		this.notificationControl.render();
	}
	addMessage(id, text, image, messageItem) {
		const acknowledgeDom =
			messageItem && messageItem.selectSingleNode('acknowledge');
		const isOnceAcknowledge = acknowledgeDom && acknowledgeDom.text === 'Once';
		const type = messageItem.getAttribute('type');

		this.updateControlDataset(dataItem => {
			if (dataItem.roots.indexOf(id) === -1) {
				dataItem.roots.push(id);
				if (!dataItem.data) {
					dataItem.data = new Map();
				}
			}
			dataItem.data.set(id, {
				id: id,
				type,
				label: text,
				name: id,
				icon: image,
				isOnce: isOnceAcknowledge,
				isOld: false
			});
			return dataItem;
		});
	}
	removeMessage(id) {
		this.updateControlDataset(dataItem => {
			const itemIndex = dataItem.roots.indexOf(id);
			if (itemIndex !== -1) {
				dataItem.roots.splice(itemIndex, 1);
				dataItem.data.delete(id);
			}
			return dataItem;
		});
	}
	clearCollection() {
		this.updateControlDataset(dataItem => {
			if (dataItem.data) {
				dataItem.roots = [];
				dataItem.data = new Map();
			}
			return dataItem;
		});
	}
	makeAllMessagesOld() {
		this.updateControlDataset(dataItem => {
			if (dataItem.data) {
				const newData = new Map();
				dataItem.data.forEach(function(value, key) {
					newData.set(
						key,
						Object.assign({}, value, {
							isOld: true
						})
					);
				});
				dataItem.data = newData;
			}
			return dataItem;
		});
	}
	clearOldMessages() {
		this.updateControlDataset(dataItem => {
			if (dataItem.data) {
				const newData = new Map();
				const newRoots = [];
				dataItem.data.forEach(function(value, key) {
					if (value.isOld) {
						return;
					}
					newData.set(key, value);
					newRoots.push(key);
				});
				dataItem.data = newData;
				dataItem.roots = newRoots;
			}
			return dataItem;
		});
	}
	getMessageQuery() {
		const queryDom = aras.createXMLDocument();

		queryDom.loadXML('<Parameters/>');
		const currentUserIDNode = queryDom.createElement('CurrentUserID');
		const dateTimeNode = queryDom.createElement('DateTime');
		const intlObject = ArasModules.intl;

		currentUserIDNode.text = aras.getCurrentUserID();
		dateTimeNode.text = intlObject.date.toIS0Format(new Date());

		queryDom.documentElement.appendChild(currentUserIDNode);
		queryDom.documentElement.appendChild(dateTimeNode);

		return queryDom.documentElement.xml;
	}
	asyncCheckNewMessages(getCollectionCallback) {
		const resultCallbackHandler = soapResult => {
			const messageCollection = this.processSoapResult(soapResult);
			getCollectionCallback(this, messageCollection);
		};

		const soapController = new window.SoapController(resultCallbackHandler);

		const query = this.getMessageQuery();
		window.aras.soapSend(
			'GetNotifications',
			query,
			this.url,
			null,
			soapController
		);
	}
	syncGetMessageCollection() {
		const query = this.getMessageQuery();
		const soapResult = parent.aras.soapSend(
			'GetNotifications',
			query,
			this.url
		);

		return this.processSoapResult(soapResult);
	}
	processSoapResult(soapResult) {
		if (soapResult.getFaultCode() !== 0) {
			ArasModules.Dialog.alert('', {
				type: 'soap',
				data: soapResult
			});
			return null;
		}

		if (soapResult.isFault()) {
			return null;
		}
		return soapResult.getResult();
	}
	refreshUpdateDom(resultDom) {
		if (!resultDom) {
			return;
		}
		this.updateInfoDom = resultDom.selectSingleNode(
			'Item[@id="UpdateInfoMessage"]'
		);
	}
	fillStandardMessageCollection(resultDom, clearBefore) {
		if (clearBefore) {
			this.clearCollection();
		} else {
			this.makeAllMessagesOld();
		}

		if (!resultDom) {
			this.clearCollection();
			return;
		}

		const standardCollection = resultDom.selectNodes(
			'Item[type/text()="Standard"]'
		);

		for (let i = 0; i < standardCollection.length; i++) {
			const messageItem = standardCollection[i];
			const messageId = messageItem.getAttribute('id');
			const title = parent.aras.getItemProperty(messageItem, 'title');
			const imageUrl = parent.aras.getItemProperty(messageItem, 'icon');
			this.addMessage(messageId, title, imageUrl, messageItem);
		}

		this.clearOldMessages();
	}
	showPopupCollection(resultDom, showAsModeless) {
		if (!resultDom) {
			return;
		}

		const popupCollection = resultDom.selectNodes('Item[type/text()="Popup"]');

		const sortedList = this.sortMessagesByPriority(popupCollection);
		let i = 0;

		const nextSortedList = function() {
			if (i >= sortedList.length) {
				return;
			}
			const index = sortedList.length - i - 1;
			if (!sortedList[index]) {
				i++;
				return nextSortedList();
			}
			nextMsgIndex(index, 0);
		};

		const nextMsgIndex = (index, msgIndex) => {
			if (msgIndex >= sortedList[index].length) {
				i++;
				nextSortedList();
			} else {
				this.displayMessageDialog(
					sortedList[index][msgIndex],
					showAsModeless,
					function() {
						setTimeout(function() {
							msgIndex++;
							nextMsgIndex(index, msgIndex);
						}, 0);
					}
				);
			}
		};

		nextSortedList();
	}
	sortMessagesByPriority(messageCollection) {
		const sortedList = [];

		for (let i = 0; i < messageCollection.length; i++) {
			const priority = parent.aras.getItemProperty(
				messageCollection[i],
				'priority'
			);
			if (!sortedList[priority]) {
				sortedList[priority] = [];
			}

			sortedList[priority][sortedList[priority].length] = messageCollection[i];
		}

		return sortedList;
	}
	updateMessageCollection(doAsync, callback) {
		if (!doAsync) {
			doAsync = false;
		}

		if (doAsync) {
			const getCollectionCallback = function(container, messageCollection) {
				container.refreshUpdateDom(messageCollection);
				container.fillStandardMessageCollection(messageCollection);
				container.showPopupCollection(messageCollection, true);
				if (callback) {
					callback();
				}
			};
			this.asyncCheckNewMessages(getCollectionCallback);
		} else {
			const messageCollection = this.syncGetMessageCollection();
			this.refreshUpdateDom(messageCollection);
			this.fillStandardMessageCollection(messageCollection);
			this.showPopupCollection(messageCollection, false);
		}
	}
	startIterativeUpdateMessageCollection() {
		const callback = () => {
			this.checkTimeout = setTimeout(
				this.startIterativeUpdateMessageCollection.bind(this),
				this.checkInterval
			);
		};

		this.updateMessageCollection(true, callback);
	}
	displayMessageDialogById(messageId, showAsModeless) {
		if (messageId === 'UpdateInfoMessage') {
			this.displayMessageDialog(this.updateInfoDom, showAsModeless);
		} else {
			const messageItem = aras.getItemById('Message', messageId);
			if (messageItem) {
				this.displayMessageDialog(messageItem, showAsModeless);
			} else {
				ArasModules.Dialog.alert(
					aras.getResource('', 'user_notifications.message_no_more_available')
				);
				this.removeMessage(messageId);
			}
		}
	}
	displayMessageDialog(messageItem, showAsModeless, callback) {
		if (!messageItem) {
			return;
		}

		const templateUrl = parent.aras.getI18NXMLResource(
			'notification_popup_template.xml',
			parent.aras.getBaseURL()
		);
		const templateDom = parent.aras.createXMLDocument();
		templateDom.load(templateUrl);

		let parameters = this.popupParametersArray[
			parent.aras.getItemProperty(messageItem, 'id')
		];
		if (!parameters) {
			parameters = {};
		}

		const openedMessageWindow = parameters.window;

		parameters.id = parent.aras.getItemProperty(messageItem, 'id');
		parameters.default_template = templateDom.selectSingleNode(
			'template/html'
		).text;
		parameters.custom_html = parent.aras.getItemProperty(
			messageItem,
			'custom_html'
		);

		parameters.dialogWidth = parent.aras.getItemProperty(messageItem, 'width');
		parameters.dialogHeight = parent.aras.getItemProperty(
			messageItem,
			'height'
		);
		parameters.css = parent.aras.getItemProperty(messageItem, 'css');
		parameters.is_standard_template =
			parent.aras.getItemProperty(messageItem, 'is_standard_template') === '1';

		if (!parameters.dialogWidth || parameters.is_standard_template) {
			parameters.dialogWidth = templateDom.selectSingleNode(
				'template/dialog_width'
			).text;
		}

		if (!parameters.dialogHeight || parameters.is_standard_template) {
			parameters.dialogHeight = templateDom.selectSingleNode(
				'template/dialog_height'
			).text;
		}

		parameters.title = parent.aras.getItemProperty(messageItem, 'title');
		parameters.text = parent.aras.getItemProperty(messageItem, 'text');
		parameters.url = parent.aras.getItemProperty(messageItem, 'url');
		parameters.icon = parent.aras.getItemProperty(messageItem, 'icon');

		parameters.OK_IsVisible =
			parent.aras.getItemProperty(messageItem, 'show_ok_button') === '1';
		parameters.Exit_IsVisible =
			parent.aras.getItemProperty(messageItem, 'show_exit_button') === '1';

		parameters.OK_Label = parent.aras.getItemProperty(
			messageItem,
			'ok_button_label'
		);
		parameters.Exit_Label = parent.aras.getItemProperty(
			messageItem,
			'exit_button_label'
		);

		parameters.container = this;
		parameters.opener = window;
		parameters.writeContent = writeContent;
		parameters.aras = parent.aras;

		this.popupParametersArray[parameters.id] = parameters;

		if (!showAsModeless) {
			parameters.content = 'modalDialog.html';

			window.ArasModules.Dialog.show('iframe', parameters).promise.then(
				function(res) {
					if (res === this.acknowledge) {
						this.acknowledgeMessage(messageItem);
					}
					if (callback) {
						callback();
					}
				}.bind(this)
			);
		} else {
			const sFeatures = `height=${parameters.dialogHeight}, width=${
				parameters.dialogWidth
			}`;

			if (openedMessageWindow) {
				openedMessageWindow.close();
			}

			const OnBeforeUnloadHandler = function() {
				const container = parameters.window.parameters.container;
				if (parameters.window.returnValue === container.acknowledge) {
					container.acknowledgeMessage(messageItem);
				}
				if (callback) {
					callback();
				}
				parameters.window = null;
			};

			parameters.window = window.open(
				parent.aras.getScriptsURL() + 'blank.html',
				'',
				sFeatures
			);
			parameters.window.focus();
			writeContent(parameters.window);
			parameters.window.parameters = parameters;
			parameters.window.addEventListener('beforeunload', OnBeforeUnloadHandler);
		}

		function uiDrawInputButton(name, value, handlerCode, className) {
			return `<input name="${name}" type="button" value="${value}" onclick="${handlerCode}" class="btn ${className}"/>`;
		}

		function writeContent(w) {
			const doc = w.document;
			let template;
			const titleExpr = new RegExp('{TITLE}', 'g');
			const urlExpr = new RegExp('{URL}', 'g');
			const textExpr = new RegExp('{TEXT}', 'g');
			const iconExpr = new RegExp('{ICON}', 'g');

			if (parameters.icon) {
				parameters.icon = parent.aras.getScriptsURL() + parameters.icon;
			}
			if (!parameters.is_standard_template) {
				template = parameters.custom_html.replace(titleExpr, parameters.title);
				template = template.replace(urlExpr, parameters.url);
				template = template.replace(textExpr, parameters.text);
				template = template.replace(iconExpr, parameters.icon);

				doc.write(template);
			} else {
				template = parameters.default_template.replace(
					titleExpr,
					parameters.title
				);
				template = template.replace(urlExpr, parameters.url);
				template = template.replace(textExpr, parameters.text);
				template = template.replace(iconExpr, parameters.icon);
				doc.write(template);

				if (parameters.css) {
					const style = doc.createElement('style');
					style.innerHTML = parameters.css;
					doc.head.appendChild(style);
				}
				const notificationTitle = doc.getElementById('notification_title');
				const titleElement = notificationTitle.appendChild(
					doc.createElement('h2')
				);
				titleElement.setAttribute('class', 'title');
				if (parameters.url) {
					const urlElement = titleElement.appendChild(doc.createElement('a'));
					urlElement.setAttribute('href', parameters.url);
					urlElement.setAttribute('target', '_about');
					urlElement.setAttribute('class', 'sys_item_link');
					urlElement.textContent = parameters.title;
				} else {
					titleElement.textContent = parameters.title;
				}

				if (!parameters.icon) {
					doc.getElementById('message_icon').style.display = 'none';
				}
			}
			w.returnValue = parameters.container.сloseText;
			w.closeWindow = function(value) {
				if (w.dialogArguments && w.dialogArguments.dialog) {
					w.dialogArguments.dialog.close(value);
				} else {
					w.returnValue = value || w.returnValue;
					w.close();
				}
			};
			doc.close();

			let innerHTML = '';
			if (parameters.OK_IsVisible) {
				innerHTML += uiDrawInputButton(
					'OK_button',
					parameters.OK_Label,
					` window.closeWindow('${parameters.container.acknowledge}');`,
					'notification_ok_btn'
				);
			}
			if (parameters.Exit_IsVisible) {
				innerHTML += uiDrawInputButton(
					'Exit_button',
					parameters.Exit_Label,
					` window.closeWindow('${parameters.container.сloseText}');`,
					'cancel_button notification_exit_btn'
				);
			}
			if (innerHTML) {
				const el = doc.createElement('center');
				el.innerHTML = innerHTML;
				const source =
					doc.getElementById('btns') || doc.getElementsByTagName('body')[0];
				if (source) {
					source.appendChild(el);
				}
			}
		}
	}
	acknowledgeMessage(messageItem) {
		if (!messageItem) {
			return;
		}

		const acknowledgeDom = messageItem.selectSingleNode('acknowledge');

		if (acknowledgeDom.text === 'Once') {
			const messageId = messageItem.getAttribute('id');

			const relsh = `<Item type="Message Acknowledgement" action="add">
					<related_id>${parent.aras.getCurrentUserID()}</related_id>
					<source_id>${messageItem.getAttribute('id')}</source_id>
				</Item>`;

			const soapResult = parent.aras.soapSend('ApplyItem', relsh);

			if (soapResult.getFaultCode() !== 0) {
				ArasModules.Dialog.alert('', {
					type: 'soap',
					data: soapResult
				});
				return null;
			}

			if (soapResult.isFault()) {
				return null;
			}

			this.removeMessage(messageId);
		}
	}
	dispose() {
		if (this.checkTimeout) {
			clearTimeout(this.checkTimeout);
		}
	}
}
