﻿const cuiMethods = {
	collectCuiData: function(items, initialData) {
		const data = Object.assign({}, initialData, {
			dataMap: new Map(),
			events: new Set(['click']),
			images: [],
			roots: []
		});

		return items.reduce(function(data, item) {
			item = cuiMethods.initializeItem(item, data);
			if (item.data) {
				const itemsMap = new Map();
				item.data.forEach(function(childItem) {
					childItem = cuiMethods.initializeItem(childItem, data, item);
					cuiMethods.addItemPropertiesToCuiData(data, childItem);
					itemsMap.set(childItem.id, childItem);
				});
				item.data = itemsMap;
			}
			cuiMethods.addItemPropertiesToCuiData(data, item);
			data.dataMap.set(item.id, item);
			data.roots.push(item.name);
			return data;
		}, data);
	},
	addItemPropertiesToCuiData: function(cuiData, item) {
		if (item.clickEventType) {
			cuiData.events.add(item.clickEventType);
		}
		if (item.image) {
			cuiData.images.push(item.image);
		}
	},
	destroyMethodsCollection: new WeakMap(),
	destroyMethodFactory: function(control, fnList) {
		const eventListeners = function() {
			(fnList || []).forEach(function(fn) {
				fn();
			});
		};
		cuiMethods.destroyMethodsCollection.set(
			control,
			eventListeners
		);
		return {
			destroy: eventListeners
		};
	},
	defaultEventHandler: function(control, options, itemId, event) {
		let parentItem;
		let targetItem;
		const menuItemNode = event.target.closest('li[data-index]');
		if (menuItemNode) {
			parentItem = control.data.get(itemId);
			const targetId = menuItemNode.dataset.index;
			targetItem = parentItem.data.get(targetId);
		} else {
			targetItem = control.data.get(itemId);
		}

		const handler = targetItem && (targetItem.on_click_handler || targetItem.on_keydown_handler);
		if (!handler) {
			return;
		}

		const clickEventType = targetItem.clickEventType;
		if (clickEventType === event.type || !clickEventType && event.type === 'click') {
			const context = {
				currentTarget: parentItem || targetItem,
				target: targetItem,
				control: control
			};
			cuiMethods.executeClientMethod(handler, context, options);
		}
	},
	executeClientMethod: function(methodId, context, options) {
		if (!methodId) {
			return;
		}

		const methodNd = aras.MetadataCache.GetClientMethodNd(methodId, 'id');
		if (!methodNd) {
			const notify = aras.getNotifyByContext(window);
			notify(
				aras.getResource('', 'cui.failed_to_execute_method', methodId),
				{ type: 'error' }
			);
			return;
		}

		const method = new Function('control', 'target', 'options', methodNd.selectSingleNode('method_code').text); // jshint ignore:line
		Object.assign(context, options);

		try {
			return method(context.control, context.target, context);
		} catch (e) {
			aras.AlertError(
				aras.getResource('', 'aras_object.method_failed', aras.getItemProperty(methodNd, 'name')),
				aras.getResource('', 'aras_object.aras_object', e.number, e.description || e.message),
				aras.getResource('', 'common.client_side_err')
			);
		}
	},
	getConfigurableUi: function(location, options) {
		const requestParams = cuiMethods.getRequestParams(location, options);
		return aras.MetadataCache.GetConfigurableUiAsync(requestParams, true);
	},
	getRequestParams: (location, options) => {
		const {
			itemId = '',
			itemTypeName = '',
			item_classification = ''
		} = options;
		const itemTypeId = aras.getItemTypeId(itemTypeName) || '';

		return {
			'item_id': isPresentableItem(itemTypeId) ? itemId : '',
			'item_type_id': itemTypeId,
			'location_name': location, 'item_classification': item_classification
		};
	},
	handleControlEvents: function(control, events, options, handler) {
		if (!handler) {
			return;
		}
		if (cuiMethods.destroyMethodsCollection.has(control)) {
			throw new Error('The event handler for this control is already defined.');
		}

		handler = handler.bind(null, control, options);
		const eventListeners = [];
		events.forEach(function(eventName) {
			const removeListenerFn = control.on(eventName, handler);
			eventListeners.push(removeListenerFn);
		});

		return cuiMethods.destroyMethodFactory(control, eventListeners);
	},
	initializeCuiControl: function(control, location, cuiItemTypes, options) {
		const destroyMethodsCollection = cuiMethods.destroyMethodsCollection;
		if (destroyMethodsCollection.has(control)) {
			destroyMethodsCollection.get(control)();
			destroyMethodsCollection.delete(control);
		}

		return cuiMethods.getConfigurableUi(location, options)
			.then(function(items) {
				const cuiData = cuiMethods.collectCuiData(
					items,
					{
						control,
						cuiItemTypes,
						options
					}
				);
				ArasModules.SvgManager.load(cuiData.images);
				return cuiData;
			});
	},
	initializeItem: function(item, cuiData, rootItem) {
		const context = {
			control: cuiData.control,
			currentTarget: rootItem || item,
			target: item
		};
		const type = item['@type'];
		const itemTypeMetadata = cuiData.cuiItemTypes[type];
		const initHandlerResult = cuiMethods.executeClientMethod(item.on_init_handler, context, cuiData.options);
		Object.assign(item, itemTypeMetadata, item.additional_data, initHandlerResult);

		item.id = item.name;
		return item;
	},
	reinitControlItems: function(control, eventType, options) {
		const context = {
			control: control
		};
		const reinitAllItems = function(data, rootItem) {
			data.forEach(function(item) {
				const events = item.include_events;
				if (events && events.indexOf(eventType) !== -1) {
					initItem(data, item, rootItem);
				}
			});
		};
		const initItem = function(data, item, rootItem) {
			Object.assign(
				context,
				{
					currentTarget: rootItem || item,
					target: item
				}
			);
			const initHandlerResult = cuiMethods.executeClientMethod(item.on_init_handler, context, options);
			const newItem = Object.assign({}, item, item.additional_data, initHandlerResult);
			data.set(item.id, newItem);

			if (item.data) {
				reinitAllItems(item.data, item);
			}
		};

		reinitAllItems(control.data);
		control.render();
	},
	adaptDataForControl: function(cuiData) {
		const data = cuiData.dataMap;
		data.forEach(function(item) {
			if (item.image) {
				item.icon = item.image;
				delete item.image;
			}
			if (!item.data) {
				return;
			}
			item.data.forEach(function(child) {
				data.set(child.id, child);
			});
			item.children = item.roots;
			delete item.data;
			delete item.roots;
		});
		return cuiData;
	}
};

const isPresentableItem = itemTypeId => {
	if (!itemTypeId) {
		return false;
	}

	const item = aras.getItemTypeForClient('PresentableItems', 'name');
	const morphaeList = aras.getMorphaeList(item.node);
	return morphaeList.some(item => item.id === itemTypeId);
};

export default cuiMethods;
