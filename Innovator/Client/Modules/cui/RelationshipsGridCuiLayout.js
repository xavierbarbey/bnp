﻿import CuiLayout from './CuiLayout';

export default class RelationshipsGridCuiLayout extends CuiLayout {
	_getCuiControls() {
		return Promise.resolve([
			{
				control_type: 'ToolbarControl',
				id: 'relationshipsTitleBar',
				additional_data: {
					cssClass: 'aras-titlebar aras-titlebar_c'
				},
				name: 'ItemView.RelationshipsTitleBar',
				'location@keyed_name': 'ItemView.RelationshipsTitleBar'
			},
			{
				control_type: 'ToolbarControl',
				additional_data: {
					cssClass: 'aras-commandbar'
				},
				id: 'relationshipsCommandBar',
				name: 'ItemView.RelationshipsCommandBar',
				'location@keyed_name': 'ItemView.RelationshipsCommandBar'
			}
		]);
	}
}
