﻿import cuiMethods from './cuiMethods';

const cuiItemTypes = {};

function cuiToc(control, location, options = {}) {
	return cuiMethods.initializeCuiControl(control, location, cuiItemTypes, options)
		.then(cuiMethods.adaptDataForControl)
		.then(function(cuiData) {
			control.dataStore.items = cuiData.dataMap;
			control.dataStore.roots = new Set(cuiData.roots);
			const handleControlEvents = cuiMethods.handleControlEvents.bind(
				null,
				control,
				cuiData.events,
				options,
				eventHandler
			);
			return control
				.render()
				.then(handleControlEvents);
		});
}

function eventHandler(control, options, itemId, event) {
	const item = control.data.get(itemId);
	const tabsObj = aras.getMainWindow().arasTabs;
	const searchIconNode = event.target.closest('.aras-button.aras-nav-leaf-ico');

	if (item.formId) {
		tabsObj.openForm(item.formId, item.icon, item.label);
	} else if (item.startPage) {
		const url = item.startPage;
		let parameters = item.parameters || '';
		if (parameters) {
			if (parameters.startsWith('\'') && parameters.endsWith('\'')) {
				parameters = parameters.slice(1, -1);
			}
			parameters = '?' + parameters;
		}
		tabsObj.openPage(url + parameters, item.itemTypeId, item.icon, item.label);
	} else if (searchIconNode) {
		tabsObj.openSearch(item.itemTypeId);
	}
}

export default cuiToc;
