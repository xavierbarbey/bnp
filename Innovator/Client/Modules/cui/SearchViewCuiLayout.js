﻿import CuiLayout from './CuiLayout';

export default class SearchViewCuiLayout extends CuiLayout {
	_getCuiControls() {
		return Promise.resolve([
			{
				control_type: 'ToolbarControl',
				id: 'searchViewTitleBar',
				additional_data: {
					cssClass: 'aras-titlebar'
				},
				name: 'SearchView.TitleBar',
				'location@keyed_name': 'SearchView.TitleBar'
			},
			{
				control_type: 'ToolbarControl',
				id: 'searchViewCommandBar',
				additional_data: {
					cssClass: 'aras-commandbar aras-commandbar_bordered'
				},
				name: 'SearchView.CommandBar',
				'location@keyed_name': 'SearchView.CommandBar'
			}
		]);
	}
}