﻿import cuiMethods from './cuiMethods';
import cuiToolbar from './cuiToolbar';
import { tabContainerControl, tabElementControl } from './cuiTabsContainer';

const defaultControlType = 'default';

const defaultConstructor = (control, childNodes, metadata) => {
	const { additional_data = {}, control_type: type, name } = control;
	const { cssClass = '', cssStyle = '' } = additional_data;
	const { webComponentName } = metadata;

	return HyperHTMLElement.wire(null, `:${name}`)(
		[
			`<${webComponentName} class="`,
			`" style="`,
			`" >`,
			`</${webComponentName}>`
		],
		cssClass,
		cssStyle,
		childNodes
	);
};

const cuiControls = new Map();
cuiControls.set(
	defaultControlType,
	{
		constructor: defaultConstructor,
		webComponentName: 'div'
	}
);

cuiControls.set(
	'AccordionElementControl',
	{
		constructor: defaultConstructor,
		webComponentName: 'aras-accordion'
	}
);
cuiControls.set(
	'FormControl',
	{
		constructor: defaultConstructor,
		webComponentName: 'aras-form'
	}
);
cuiControls.set(
	'TabContainerControl',
	tabContainerControl
);
cuiControls.set(
	'TabElementControl',
	tabElementControl
);
cuiControls.set(
	'ToolbarControl',
	{
		constructor: defaultConstructor,
		eventHandler: cuiMethods.reinitControlItems,
		initControl: cuiToolbar,
		webComponentName: 'aras-toolbar'
	}
);

const getControlMetadata = type => cuiControls.has(type) ? cuiControls.get(type) : cuiControls.get(defaultControlType);

export default getControlMetadata;
