﻿import getControlMetadata from './cuiControls';
import cuiMethods from './cuiMethods';
import CuiObserver from './CuiObserver';

const collectControlsData = controls => {
	const topToolbarsNode = document.getElementById('top-toolbars');
	const firstNonToolbarControlIndex = topToolbarsNode ? controls.findIndex(
		({ control_type }) => {
			return control_type !== 'ToolbarControl';
		}
	) : 0;
	return controls.reduce(
		(controlsData, control, index) => {
			const { dataMap, firstNonToolbarControlIndex, otherControls, topToolbars } = controlsData;
			const { data, name } = control;
			if (index < firstNonToolbarControlIndex) {
				topToolbars.push(name);
			} else {
				otherControls.push(name);
			}

			dataMap.set(name, control);
			if (data) {
				data.forEach(child => {
					dataMap.set(child.name, child);
				});
			}

			return controlsData;
		},
		{ dataMap: new Map(), firstNonToolbarControlIndex, otherControls: [], topToolbars: [] }
	);
};

export default class CuiLayout {
	constructor(dom, location = '', options = {}) {
		this.dom = dom;
		this.location = location;
		this.observer = new CuiObserver();
		this.options = options;
	}

	init() {
		return this._getCuiControls().then(controls => {
			const { dataMap, otherControls, topToolbars } = collectControlsData(controls);
			const controlsMap = new Map();
			if (topToolbars.length) {
				HyperHTMLElement.bind(document.getElementById('top-toolbars'))`${
					topToolbars.map(controlName => this._initCuiControl(controlName, { dataMap, controlsMap }))
				}`;
			}
			HyperHTMLElement.bind(this.dom)`${
				otherControls.map(controlName => this._initCuiControl(controlName, { dataMap, controlsMap }))
			}`;

			const initializedControlsPromises = [];
			let defferedEvents = new Set();

			controlsMap.forEach(({control_type: type, 'location@keyed_name': location}, node) => {
				const { eventHandler, initControl } = getControlMetadata(type);

				if (eventHandler) {
					this.observer.subscribe((eventType) => {
						if (!defferedEvents) {
							eventHandler(node, eventType, this.options);
						} else {
							defferedEvents.add(eventType);
						}
					});
				}

				if (initControl) {
					const initPromise = initControl(node, location, this.options);
					initializedControlsPromises.push(initPromise);
				}
			});

			return Promise.all(initializedControlsPromises).then(() => {
				const setEvents = defferedEvents;
				defferedEvents = null;
				setEvents.forEach(eventType => this.observer.notify(eventType));
			});
		});
	}

	_getCuiControls() {
		const requestParams = cuiMethods.getRequestParams(
			this.location,
			this.options
		);
		return aras.MetadataCache.GetConfigurableUIControls(requestParams);
	}

	_initCuiControl(controlName, data) {
		const { dataMap, controlsMap } = data;
		const control = dataMap.get(controlName);
		const { children, control_type: type, roots } = control;
		const childNodes = (roots || children || []).map(
			childName => this._initCuiControl(childName, data)
		);
		const controlMetadata = getControlMetadata(type);
		const nodes = controlMetadata.constructor(control, childNodes, controlMetadata);
		const controlNode = Array.isArray(nodes) ? nodes[0] : nodes;
		controlsMap.set(controlNode, control);
		return nodes;
	}
}
