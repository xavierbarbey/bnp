﻿const accordionComponentName = 'aras-accordion';
const invisibleClass = 'aras-invisible';

const tabContainerControlConstructor = (control, childNodes, metadata) => {
	const { additional_data = {}, control_type: type, name } = control;
	const { cssClass = '', cssStyle = '', slot = '' } = additional_data;
	const { webComponentName } = metadata;

	const node = HyperHTMLElement.wire(null, `:${name}`)(
		[
			`<${webComponentName} class="`,
			`" style="`,
			`" slot="`,
			`" />`
		],
		cssClass,
		cssStyle,
		slot
	);

	return [ node, ...childNodes ];
};

const initTabContainerControl = control => {
	const accordion = control.closest(accordionComponentName);
	if (!accordion) {
		return;
	}

	const tabElementSelector = tabElementControl.webComponentName;
	const tabs = Array
		.from(accordion.children)
		.concat(accordion._contentNodes)
		.filter(child => child.matches(`${tabElementSelector}[data-tab-id]`));
	if (!tabs.length) {
		return;
	}

	control.on('click', tabId => {
		if (accordion.collapsed) {
			accordion.expand();
		}

		control.selectTab(tabId);
		tabs.forEach(tab =>
			tab.dataset.tabId === tabId ?
				tab.classList.remove(invisibleClass) :
				tab.classList.add(invisibleClass)
		);
	});

	const [ firstTab ] = tabs;
	firstTab.classList.remove(invisibleClass);
	tabs.forEach(tab => {
		control.addTab(
			tab.dataset.tabId,
			{
				label: tab.dataset.tabLabel
			}
		);
	});
	control.selectTab(firstTab.dataset.tabId);
};

const tabContainerControl = {
	constructor: tabContainerControlConstructor,
	initControl: initTabContainerControl,
	webComponentName: 'aras-tabs'
};

const tabElementControlConstructor = (control, childNodes, metadata) => {
	const { additional_data = {}, control_type: type, label, name } = control;
	const { cssClass = '', cssStyle = '' } = additional_data;
	const { webComponentName } = metadata;

	return HyperHTMLElement.wire(null, `:${name}`)(
		[
			`<${webComponentName} class="`,
			`" style="`,
			`" data-tab-id="`,
			`" data-tab-label="`,
			`" >`,
			`</${webComponentName}>`
		],
		cssClass ? `${cssClass} ${invisibleClass}` : invisibleClass,
		cssStyle,
		name,
		label || name,
		childNodes
	);
};

const tabElementControl = {
	constructor: tabElementControlConstructor,
	webComponentName: 'div'
};

export { tabContainerControl, tabElementControl };