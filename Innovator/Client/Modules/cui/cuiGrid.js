﻿const cachedRowStyles = new Map();
const cachedRowStringStyles = new Map();

function cuiGrid(control, options = {}) {
	initGridHeader(control, options.gridHeaderInfo);
	return Promise.resolve();
}

function initGridHeader(grid, headerInfo = {headMap: new Map(), indexHead: []}) {
	grid.head = headerInfo.headMap;
	grid.settings.indexHead = headerInfo.indexHead;
	grid.rows = new Map();
	grid.settings.indexRows = [];
	grid.settings.selectedRows = [];
	grid.getCellStyles = getCellStyles.bind(grid);
}

const getCellStyles = (headId, rowId) => {
	const resultDom = currQryItem.getResultDOM();
	const styleString = cachedRowStringStyles.get(rowId) || aras.getValueByXPath(`//Result/Item[id="${rowId}"]/css`, resultDom);
	cachedRowStringStyles.set(rowId, styleString);
	const rowStyles = cachedRowStyles.get(styleString) || parseCss(styleString);
	cachedRowStyles.set(styleString, rowStyles);

	return rowStyles.get(headId.slice(0, -2)) || {};
};

const normalizeCSS = (css = '') => {
	css = css.trim();
	if (!css) {
		return '';
	}

	const trimKeys = '{}:;';
	css = trimKeys.split('').reduce((css, key) => {
		return css.replace(new RegExp('[\\s]*\\' + key +  '[\\s]*', 'g'), key);
	}, css);
	return css;
};

const parseCss = cssString => {
	cssString = normalizeCSS(cssString);

	const cssBlocks = cssString.match(/\..*?\}/g) || [];
	const stylesMap = cssBlocks.reduce((acc, cssBlock) => {
		const propertyName = cssBlock.match(/\..*(?=\{)/)[0].slice(1);
		const rules = cssBlock.match(/\{.*(?=\})/)[0].slice(1);
		const resultObject = {};
		rules.split(';').forEach(rule => {
			if (!rule) {
				return;
			}

			rule = rule.split(':');
			resultObject[rule[0]] = rule[1];
		});

		acc.set(propertyName, resultObject);
		return acc;
	}, new Map());

	return stylesMap;
};

export default cuiGrid;
