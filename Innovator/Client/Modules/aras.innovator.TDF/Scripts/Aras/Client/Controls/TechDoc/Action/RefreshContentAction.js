define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase'
],
function(declare, ActionBase) {
	return declare('Aras.Client.Controls.TechDoc.Action.RefreshContentAction', ActionBase, {
		constructor: function(args) {
		},

		Execute: function(/*Object*/context) {
			var contentGenerationHelper = this._viewmodel.ContentGeneration();
			var targetElement = context.selectedItem;

			contentGenerationHelper.refreshStaticContent(targetElement);
		}
	});
});
