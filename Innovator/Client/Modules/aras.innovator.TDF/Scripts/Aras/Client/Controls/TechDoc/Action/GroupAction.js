﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase'
],
function(declare, ActionBase) {
	return declare('Aras.Client.Controls.TechDoc.Action.GroupAction', ActionBase, {
		constructor: function(args) {
		},

		Execute: function(/*Object*/context) {
			var selectedItems = context.selectedItems;
			var parentItem = selectedItems[0].Parent;
			var parentChilds = parentItem.ChildItems();
			var itemIndexes = [];
			var sortedSelectedItems = [];
			var newBlockElement = this._viewmodel.CreateElement('element', {type: 'aras:block'});
			var itemsHash = {};
			var selectedItem;
			var removedItems;
			var itemIndex;
			var i;

			for (i = 0; i < selectedItems.length; i++) {
				selectedItem = selectedItems[i];
				itemIndex = parentChilds.index(selectedItem);

				itemsHash[itemIndex] = selectedItem;
				itemIndexes.push(itemIndex);
			}

			itemIndexes = itemIndexes.sort();

			for (i = 0; i < itemIndexes.length; i++) {
				itemIndex = itemIndexes[i];
				sortedSelectedItems.push(itemsHash[itemIndex]);
			}

			this._viewmodel.SuspendInvalidation();

			itemIndex = parentChilds.index(sortedSelectedItems[0]);
			removedItems = parentChilds.splice(itemIndex, sortedSelectedItems.length);
			newBlockElement.ChildItems().addRange(removedItems);
			parentChilds.insertAt(itemIndex, newBlockElement);

			this._viewmodel.SetSelectedItems(newBlockElement);
			this._viewmodel.ResumeInvalidation();

			return newBlockElement;
		}
	});
});
