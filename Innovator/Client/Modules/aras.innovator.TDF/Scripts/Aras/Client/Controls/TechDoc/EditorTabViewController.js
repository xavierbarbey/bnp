﻿var aras = parent.aras;
var viewsController = parent.tabViewsController;
var isUIControlsCreated = false;
var viewContext = {
	data: {
		documentItem: null,
		documentViewSettings: null
	},
	controls: {
		sbsViewController: null,
		primaryViewPanel: null,
		secondaryViewPanel: null
	},
	topWindow: aras.getMostTopWindowWithAras(window),
	editState: null
};

function loadView(newDocumentItem) {
	var viewData = viewContext.data;

	viewData.documentItem = newDocumentItem;

	if (isUIControlsCreated) {
		reloadView(newDocumentItem);
	} else {
		createUIControls();
	}
}

function reloadView(newDocumentItem) {
	updateOriginDocumentPanels(newDocumentItem);
}

function updateOriginDocumentPanels(newDocumentItem)  {
	var viewController = viewContext.controls.sbsViewController;
	var documentViewSettings = viewContext.data.documentViewSettings;
	var documentItemId = newDocumentItem.getAttribute('id');
	var isDirty = newDocumentItem.getAttribute('isDirty') == '1';
	var currentPanel;
	var panelSettings;
	var i;

	for (i = 0; i < viewController.panelsCount; i++) {
		currentPanel = viewController.getPanelByIndex(i);
		panelSettings = documentViewSettings[currentPanel.id];

		if (panelSettings.documentItemId == documentItemId) {
			panelSettings.documentItem = newDocumentItem;

			if (currentPanel.isVisible) {
				loadPanelViewData(currentPanel, !isDirty);
			}
		}
	}
}

function loadPanelViewData(targetPanel, forceReload) {
	if (isUIControlsCreated && targetPanel) {
		if (targetPanel.isFrameLoaded) {
			onPanelLoaded(targetPanel, forceReload);
		} else {
			var eventListener = targetPanel.addEventListener(this, this, 'onFrameContentLoaded', function() {
				eventListener.remove();
				onPanelLoaded(targetPanel, forceReload);
			});
		}
	}
}

function onPanelLoaded(targetPanel, forceReload) {
	var viewData = viewContext.data;
	var panelFrameWindow = targetPanel.getFrameWindow();
	var panelViewSettings = viewData.documentViewSettings[targetPanel.id];

	if (!targetPanel.isInitialized) {
		panelFrameWindow.initializeView({
			aras: aras
		});
		targetPanel.isInitialized = true;
	}

	panelFrameWindow.setupView(panelViewSettings, {
		forceReload: forceReload
	});
}

function getDefaultLaguageCode() {
	var languageCode = aras.getVariable('tp_DefaultLanguageCode');

	if (!languageCode) {
		var methodItem = aras.newIOMItem('Method', 'tp_GetDefaultLanguageCode');

		methodItem = methodItem.apply();
		languageCode = methodItem.getResult();
		aras.setVariable('tp_DefaultLanguageCode', languageCode);
	}

	return languageCode;
}

function getLanguageList(languageCode) {
	var languageItemNodes = aras.getLanguagesResultNd().selectNodes('.//Item[@type="Language"]');
	var resultList = {};
	var currentNode;
	var propertyValue;
	var i;

	for (i = 0; i < languageItemNodes.length; i++) {
		currentNode = languageItemNodes[i];
		propertyValue = aras.getItemProperty(currentNode, 'code');

		resultList[propertyValue] = aras.getItemProperty(currentNode, 'name');
	}

	return resultList;
}

function getConfigurableUIData() {
	var viewData = viewContext.data;
	var responceData = aras.MetadataCache.GetConfigurableUi({
		item_type_id: aras.getItemTypeId(viewData.documentItem.getAttribute('type')),
		location_name: 'HtmlRichTextEditor'
	});

	return responceData && responceData.results;
}

function createViewSettings() {
	var viewData = viewContext.data;
	var languageCode = getDefaultLaguageCode();
	var documentViewSettings = {
		viewType: 'single', // [single|sidebyside],
		defaultLanguageCode: languageCode,
		primaryView: {
			isPrimaryView: true,
			displayType: 'html',
			documentItem: viewData.documentItem,
			documentItemId: viewData.documentItem.getAttribute('id'),
			languageCode: languageCode,
			filteredContentView: 'inactive',
			filterFamilies: {}
		},
		secondaryView: {
			displayType: 'html',
			documentItem: null,
			documentItemId: '',
			languageCode: languageCode,
			filteredContentView: 'inactive',
			filterFamilies: {}
		}
	};

	return documentViewSettings;
}

function onViewSettingsChanged(documentViewSettings) {
	var primaryViewPanel = viewContext.controls.primaryViewPanel;
	var secondaryViewPanel = viewContext.controls.secondaryViewPanel;
	var primaryViewSettings = documentViewSettings.primaryView;

	loadPanelViewData(primaryViewPanel);

	if (documentViewSettings.viewType == 'single') {
		secondaryViewPanel.hide();
	} else {
		var secondaryViewSettings = documentViewSettings.secondaryView;

		secondaryViewSettings.documentItem = aras.getItemById(primaryViewSettings.documentItem.getAttribute('type'), secondaryViewSettings.documentItemId);

		secondaryViewPanel.show();
		loadPanelViewData(secondaryViewPanel);
	}
}

function onPanelActivatedHandler(targetPanel, panelSettings) {
	var panelDocumentItem = panelSettings.documentItem;
	var topWindow = viewContext.topWindow;
	var topWindowItem = viewContext.topWindow.item;
	var viewControls = viewContext.controls;

	if (panelDocumentItem !== topWindow.item) {
		topWindow.item = panelDocumentItem;
		topWindow.updateMenuState();
	}

	viewControls.sbsViewController.raiseEvent('onViewPanelActivated', targetPanel);
}

function createUIControls() {
	require([
		'dojo/ready',
		'dojo/_base/declare',
		'TDF/Scripts/Aras/Client/Controls/TechDoc/UI/SideBySideView/PanelsViewController',
		'Modules/aras.innovator.core.Core/Scripts/Classes/Eventable',
		'dojo/domReady!'
	],
	function(ready, declare, PanelsViewController, Eventable) {
		ready(function() {
			var viewData = viewContext.data;

			// if current document item is not temporary
			if (!aras.isTempEx(viewData.documentItem)) {
				var viewControls = viewContext.controls;
				var documentViewSettings = createViewSettings();
				var sbsViewController;

				viewData.documentViewSettings = declare([Eventable], documentViewSettings)();
				viewData.documentViewSettings.addEventListener(window, window, 'onSettingsChanged', onViewSettingsChanged);

				sbsViewController = new PanelsViewController({id: 'sbsViewContainer', connectId: 'sbsViewContainer'});
				sbsViewController.configureView({
					panels: [{
						name: 'primaryView',
						type: 'url',
						URL: '../../Modules/aras.innovator.TDF/EditorViewPanel',
						eventListeners: {
							onPanelActivated: onPanelActivatedHandler
						}
					}, {
						name: 'secondaryView',
						type: 'url',
						URL: '../../Modules/aras.innovator.TDF/EditorViewPanel',
						visible: false,
						eventListeners: {
							onPanelActivated: onPanelActivatedHandler
						}
					}]
				});

				viewControls.sbsViewController = sbsViewController;
				viewControls.primaryViewPanel = sbsViewController.getPanelById('primaryView');
				viewControls.secondaryViewPanel = sbsViewController.getPanelById('secondaryView');

				sbsViewController.setSharedData('documentViewSettings', viewData.documentViewSettings);
				sbsViewController.setSharedData('configurableUIData', getConfigurableUIData());
				sbsViewController.setSharedData('systemLanguages', getLanguageList());
				sbsViewController.setSharedData('tdfSettings', viewsController.shareData.tdfSettings);

				isUIControlsCreated = true;
				loadPanelViewData(viewControls.primaryViewPanel);
			}
		});
	});
}
