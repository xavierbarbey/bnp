﻿define([
	'dojo/_base/declare',
	'TechDoc/Aras/Client/Controls/TechDoc/Action/ActionBase'
],
function(declare, ActionBase) {
	return declare('Aras.Client.Controls.TechDoc.Action.ViewExternalItem', ActionBase, {
		constructor: function(args) {
		},

		Execute: function(/*Object*/context) {
			var targetElement = context.selectedItem;
			var typeName;
			var itemId;

			if (targetElement.is('ArasBlockXmlSchemaElement')) {
				typeName = this._viewmodel.getDocumentItem().getAttribute('type');
				itemId = targetElement.BlockId();
			} else if (targetElement.is('ArasItemXmlSchemaElement')) {
				typeName = targetElement.ItemType();
				itemId = targetElement.ItemId();
			}

			if (typeName && itemId) {
				this.aras.uiShowItem(typeName, itemId);
			}
		}
	});
});
