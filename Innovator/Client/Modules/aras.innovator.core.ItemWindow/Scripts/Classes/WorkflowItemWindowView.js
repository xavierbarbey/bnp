﻿ModulesManager.define(
	['aras.innovator.core.ItemWindow/DefaultItemWindowView'],
	'aras.innovator.core.ItemWindow/WorkflowItemWindowView',
	function(DefaultItemWindowView) {
		function WorkflowItemWindowView(inDom, inArgs) {
			this.inDom = inDom;
			this.inArgs = inArgs;
		}

		WorkflowItemWindowView.prototype = new DefaultItemWindowView();

		WorkflowItemWindowView.prototype.getWindowProperties = function() {
			var result = null;
			var topWindow = window;

			var screenHeight = topWindow.screen.availHeight;
			var screenWidth = topWindow.screen.availWidth;
			var mainWindowHeight = topWindow.outerHeight;
			var mainWindowWidth = topWindow.outerWidth;
			var tempHeight;
			var tempWidth;
			var percentOfScreen = (mainWindowHeight * mainWindowWidth) / (screenWidth * screenHeight);

			//until the main window is more than 80% of the screen square, we calculate the size of the LifeCycle map as 80 percent of the screen size;
			tempHeight = percentOfScreen > 0.8 ? screenHeight : mainWindowHeight;
			tempWidth = percentOfScreen > 0.8 ? screenWidth : mainWindowWidth;

			sizeTrue = tempHeight > 800 && tempWidth > 1200;
			tempHeight = sizeTrue ? tempHeight : 800;	// 800*0.8= 640px
			tempWidth = sizeTrue ? tempWidth : 1200;	// 1200*0.8= 960px	960*640 is default size if the main window smaller than 1200*800

			var WFMapWindowHeight = tempHeight * 0.8;
			var WFMapWindowWidth = tempWidth * 0.8;

			// workflowMap window will be center-aligned
			var WFMapWindowTop = (screenHeight - WFMapWindowHeight) / 2;
			var WFMapWindowLeft = (screenWidth - WFMapWindowWidth) / 2;

			result = {height: WFMapWindowHeight, width: WFMapWindowWidth, x: WFMapWindowLeft, y: WFMapWindowTop};
			return result;
		};

		WorkflowItemWindowView.prototype.getWindowArguments = function() {
			var result = {};

			var wfNd = this.inDom;
			var arasObj = aras;

			var wfID = wfNd.getAttribute('id');
			var url = 'Workflow/workflowtool.html?id=' + wfID;

			var isEditMode = (arasObj.isTempEx(wfNd) || arasObj.isLockedByUser(wfNd)) ? true : false;
			var typeID = wfNd.getAttribute('typeId');
			var itemTypeNd = arasObj.getItemTypeDictionary(typeID ? arasObj.getItemTypeName(typeID) : 'Workflow Map').node;
			var tmpKey = (isEditMode ? 'ui_methods_ex.itemtype_label_item_keyed_name' : 'ui_methods_ex.itemtype_label_item_keyed_name_readonly');
			var wfmLabel = arasObj.getItemProperty(itemTypeNd, 'label');
			if (!wfmLabel) {
				wfmLabel = arasObj.getItemProperty(itemTypeNd, 'name');
			}
			var keyedName = arasObj.getKeyedNameEx(wfNd);

			var title = arasObj.getResource('', tmpKey, wfmLabel, keyedName);
			keyedName = arasObj.getKeyedNameEx(wfNd);

			result.url = url;
			result.keyedName = keyedName;
			result.wfmLabel = wfmLabel;
			result.title = title;
			result.item = wfNd;
			result.currWFNode = wfNd;
			result.itemID = wfID;
			result.itemTypeName = 'Workflow Map';
			result.itemType = itemTypeNd;
			result.viewMode = '';
			result.isEditMode = isEditMode;
			result.isTearOff = true;
			result.aras = arasObj;

			return result;
		};

		WorkflowItemWindowView.prototype.getWindowUrl = function() {
			var arasObj = aras;
			return arasObj.getBaseURL() + '/Modules/aras.innovator.core.ItemWindow/workflowView';
		};

		return WorkflowItemWindowView;
	});
