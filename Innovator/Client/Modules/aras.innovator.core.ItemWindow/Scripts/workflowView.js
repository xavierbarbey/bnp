﻿var item = window.opener[paramObjectName].item;
var currWFNode = window.opener[paramObjectName].item;
var itemID = window.opener[paramObjectName].itemID;
var itemTypeName = window.opener[paramObjectName].itemTypeName;
var itemType = window.opener[paramObjectName].itemType;
var viewMode = window.opener[paramObjectName].viewMode;
var isEditMode = window.opener[paramObjectName].isEditMode;
var isTearOff = window.opener[paramObjectName].isTearOff;
var wfmLabel = window.opener[paramObjectName].wfmLabel;
var keyedName = window.opener[paramObjectName].keyedName;
viewType = 'wokflowtool';
window.opener[paramObjectName] = undefined;

function setTitle(isEditMode) {
	var tmpKey = (isEditMode ? 'ui_methods_ex.itemtype_label_item_keyed_name' : 'ui_methods_ex.itemtype_label_item_keyed_name_readonly');
	document.title = aras.getResource('', tmpKey, wfmLabel, keyedName);
}

function getItem() {
	return item;
}
