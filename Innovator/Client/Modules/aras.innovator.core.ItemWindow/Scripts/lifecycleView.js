﻿var item = window.opener[paramObjectName].item;
var currLCNode = window.opener[paramObjectName].item;
var itemID = window.opener[paramObjectName].itemID;
var itemTypeName = window.opener[paramObjectName].itemTypeName;
var itemType = window.opener[paramObjectName].itemType;
var viewMode = window.opener[paramObjectName].viewMode;
var isEditMode = window.opener[paramObjectName].isEditMode;
var isTearOff = window.opener[paramObjectName].isTearOff;
var lcmLabel = window.opener[paramObjectName].lcmLabel;
var keyedName = window.opener[paramObjectName].keyedName;
var url2tool = window.opener[paramObjectName].url2tool;
viewType = 'formtool';
window.opener[paramObjectName] = undefined;

function setTitle(isEditMode) {
	var tmpKey = (isEditMode ? 'ui_methods_ex.itemtype_label_item_keyed_name' : 'ui_methods_ex.itemtype_label_item_keyed_name_readonly');
	document.title = aras.getResource('', tmpKey, lcmLabel, keyedName);
}

function getItem() {
	return item;
}

onload = function onloadHandler() {
	window.aras = new Aras(window.aras);

	var frm = document.getElementById('editor');

	frm.addEventListener('load', function() {
		aras.browserHelper.toggleSpinner(document, false);
	});

	frm.src = url2tool;
};
