﻿define([
	'dojo/dom',
	'dijit/registry',
	'TreeGridView/Scripts/Configurator/FormView'
],
function(dom, registry, FormView) {
	'use strict';
	var TreeGridViewDefinitionView = (function() {
		function TreeGridViewDefinitionView(contextDocument) {
			this.configuratorFormVisible = false;
			this.contextDocument = contextDocument;
		}
		TreeGridViewDefinitionView.prototype.initialize = function(viewDictionary) {
			this.initializeSidebar();
			this.initializeChildViews(viewDictionary);
		};
		TreeGridViewDefinitionView.prototype.showForm = function() {
			this.setVisibleFormContainer(true);
		};
		TreeGridViewDefinitionView.prototype.showConfigurator = function() {
			this.setVisibleFormContainer(false);
			this.configuratorView.initialize();
		};
		TreeGridViewDefinitionView.prototype.initializeSidebar = function() {
			this.contextDocument.addEventListener('loadSideBar', function() {
				var showFormButtonName = 'rb_TreeGridConfiguratorShowForm';
				var showFormActiveIconUrl = '../Images/ShowFormOn.svg';
				var sidebar = getSidebar();
				sidebar.switchSidebarButton(showFormButtonName, showFormActiveIconUrl, true);
			});
		};
		TreeGridViewDefinitionView.prototype.setVisibleFormContainer = function(showForm) {
			var formContainer = this.contextDocument.getElementById('CenterBorderContainer');
			var relationshipsContainer = this.contextDocument.getElementById('relationshipContentPane');
			var queryBuilderEditorContainer = this.contextDocument.getElementById('TreeGridViewDefinitionConfigurator');
			if (showForm) {
				formContainer.style.display = '';
				if (relationshipsContainer) {
					relationshipsContainer.style.display = '';
				}
				queryBuilderEditorContainer.style.display = 'none';
			} else {
				formContainer.style.display = 'none';
				if (relationshipsContainer) {
					relationshipsContainer.style.display = 'none';
				}
				queryBuilderEditorContainer.style.display = '';
				var containerWidget = registry.byId(queryBuilderEditorContainer.id);
				containerWidget.resize();
			}
			this.configuratorFormVisible = !showForm;
		};
		TreeGridViewDefinitionView.prototype.initializeChildViews = function(viewDictionary) {
			if (viewDictionary.configurator) {
				this.configuratorView = viewDictionary.configurator;
				this.configuratorView.setNode(dom.byId('configuratorTree'));
			}
			if (viewDictionary.form) {
				var formNode = dom.byId('CenterBorderContainer');
				this.form = new FormView(formNode);
			}
		};
		return TreeGridViewDefinitionView;
	}());
	return TreeGridViewDefinitionView;
});
