define([
	'dojo',
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dojo/parser',
	'dojo/io-query',
	'dijit/popup',
	'ES/Scripts/Classes/Utils',
	'ES/Scripts/Classes/Request/SelectQueryServiceRequest',
	'ES/Scripts/Controls/SearchPanel',
	'ES/Scripts/Controls/FiltersPanel',
	'ES/Scripts/Controls/ResultPanel',
	'ES/Scripts/Controls/Pagination'
], function(dojo, declare, lang, parser, ioQuery, popup, Utils, SelectQueryServiceRequest, SearchPanel, FiltersPanel, ResultPanel, Pagination) {
	return declare(null, {
		_arasObj: null,
		_utils: null,

		_isFirstExecution: true,

		_resetPagination: false,

		_topFacets: new Map(),

		//Request
		_selectQueryServiceRequest: null,

		//Widgets
		_searchPanelWidget: null,
		_filtersPanelWidget: null,
		_resultsPanelWidget: null,
		_paginationPanelWidget: null,

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});

			//Show container
			var layoutContainer = document.getElementById('layoutContainer');
			this._utils.setNodeVisibility(layoutContainer, true);

			//Parse layout
			parser.parse();

			/*---------------------------------------------------------------------------------------------*/
			//Init configurations for search

			var resItm = this._arasObj.newIOMItem('Method', 'ES_InitSearchConfigurations');
			resItm.apply();

			this._topFacets = this._utils.getTopFacets();

			/*---------------------------------------------------------------------------------------------*/
			//Create new request object

			this._selectQueryServiceRequest = new SelectQueryServiceRequest({
				arasObj: this._arasObj,
				topFacets: this._topFacets
			});

			/*---------------------------------------------------------------------------------------------*/
			//Show search toolbar

			this._searchPanelWidget = new SearchPanel({
				arasObj: this._arasObj,
				onSearch: this._onSearch.bind(this),
				onChipsChange: this._onChipsChange.bind(this)
			});

			var mainPageSearchContainer = dojo.byId('searchContainer');
			if (!this._utils.isNullOrUndefined(mainPageSearchContainer)) {
				this._searchPanelWidget.placeAt(mainPageSearchContainer);
			}
			this._searchPanelWidget.startup();

			/*---------------------------------------------------------------------------------------------*/
			//Show filters

			this._filtersPanelWidget = new FiltersPanel({
				arasObj: this._arasObj,
				onFiltersApply: this._onFiltersApply.bind(this),
				onFiltersReset: this._onFiltersReset.bind(this),
				onFilterReset: this._onFilterReset.bind(this),
				onFiltersChange: this._onFiltersChange.bind(this),
				onFilterLoadOptions: this._onFilterLoadOptions.bind(this)
			});

			var mainPageFiltersContainer = dojo.byId('filtersContainer');
			if (!this._utils.isNullOrUndefined(mainPageFiltersContainer)) {
				this._filtersPanelWidget.placeAt(mainPageFiltersContainer);
			}
			this._filtersPanelWidget.startup();

			/*---------------------------------------------------------------------------------------------*/
			//Show results

			this._resultsPanelWidget = new ResultPanel({
				arasObj: this._arasObj
			});

			var mainPageResultsContainer = dojo.byId('resultsContainer');
			if (!this._utils.isNullOrUndefined(mainPageResultsContainer)) {
				this._resultsPanelWidget.placeAt(mainPageResultsContainer);
			}
			this._resultsPanelWidget.startup();

			/*---------------------------------------------------------------------------------------------*/
			//Show pagination panel
			this._paginationPanelWidget = new Pagination({
				onPageChange: this._onPageChange.bind(this)
			});
			this._paginationPanelWidget.attach(dojo.byId('paginatorContainer'));

			/*---------------------------------------------------------------------------------------------*/
			//Perform search if have parameter

			var queryObject = ioQuery.queryToObject(window.location.search.slice(1));

			if (!this._utils.isNullOrUndefined(queryObject.q)) {
				this._searchPanelWidget.setQueryText(queryObject.q);

				this._onSearch();
			}

			var tabTitle = this._getTitle();
			this._utils.updateTabTitle(tabTitle);

			/*---------------------------------------------------------------------------------------------*/
			//Event listeners

			document.addEventListener('keyup', this._onDocumentKeyUp.bind(this));
			document.body.addEventListener('click', this._closePopup.bind(this));
		},

		/**
		 * Perform search
		 *
		 * @private
		 */
		_search: function() {
			this._utils.toggleSpinner(true, function() {
				try {
					this._sendRequest();
					this._update();
				} catch (ex) {
					this._arasObj.AlertError(ex.message);
				} finally {
					this._resetPagination = false;
					this._utils.toggleSpinner(false, null);
				}
			}.bind(this));
		},

		/**
		 * Update UI
		 *
		 * @private
		 */
		_update: function() {
			var totalRows = this._selectQueryServiceRequest.getTotalRows();
			var requestFacets = this._selectQueryServiceRequest.getQueryFacets();
			var requestFacetsClone = requestFacets.map(function(facet) {
				return dojo.clone(facet);
			});
			var resultFacets = [];
			var resultItems = [];
			if (totalRows > 0) {
				resultFacets = this._selectQueryServiceRequest.getResultFacets();
				resultItems = this._selectQueryServiceRequest.getResultItems();
			}

			//Update search panel
			this._searchPanelWidget.facets = requestFacets;
			this._searchPanelWidget.update();

			//Update filters panel
			this._filtersPanelWidget.items = this._composeFilterFacets(requestFacets, resultFacets);
			this._filtersPanelWidget.initialFacets = requestFacetsClone;
			this._filtersPanelWidget.update();

			//Update results panel
			this._resultsPanelWidget.items = resultItems;
			this._resultsPanelWidget.update();

			// Update pagination panel
			this._paginationPanelWidget.update(totalRows, this._resetPagination ? 1 : undefined);
		},

		/**
		 * Send request to server
		 *
		 * @private
		 */
		_sendRequest: function() {
			this._selectQueryServiceRequest.queryText = this._searchPanelWidget.getQueryText();
			this._selectQueryServiceRequest.start = this._resetPagination ? 0 : this._paginationPanelWidget.getOffset();
			this._selectQueryServiceRequest.rows = this._paginationPanelWidget.getPageSize();
			this._selectQueryServiceRequest.run();
		},

		/**
		 * Get tab title base on search query text
		 *
		 * @private
		 */
		_getTitle: function() {
			return this._utils.getResourceValueByKey('tab.title');
		},

		/**
		 * Composes list of facets for filters panel
		 *
		 * @private
		 * @param {object[]} requestFacets List of request facets
		 * @param {object[]} resultFacets List of result facets
		 * @returns {object[]} List of filter facets
		 */
		_composeFilterFacets: function(requestFacets, resultFacets) {
			var filterFacets = lang.clone(resultFacets);

			requestFacets.forEach(function(requestFacet) {
				var filterFacet = null;

				filterFacets.some(function(facet) {
					if (facet.name === requestFacet.name) {
						filterFacet = facet;
						return true;
					}
				});
				if (!filterFacet) {
					var facet = lang.clone(requestFacet);
					facet.clearOptions();
					requestFacet.getOptions().forEach(function(opt) {
						if (opt.isSelected) {
							facet.addOption(opt.name, opt.label, 0, true);
						}
					});
					filterFacets.push(facet);
				} else {
					var filterFacetOptionsMap = filterFacet.getOptions().reduce(function(map, opt) {
						map[opt.name] = opt;
						return map;
					}, {});
					requestFacet.getOptions().forEach(function(opt) {
						if (opt.isSelected && !filterFacetOptionsMap.hasOwnProperty(opt.name)) {
							filterFacet.addOption(opt.name, opt.label, 0, true);
						}
					});
				}
			});

			return filterFacets;
		},

		/**
		 * Closes any opened popup
		 *
		 * @param {object} ev Event object
		 * @param {boolean} forceClose Should popup be closed despite event target
		 */
		_closePopup: function(ev, forceClose) {
			var isForceClose = this._utils.isNullOrUndefined(forceClose) ? false : forceClose;
			var topPopup = popup.getTopPopup();

			if (!this._utils.isNullOrUndefined(topPopup) && !topPopup.widget._destroyed) {
				if (isForceClose) {
					topPopup.onCancel();
				} else {
					var isEventInPopup = topPopup.widget.domNode.contains(ev.target);

					if (!isEventInPopup) {
						// If click was outside of popup - close it
						topPopup.onCancel();
					}
				}
			}
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onSearch: function() {
			this._resetPagination = true;
			if (this._isFirstExecution) {
				this._selectQueryServiceRequest.restoreDefaultQueryFacets();
				this._isFirstExecution = false;
			}

			this._search();
		},

		_onDocumentKeyUp: function(ev) {
			switch (ev.key) {
				case 'Esc': // fix for IE
				case 'Escape':
					this._closePopup(ev, true);
					break;
				case 'Enter':
					var isActionable = ev.target.getAttribute('data-action') !== 'ignored';

					if (isActionable) {
						this._closePopup(ev, true);
						this._onSearch();
					}
			}
		},

		_onFiltersApply: function() {
			this._resetPagination = true;

			this._search();
		},

		_onPageChange: function() {
			var pageSize = this._paginationPanelWidget.getPageSize();
			var offset = this._paginationPanelWidget.getOffset();

			if (this._selectQueryServiceRequest.rows !== pageSize || this._selectQueryServiceRequest.start !== offset) {
				this._resetPagination = this._selectQueryServiceRequest.rows !== pageSize;
				this._search();
			}
		},

		_onFiltersReset: function() {
			this._resetPagination = true;
			this._selectQueryServiceRequest.resetQueryFacets();

			this._search();
		},

		_onFilterReset: function(facet) {
			this._resetPagination = true;
			this._selectQueryServiceRequest.removeQueryFacet(facet);

			this._search();
		},

		_onFilterLoadOptions: function(facet) {
			var selectQueryServiceRequestFacet = new SelectQueryServiceRequest({
				arasObj: this._arasObj,
				topFacets: this._topFacets
			});
			selectQueryServiceRequestFacet.rows = 0;
			selectQueryServiceRequestFacet.start = 0;
			selectQueryServiceRequestFacet.queryText = this._selectQueryServiceRequest.queryText;

			selectQueryServiceRequestFacet.setRequestFacet(facet.name);
			var queryFacets = this._selectQueryServiceRequest.getQueryFacets();
			for (var i = 0; i < queryFacets.length; i++) {
				selectQueryServiceRequestFacet.setQueryFacet(queryFacets[i]);
			}
			selectQueryServiceRequestFacet.updateQueryFacet(facet);
			selectQueryServiceRequestFacet.run();

			//there is only one facet in response so return item with zero index
			var newFacet = selectQueryServiceRequestFacet.getResultFacets()[0];
			if (!this._utils.isNullOrUndefined(newFacet)) {
				return newFacet;
			}
			return facet;
		},

		_onFiltersChange: function(filters) {
			var self = this;

			filters.forEach(function(filter) {
				self._selectQueryServiceRequest.updateQueryFacet(filter);
			});

			var currentQueryFacets = this._selectQueryServiceRequest.getQueryFacets();
			var areSameFacetsSelected = this._utils.compareFacets(this._filtersPanelWidget.initialFacets, currentQueryFacets);

			this._utils.setNodeDisabledState(this._filtersPanelWidget.filtersPanelApplyAllButton, areSameFacetsSelected);
			this._utils.setNodeDisabledState(this._filtersPanelWidget.filtersPanelClearAllButton, currentQueryFacets.length === 0);
		},

		_onChipsChange: function(chips) {
			var self = this;

			this._resetPagination = true;
			chips.forEach(function(chip) {
				self._selectQueryServiceRequest.updateQueryFacet(chip);
			});

			this._search();
		}
	});
});
