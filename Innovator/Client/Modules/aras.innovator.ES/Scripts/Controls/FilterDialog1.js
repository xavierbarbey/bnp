define([
	'dojo',
	'dojo/on',
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/TooltipDialog',
	'dijit/popup',
	'ES/Scripts/Controls/FilterDialog2',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/FilterDialog1.html'
], function(dojo, on, declare, lang, _WidgetBase, _TemplatedMixin, TooltipDialog, popup, FilterDialog2, Utils, filterDialog1Template) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,
		_filters: [],
		_changedFiltersIndex: [],

		_onOkButtonClick: null,
		_onCancelButtonClick: null,

		_tooltipDialog: null,
		_aroundNodeId: 'searchPanelShowAllFiltersButton',
		_filterDialog2: null,

		_filterMarkup: '' +
		'<div class="filterDialog1FilterContainer">' +
		'	<span class="boldLabel">{0}</span>' +
		'	<span class="filterDialog1ControlButtonsContainer">' +
		'		<span data-filter-index="{1}" data-dojo-attach-event="onclick: _onClearButtonClickEventHandler" class="linkButton filterDialog1ClearButton">{2}</span>' +
		'		<span class="filterDialog1ButtonSeparator"></span>' +
		'		<span data-filter-index="{1}" data-dojo-attach-event="onclick: _onEditButtonClickEventHandler" class="linkButton">{3}</span>' +
		'	</span>' +
		'	<div data-dojo-attach-point="_filter{1}OptionsContainer" class="filterDialog1OptionsContainer">{4}</div>' +
		'</div>',

		_filterOptionMarkup: '' +
		'<div data-dojo-attach-point="_filter{0}Option{1}Container" class="{2}">' +
		'	<label>' +
		'		<input type="checkbox" {3} data-dojo-attach-point="_filter{0}Option{1}" data-dojo-attach-event="onchange: _onFilterOptionChangedEventHandler" ' +
		'			data-filter-index="{0}" data-filter-option-index="{1}">' +
		'		<span>{4}</span>' +
		'	</label>' +
		'</div>',

		templateString: '',

		constructor: function(args) {
			var self = this;

			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this._onOkButtonClick = args.onOkButtonClick;
			this._onCancelButtonClick = args.onCancelButtonClick;

			//Initialize labels
			var allSelectedFiltersLabel = this._utils.getResourceValueByKey('filters.all_selected_filters');
			var editLabel = this._utils.getResourceValueByKey('filters.edit');
			var clearLabel = this._utils.getResourceValueByKey('filters.clear');
			var clearAllLabel = this._utils.getResourceValueByKey('filters.clear_all');
			var okLabel = this._utils.getResourceValueByKey('buttons.ok');
			var cancelLabel = this._utils.getResourceValueByKey('buttons.cancel');

			//Build markup
			var filtersMarkup = '';
			args.filters.forEach(function(filter) {
				//Skip filters without selected options
				if (!filter.isAnyOptionSelected()) {
					return;
				}

				var filterIndex = self._filters.push(lang.clone(filter)) - 1;

				var filterOptionsMarkup = '';

				var options = filter.getOptions();
				options.forEach(function(option, optionIndex) {
					filterOptionsMarkup += lang.replace(
						self._filterOptionMarkup,
						[filterIndex, optionIndex, !option.isSelected ? 'hidden' : '', option.isSelected ? 'checked' : '', option.label]
					);
				});

				filtersMarkup += lang.replace(self._filterMarkup, [filter.title, filterIndex, clearLabel, editLabel, filterOptionsMarkup]);
			});

			this.templateString = lang.replace(filterDialog1Template, [allSelectedFiltersLabel, filtersMarkup, clearAllLabel, okLabel, cancelLabel]);
		},

		postCreate: function() {
			this._tooltipDialog = new TooltipDialog({
				style: 'width: 510px; outline: none;',
				content: this.domNode
			});
		},

		/**
		 * Show dialog
		 */
		show: function() {
			var self = this;

			var layoutContainer = document.getElementById('layoutContainer');
			if (layoutContainer) {
				cloakNode = document.createElement('div');
				cloakNode.setAttribute('id', 'bodyCloakDiv');
				cloakNode.setAttribute('class', 'bodyCloak');
				layoutContainer.appendChild(cloakNode);
			}

			if (!this._isOpened) {
				popup.open({
					popup: this._tooltipDialog,
					around: this._aroundNodeId,
					onCancel: function() {
						self.hide();
					}
				});

				this._isOpened = true;
			}
		},

		/**
		 * Hide dialog
		 */
		hide: function() {
			popup.close(this._tooltipDialog);
			var layoutContainer = document.getElementById('layoutContainer');
			var bodyCloak = document.getElementById('bodyCloakDiv');
			if (layoutContainer && bodyCloak) {
				layoutContainer.removeChild(bodyCloak);
			}

			this._isOpened = false;
		},

		/**
		 * Reset all checkboxes in specified node
		 *
		 * @param {object} nd HTML node
		 */
		reset: function(nd) {
			var inputs = nd.getElementsByTagName('input');
			for (var i = 0; i < inputs.length; i++) {
				var input = inputs[i];

				input.checked = false;
			}
		},

		/**
		 * Update filters
		 *
		 * @param {boolean} isSelected New state
		 * @param {int} [filterIndex] Index of filter (if not specified, all filters will be affected)
		 * @param {int} [optionIndex] Index of option (if not specified, all options will be affected)
		 * @private
		 */
		_updateFilterSelection: function(isSelected, filterIndex, optionIndex) {
			var self = this;
			var filters = [];
			var options = [];

			if (!this._utils.isNullOrUndefined(filterIndex)) {
				var filter = this._filters[filterIndex];
				if (!this._utils.isNullOrUndefined(filter)) {
					filters.push(filter);
				}

				if (this._changedFiltersIndex.indexOf(filterIndex) === -1) {
					this._changedFiltersIndex.push(filterIndex);
				}
			} else {
				filters = this._filters;

				this._filters.forEach(function(filter, index) {
					if (self._changedFiltersIndex.indexOf(index) === -1) {
						self._changedFiltersIndex.push(index);
					}
				});
			}

			filters.forEach(function(filter) {
				if (!self._utils.isNullOrUndefined(optionIndex)) {
					var option = filter.getOptions()[optionIndex];
					if (!self._utils.isNullOrUndefined(option)) {
						options.push(option);
					}
				} else {
					options = options.concat(filter.getOptions());
				}
			});

			options.forEach(function(option) {
				option.isSelected = isSelected;
			});
		},

		/**
		 * Update checkboxes related to filters
		 *
		 * @param {boolean} isSelected New state
		 * @param {int} [filterIndex] Index of filter
		 * @param {int} [optionIndex] Index of option
		 * @private
		 */
		_updateCheckboxSelection: function(isSelected, filterIndex, optionIndex) {
			var self = this;
			var checkboxNds = [];
			var filter = null;
			var updateCheckboxState = function(checkboxNd, isSelected) {
				if (self._utils.isNullOrUndefined(checkboxNd)) {
					return;
				}

				var filterIndex = +checkboxNd.getAttribute('data-filter-index');
				var optionIndex = +checkboxNd.getAttribute('data-filter-option-index');

				if (isSelected) {
					//Show container if it's not visible
					var filterOptionContainerNd = self[lang.replace('_filter{0}Option{1}Container', [filterIndex, optionIndex])];
					if (!self._utils.isNullOrUndefined(filterOptionContainerNd)) {
						self._utils.setNodeVisibility(filterOptionContainerNd, true);
					}
				}

				checkboxNd.checked = isSelected;
			};

			if (arguments.length === 1) {
				this._filters.forEach(function(filter, filterIndex) {
					var options = filter.getOptions();
					options.forEach(function(option, optionIndex) {
						var checkboxNd = self[lang.replace('_filter{0}Option{1}', [filterIndex, optionIndex])];
						checkboxNds.push(checkboxNd);
					});
				});
			}

			if (arguments.length === 2) {
				filter = this._filters[filterIndex];
				if (!this._utils.isNullOrUndefined(filter)) {
					var options = filter.getOptions();
					options.forEach(function(option, optionIndex) {
						var checkboxNd = self[lang.replace('_filter{0}Option{1}', [filterIndex, optionIndex])];
						checkboxNds.push(checkboxNd);
					});
				}

			}

			if (arguments.length === 3) {
				filter = this._filters[filterIndex];
				if (!this._utils.isNullOrUndefined(filter)) {
					var option = filter.getOptions()[optionIndex];
					if (!this._utils.isNullOrUndefined(option)) {
						var checkboxNd = this[lang.replace('_filter{0}Option{1}', [filterIndex, optionIndex])];
						checkboxNds.push(checkboxNd);
					}
				}
			}

			checkboxNds.forEach(function(checkboxNd) {
				updateCheckboxState(checkboxNd, isSelected);
			});
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onOkButtonClickEventHandler: function() {
			var self = this;

			var changedFilters = [];
			this._changedFiltersIndex.forEach(function(changedFilterIndex) {
				changedFilters.push(self._filters[changedFilterIndex]);
			});

			this.hide();

			if (!this._utils.isNullOrUndefined(this._onOkButtonClick)) {
				this._onOkButtonClick(changedFilters);
			}

			this._changedFiltersIndex.length = 0;
		},

		_onCancelButtonClickEventHandler: function() {
			this.hide();

			if (!this._utils.isNullOrUndefined(this._onCancelButtonClick)) {
				this._onCancelButtonClick();
			}

			this._changedFiltersIndex.length = 0;
		},

		_onFilterOptionChangedEventHandler: function(ev) {
			var filterIndex = +ev.target.getAttribute('data-filter-index');
			var optionIndex = +ev.target.getAttribute('data-filter-option-index');

			this._updateFilterSelection(ev.target.checked, filterIndex, optionIndex);
		},

		_onEditButtonClickEventHandler: function(ev) {
			var self = this;
			var filterIndex = +ev.target.getAttribute('data-filter-index');
			var filter = this._filters[filterIndex];

			if (!this._utils.isNullOrUndefined(filter)) {
				this.hide();

				//Destroy old dialog
				if (!this._utils.isNullOrUndefined(this._filterDialog2)) {
					this._filterDialog2.destroy();
				}

				this._filterDialog2 = FilterDialog2({
					arasObj: this._arasObj,
					facet: filter,
					onOkButtonClick: function(filter) {
						var options = filter.getOptions();
						options.forEach(function(option, optionIndex) {
							self._updateFilterSelection(option.isSelected, filterIndex, optionIndex);
							self._updateCheckboxSelection(option.isSelected, filterIndex, optionIndex);
						});

						self.show();
					},
					onCancelButtonClick: function() {
						self.show();
					}
				});
				this._filterDialog2.show(this._aroundNodeId);
			}
		},

		_onClearButtonClickEventHandler: function(ev) {
			var filterIndex = +ev.target.getAttribute('data-filter-index');

			this._updateFilterSelection(false, filterIndex);
			this._updateCheckboxSelection(false, filterIndex);
		},

		_onClearAllButtonClickEventHandler: function() {
			this._updateFilterSelection(false);
			this._updateCheckboxSelection(false);
		}
	});
});
