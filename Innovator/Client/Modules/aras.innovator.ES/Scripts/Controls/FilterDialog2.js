define([
	'dojo',
	'dojo/query',
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/TooltipDialog',
	'dijit/popup',
	'ES/Scripts/Classes/Utils',
	'ES/Scripts/Constants',
	'dojo/text!./../../Views/Templates/FilterDialog2.html',
	'dojo/text!./../../Views/Templates/FilterDialog2FilterOption1.html'
], function(dojo,
			query,
			declare,
			lang,
			_WidgetBase,
			_TemplatedMixin,
			TooltipDialog,
			popup,
			Utils,
			Constants,
			filterDialog2Template,
			filterDialog2FilterOption1) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,
		_constants: null,

		_popupDialog: null,
		_tooltipDialog: null,

		_onPopupClose: null,

		facet: null,

		_domNodeOptionIndexesByName: new Map(),

		currentSortOrder: '',
		currentSortMode: '',

		firstLetterGroupId: -1,

		templateString: '',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this._constants = new Constants();

			this.currentSortOrder = this._constants.sortOrders.ascending;
			this.currentSortMode = this._constants.sortModes.alphabetical;

			this.facet = args.facet;
			this.showCounts = args.showCounts;

			this._onPopupClose = args.onPopupClose;

			var firstLetterWasChanged = false;
			var filterOptionsMarkup = '';
			var prevCharacter = '';
			var countHidden = this.showCounts ? '' : 'hidden';

			var options = this.facet.getOptions();
			options.forEach(function(option, index) {
				var firstLetter = option.label === '' ? '' : option.label[0].toUpperCase();
				if (this._arasObj.isInteger(firstLetter)) {
					firstLetter = '#';
				}
				firstLetterWasChanged = (firstLetter !== prevCharacter);

				filterOptionsMarkup += lang.replace(filterDialog2FilterOption1, {
					index: index,
					name: option.name,
					value: option.label,
					count: option.count,
					countHidden: countHidden,
					checked: option.isSelected ? 'checked' : '',
					firstLetter: firstLetter,
					firstLetterGroupId: firstLetterWasChanged ? ++this.firstLetterGroupId : this.firstLetterGroupId
				});

				prevCharacter = firstLetter;
			}.bind(this));

			this.templateString = lang.replace(filterDialog2Template,
				[filterOptionsMarkup, countHidden, args.selectAllTooltip, args.sortAlphaModeTooltip, args.sortFreqModeTooltip]);
		},

		postCreate: function() {
			this._tooltipDialog = new TooltipDialog({
				content: this.domNode,
				class: 'filterDialog'
			});

			var options = this.facet.getOptions();
			options.forEach(function(option, index) {
				this._domNodeOptionIndexesByName.set(option.name, index);
			}.bind(this));
		},

		/**
		 * Show dialog
		 *
		 * @param {object|string} node DOM node or id of node for placing the pop-up
		 */
		show: function(node) {
			popup.open({
				popup: this._tooltipDialog,
				around: node,
				maxHeight: dojo.window.getBox().h,
				orient: ['after-centered', 'below'],
				onCancel: function() {
					this.hide();
				}.bind(this)
			});

			this._popupDialog = popup.getTopPopup();

			this._updateSelectAllCheckBox();
			this._updateFirstLetters();
		},

		/**
		 * Hide dialog
		 */
		hide: function() {
			this.facet.getOptions().forEach(function(option) {
				option.visible = true;
			});

			if (!this._utils.isNullOrUndefined(this._onPopupClose)) {
				this._onPopupClose(this.facet);
			}

			this._popupDialog.widget.destroyRecursive();
		},

		/**
		 * Update checkbox 'Select All'
		 *
		 * @private
		 */
		_updateSelectAllCheckBox: function() {
			var facetOptionsState = this.facet.getFacetOptionsState();
			var facetStateClass = facetOptionsState.areAllVisibleOptionsSelected ? 'checked' :
				!facetOptionsState.isAnyVisibleOptionSelected ? 'unchecked' : 'indet';

			this._filterDialog2SelectAllButton.setAttribute('data-state', facetStateClass);

			if (facetOptionsState.isAnyOptionVisible) {
				this._filterDialog2SelectAllButton.removeAttribute('disabled');
				this._alphabeticalSortButton.removeAttribute('disabled');
				this._frequencySortButton.removeAttribute('disabled');
			} else {
				this._filterDialog2SelectAllButton.setAttribute('disabled', 'disabled');
				this._alphabeticalSortButton.setAttribute('disabled', 'disabled');
				this._frequencySortButton.setAttribute('disabled', 'disabled');
			}
		},

		/**
		 * Hides or shows first letters
		 *
		 * @private
		 */
		_updateFirstLetters: function() {
			for (var i = 0; i <= this.firstLetterGroupId; i++) {
				var nodesByFirstLetter = query('.firstLetterGroup' + i + ':not(.hidden)', this.domNode);

				var firstOptionIndex = this.currentSortOrder === this._constants.sortOrders.ascending ? 0 : nodesByFirstLetter.length - 1;
				for (var j = 0; j < nodesByFirstLetter.length; j++) {
					var nodeByFirstLetter = nodesByFirstLetter[j];

					this._utils.switchClassByCondition(nodeByFirstLetter, j === firstOptionIndex, 'firstLetterVisible');
				}
			}
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onSearchTextBoxChangedEventHandler: function() {
			var options = this.facet.getOptions();
			var textBoxValueLowered = this._filterDialog2SearchTextBox.value.toLowerCase();

			for (var i = 0; i < options.length; i++) {
				var option = options[i];
				var isVisible = option.label.toLowerCase().indexOf(textBoxValueLowered) !== -1;

				option.visible = isVisible;
				this._utils.switchClassByCondition(this['filterOptionContainer' + i], !isVisible, 'hidden');
			}

			this._updateSelectAllCheckBox();
			this._updateFirstLetters();
		},

		_onFilterOptionStateChangedEventHandler: function(ev) {
			var name = ev.target.getAttribute('data-filter-option-name');
			var option = this.facet.getOption(name);

			option.isSelected = ev.target.checked;

			this._updateSelectAllCheckBox();
		},

		_onSelectAllCheckboxClickEventHandler: function() {
			var options = this.facet.getOptions();
			var selected = this._filterDialog2SelectAllButton.getAttribute('data-state') !== 'checked';

			for (var i = 0; i < options.length; i++) {
				var option = options[i];

				if (!option.visible) {
					continue;
				}

				option.isSelected = selected;
				this['filterOptionCheckBox' + i].checked = selected;
			}

			this._updateSelectAllCheckBox();
		},

		_onSortButtonsClickEventHandler: function(ev) {
			var prevSortMode = this.currentSortMode;
			var prevSortOrder = ev.currentTarget.getAttribute('data-filter-control-sort-order');
			var newSortMode = ev.currentTarget.getAttribute('data-filter-control-sort-mode');
			var options = [];

			if (prevSortMode === newSortMode) {
				this.currentSortOrder = (prevSortOrder === this._constants.sortOrders.ascending) ?
					this._constants.sortOrders.descending : this._constants.sortOrders.ascending;
			} else {
				this.currentSortOrder = (prevSortMode !== this._constants.sortModes.frequency) ?
					this._constants.sortOrders.descending : this._constants.sortOrders.ascending;

				this.currentSortMode = newSortMode;

				this._utils.swapClass(this._alphabeticalSortButton, this._frequencySortButton, 'active');
			}

			switch (this.currentSortMode) {
				case this._constants.sortModes.alphabetical:
					options = this.facet.getSortedOptionsByAlphabet(this.currentSortOrder);

					this._alphabeticalSortButton.setAttribute('data-filter-control-sort-order', this.currentSortOrder);
					break;
				case this._constants.sortModes.frequency:
					options = this.facet.getSortedOptionsByFrequency(this.currentSortOrder);

					this._frequencySortButton.setAttribute('data-filter-control-sort-order', this.currentSortOrder);
			}

			this._filterDialogContentContainer.setAttribute('data-filter-sort-mode', this.currentSortMode);
			this._filterDialogContentContainer.setAttribute('data-filter-sort-order', this.currentSortOrder);

			options.forEach(function(option, index) {
				var domNodeOptionIndex = this._domNodeOptionIndexesByName.get(option.name);
				this['filterOptionContainer' + domNodeOptionIndex].style.order = index;
			}.bind(this));

			if (this._constants.sortModes.alphabetical) {
				this._updateFirstLetters();
			}
		}
	});
});
