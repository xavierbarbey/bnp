define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'ES/Scripts/Controls/SyntaxHelpDialog',
	'ES/Scripts/Controls/SearchPanelFilterOption',
	'ES/Scripts/Controls/FilterDialog1',
	'ES/Scripts/Classes/Utils',
	'dojo/text!./../../Views/Templates/SearchPanel.html'
], function(declare, lang, _WidgetBase, _TemplatedMixin, SyntaxHelpDialog, SearchPanelFilterOption, FilterDialog1, Utils, searchPanelTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		arasObj: null,

		_syntaxHelpDialog: null,

		widgets: [],

		facets: [],

		_filterDialog1: null,

		_onSearch: null,
		_onChipsChange: null,

		templateString: '',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});

			this._onSearch = args.onSearch;
			this._onChipsChange = args.onChipsChange;

			var runSearchHint = this._utils.getResourceValueByKey('hint.run_search');
			var searchButtonLabel = this._utils.getResourceValueByKey('buttons.search');

			this.templateString = lang.replace(
				searchPanelTemplate,
				[runSearchHint, searchButtonLabel]
			);

			window.addEventListener('resize', this._updateButtons.bind(this));
		},

		/**
		 * Get query text
		 *
		 * @returns {string}
		 */
		getQueryText: function() {
			return this._searchTextBox.value;
		},

		/**
		 * Set query text
		 *
		 * @param {string} value
		 */
		setQueryText: function(value) {
			this._searchTextBox.value = value;
		},

		/**
		 * Update widget
		 */
		update: function() {
			var self = this;

			//Destroy old widgets
			this.widgets.forEach(function(widget) {
				widget.destroy();
			});
			this.widgets = [];

			//Create new widgets
			var docFragment = document.createDocumentFragment();
			this.facets.forEach(function(facet, index) {
				var options = facet.getOptions();
				options.forEach(function(option) {
					if (option.isSelected) {
						var widget = new SearchPanelFilterOption({
							arasObj: self._arasObj,
							filterIndex: index,
							filterOptionName: option.name,
							filterOptionLabel: option.label,
							onRemoveFilterOption: self._onRemoveFilterOptionClickEventHandler.bind(self)
						});
						docFragment.appendChild(widget.domNode);

						self.widgets.push(widget);
					}
				});
			});
			this._filterOptionsContainer.appendChild(docFragment);

			this._updateButtons();
		},

		/**
		 * Update state of widget buttons
		 *
		 * @private
		 */
		_updateButtons: function() {
			//Update state of expand button
			if (this._isFilterOptionsContainerOverflowed()) {
				this._filterOptionsShowButton.style.visibility = 'visible';
			} else {
				this._filterOptionsShowButton.style.visibility = 'hidden';
			}
		},

		_isFilterOptionsContainerOverflowed: function() {
			return this._filterOptionsContainer.scrollHeight - 5 > this._filterOptionsContainer.clientHeight;
		},

		/**
		 * Open syntax help dialog
		 *
		 */
		_openSyntaxHelpDialog: function() {
			this._syntaxHelpDialog = new SyntaxHelpDialog({
				arasObj: this._arasObj
			});
			this._syntaxHelpDialog.show();
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onSearchPanelSearchButtonClickEventHandler: function() {
			if (!this._utils.isNullOrUndefined(this._onSearch)) {
				this._onSearch();
			}
		},

		_onExpandButtonClickEventHandler: function(ev) {
			//Destroy old dialog
			if (!this._utils.isNullOrUndefined(this._filterDialog1)) {
				this._filterDialog1.destroy();
			}

			this._filterDialog1 = new FilterDialog1({
				arasObj: this._arasObj,
				filters: this.facets,
				onOkButtonClick: function(changedFilters) {
					if (!this._utils.isNullOrUndefined(this._onChipsChange)) {
						this._onChipsChange(changedFilters);
					}
				}.bind(this)
			});
			this._filterDialog1.show();
			ev.stopPropagation();
		},

		_onRemoveFilterOptionClickEventHandler: function(filterIndex, filterOptionName) {
			//Deselect filter option
			var facet = this.facets[filterIndex];

			if (!this._utils.isNullOrUndefined(facet)) {
				var option = facet.getOption(filterOptionName);

				if (!this._utils.isNullOrUndefined(option)) {
					option.isSelected = false;
				}
			}

			if (!this._utils.isNullOrUndefined(this._onChipsChange)) {
				this._onChipsChange([facet]);
			}
		},

		_onSyntaxHelpButtonClickEventHandler: function(ev) {
			this._openSyntaxHelpDialog();
			ev.stopPropagation();
		}
	});
});
