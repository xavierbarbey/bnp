define([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'ES/Scripts/Classes/Utils',
	'ES/Scripts/Controls/FilterItem',
	'dojo/text!./../../Views/Templates/FiltersPanel.html'
], function(declare, lang, _WidgetBase, _TemplatedMixin, Utils, FilterItem, FiltersPanelTemplate) {
	return declare([_WidgetBase, _TemplatedMixin], {
		_arasObj: null,
		_utils: null,

		_onClearAll: null,
		_onFiltersApply: null,
		_onFiltersChange: null,
		_onFiltersReset: null,
		_onFilterReset: null,
		_onFilterLoadOptions: null,
		_onFiltersPanelTextBoxDelay: 250,
		_onFiltersPanelTextBoxDelayTimer: null,

		//Labels
		_moreLabel: '',
		_clearTooltip: '',
		_selectAllTooltip: '',
		_sortAlphaModeTooltip: '',
		_sortFreqModeTooltip: '',

		_hasScroll: false,

		items: [],
		widgets: [],
		initialFacets: [],

		itemsWithHiddenCount: ['aes_root_types'],

		templateString: '',

		baseClass: 'resultItem',

		constructor: function(args) {
			this._arasObj = args.arasObj;
			this._utils = new Utils({
				arasObj: this._arasObj
			});
			this._onFiltersApply = args.onFiltersApply;
			this._onFiltersChange = args.onFiltersChange;
			this._onFiltersReset = args.onFiltersReset;
			this._onFilterReset = args.onFilterReset;
			this._onFilterLoadOptions = args.onFilterLoadOptions;

			//Initialize labels
			var filtersLabel = this._utils.getResourceValueByKey('filters.filters');
			var filtersPlaceholder = this._utils.getResourceValueByKey('filters.placeholder');
			var clearAllLabel = this._utils.getResourceValueByKey('filters.clear_all');
			var applyLabel = this._utils.getResourceValueByKey('filters.apply');

			this._moreLabel = this._utils.getResourceValueByKey('filters.more');
			this._clearTooltip = this._utils.getResourceValueByKey('filters.clear');
			this._selectAllTooltip = this._utils.getResourceValueByKey('filters.select_all');
			this._sortAlphaModeTooltip = this._utils.getResourceValueByKey('filters.sort_alpha_mode');
			this._sortFreqModeTooltip = this._utils.getResourceValueByKey('filters.sort_freq_mode');

			this.templateString = lang.replace(FiltersPanelTemplate,
				[filtersLabel, filtersPlaceholder, clearAllLabel, applyLabel]
			);
		},

		/**
		 * Update widget
		 */
		update: function() {
			var self = this;

			//Destroy old widgets
			this.widgets.forEach(function(widget) {
				widget.destroy();
			});
			this.widgets = [];

			//Create new widgets
			var docFragment = document.createDocumentFragment();
			this.items.forEach(function(item) {
				var widget = new FilterItem({
					arasObj: self._arasObj,
					item: item,
					disableScroll: self._disableScroll.bind(self),
					onFilterChange: self._onFilterChangeEventHandler.bind(self),
					onFilterReset: self._onFilterReset,
					onFilterLoadOptions: self._onFilterLoadOptions,
					clearTooltip: self._clearTooltip,
					moreLabel: self._moreLabel,
					selectAllTooltip: self._selectAllTooltip,
					sortAlphaModeTooltip: self._sortAlphaModeTooltip,
					sortFreqModeTooltip: self._sortFreqModeTooltip,
					topFilter: item.isTop,
					showCounts: (self.itemsWithHiddenCount.indexOf(item.name) === -1)
				});
				docFragment.appendChild(widget.domNode);

				self.widgets.push(widget);
			});

			this._filtersPanelOptionsContainer.appendChild(docFragment);
			this._filtersPanelOptionsContainer.scrollTop = 0;

			this._utils.setNodeDisabledState(this.filtersPanelApplyAllButton, true);
			this._utils.setNodeDisabledState(this.filtersPanelClearAllButton, this.initialFacets.length === 0);

			this._hasScroll = this._filtersPanelOptionsContainer.scrollHeight > this._filtersPanelOptionsContainer.clientHeight;

			this.filtersPanelTextBox.value = '';
		},

		/**
		 * Disables/enables filter's panel scroll
		 *
		 * @param {boolean} disable Should scroll be disabled
		 * @private
		 */
		_disableScroll: function(disable) {
			this._utils.switchClassByCondition(this.domNode, this._hasScroll && disable, 'noScroll');
		},

		/*------------------------------------------------------------------------------------------------------------*/
		//Event handlers

		_onFilterChangeEventHandler: function(filter) {
			if (!this._utils.isNullOrUndefined(this._onFiltersChange)) {
				this._onFiltersChange([filter]);
			}
		},

		_onFiltersPanelClearAllButtonClickEventHandler: function() {
			if (!this._utils.isNullOrUndefined(this._onFiltersReset)) {
				this._onFiltersReset();
			}
		},

		_onFiltersPanelApplyAllButtonClickEventHandler: function() {
			if (!this._utils.isNullOrUndefined(this._onFiltersApply)) {
				this._onFiltersApply();
			}
		},

		_onFiltersPanelTextBoxKeyPressEventHandler: function() {
			var searchText = this.filtersPanelTextBox.value.toLowerCase();
			var handler = function(searchText) {
				this.widgets.forEach(function(widget) {
					var isWidgetVisible = true;
					if (searchText !== '') {
						isWidgetVisible = widget.item.title.toLowerCase().indexOf(searchText) !== -1;
					}
					this._utils.setNodeVisibility(widget.domNode, isWidgetVisible);
				}.bind(this));
			}.bind(this);

			this._onFiltersPanelTextBoxDelayTimer = this._utils.execWithDelay(
				handler.bind(this, searchText),
				this._onFiltersPanelTextBoxDelay,
				this._onFiltersPanelTextBoxDelayTimer);
		}
	});
});
