﻿require([
	'dojo/ready',
	'ES/Scripts/Classes/Page/InternalReindex'
], function(ready, InternalReindex) {
	var InternalReindexPage = new InternalReindex({
		arasObj: parent.parent.aras
	});
	InternalReindexPage.init();
});
