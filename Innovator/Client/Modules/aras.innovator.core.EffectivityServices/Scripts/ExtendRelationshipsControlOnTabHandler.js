﻿document.addEventListener('beforeLoadWidgets', function() {
	if (window.itemTypeName !== 'Part') {
		return;
	}

	function registerKeyboardShortcuts(contentWindow) {
		const shortcutsLoadParameters = {
			locationName: 'ItemWindowRelationshipsShortcuts',
			item_classification: '%all_grouped_by_classification%'
		};
		const settings = {
			windows: [contentWindow],
			context: contentWindow,
			registerChildFrames: true
		};
		window.cui.loadShortcutsFromCommandBarsAsync(shortcutsLoadParameters, settings);

		if (window.ITEM_WINDOW) {
			window.ITEM_WINDOW.registerStandardShortcuts(contentWindow, true, true);
		}

		if (window.returnBlockerHelper) {
			window.returnBlockerHelper.attachBlocker(contentWindow);
		}
	}

	const relationshipsWithSplitters = new Set([
		// "Part BOM" relationshiptype ID
		'159C6D88795B4A86864420863466F728',
		// "Part MultiLevel BOM" relationshiptype ID
		'567E4149FBF74DACA0B0C4C9B1E79A3B'
	]);
	let isOnTabHandlerAlreadyRunning;

	window.RelationshipsOverriddenFunctions = Object.assign(
		window.RelationshipsOverriddenFunctions || {},
		{
			onTab: function(targetTabId) {
				if (isOnTabHandlerAlreadyRunning) {
					return true;
				}

				const relationshipsControl = window.relationshipsControl;
				const currentTabId = relationshipsControl.currTabID;

				if (currentTabId === targetTabId || !relationshipsWithSplitters.has(targetTabId)) {
					return;
				}

				const iframesCollection = relationshipsControl.iframesCollection;
				let relationshipMainIframe = iframesCollection && iframesCollection[targetTabId] || document.getElementById(targetTabId);

				if (relationshipMainIframe) {
					return;
				}

				relationshipMainIframe = document.createElement('iframe');
				relationshipMainIframe.id = targetTabId;
				relationshipMainIframe.classList.add('effs-relationship-container__pane-container');
				relationshipMainIframe.classList.add('effs-relationship-container__pane-content-iframe');
				relationshipMainIframe.classList.add('aras-flex-grow');

				const splitter = document.createElement('div');
				splitter.id = targetTabId + '_splitter';
				splitter.classList.add('aras-splitter');
				splitter.classList.add('aras-hide');

				const rightSplitterPane = document.createElement('div');
				rightSplitterPane.id = targetTabId + '_right_splitter_pane';
				rightSplitterPane.classList.add('effs-relationship-container__pane-container');
				rightSplitterPane.classList.add('effs-relationship-container__pane-container-right');
				rightSplitterPane.classList.add('effs-relationship-container__pane-container_min-width-100');
				rightSplitterPane.classList.add('aras-hide');

				const paneContainer = document.createElement('div');
				paneContainer.id = targetTabId + '-pane-container';
				paneContainer.classList.add('effs-relationship-container');

				paneContainer.appendChild(relationshipMainIframe);
				paneContainer.appendChild(splitter);
				paneContainer.appendChild(rightSplitterPane);

				const relTabbar = relationshipsControl.relTabbar;

				//selectTab(targetTabId) method call triggers onTab(targetTabId) handler and "isOnTabHandlerAlreadyRunning" flag is used to break recursive calls
				isOnTabHandlerAlreadyRunning = true;
				relTabbar.selectTab(targetTabId);
				isOnTabHandlerAlreadyRunning = false;

				relTabbar._getTab(targetTabId).domNode.appendChild(paneContainer);
				window.ArasModules.splitter(splitter);

				const onLoadHandler = function(e) {
					const spinnerContainer = relTabbar.containerNode;
					window.aras.browserHelper.toggleSpinner(spinnerContainer, false, 'dimmer_spinner_relship');

					registerKeyboardShortcuts(e.target.contentWindow);
				};
				relationshipMainIframe.addEventListener('load', onLoadHandler);
			}
		}
	);
});
