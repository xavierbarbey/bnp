﻿(function(window) {
	'use strict';

	function EffectivityExpressionEditorViewController(parameters) {
		this.aras = parameters.aras;
		this.expressionItemNode = parameters.expressionItemNode;
		this._isEditMode = parameters.viewMode !== 'view';

		this._init(
			parameters.viewMode,
			parameters.applyButtonConnectId,
			parameters.applyButtonOnClickHandler,
			parameters.cancelButtonConnectId,
			parameters.cancelButtonOnClickHandler,
			parameters.editorIframeConnectId);
	}

	EffectivityExpressionEditorViewController.prototype = {
		constructor: EffectivityExpressionEditorViewController,

		aras: null,

		expressionItemNode: null,

		_applyButtonElement: null,

		_isEditMode: false,

		_isScopeSelected: false,

		_isEffectivityExpressionValid: false,

		_effsModuleSolutionBasedRelativePath: '../Modules/aras.innovator.core.EffectivityServices',

		_init: function(
				viewMode,
				applyButtonConnectId,
				applyButtonOnClickHandler,
				cancelButtonConnectId,
				cancelButtonOnClickHandler,
				editorIframeConnectId) {
			this._setupApplyButton(applyButtonConnectId, applyButtonOnClickHandler);
			this._setupCancelButton(cancelButtonConnectId, cancelButtonOnClickHandler);

			const editorFrameContentWindow = document.getElementById(editorIframeConnectId);
			//Id represents the 'effs_expression' form
			this._showFormInFrame('5D991BF16A404420BC38D2C895C9E6E1', editorFrameContentWindow, viewMode, this.expressionItemNode);
		},

		_setupApplyButton: function(applyButtonConnectId, applyButtonOnClickHandler) {
			this._applyButtonElement = document.getElementById(applyButtonConnectId);
			this._setupButton(
				this._applyButtonElement,
				'effectivity_expression_editor.apply_button_label',
				function() { applyButtonOnClickHandler(this.expressionItemNode); }.bind(this));
			this._setElementState(this._applyButtonElement, false);
			this._setupListenerForUpdatingApplyButtonState();
		},

		_setupListenerForUpdatingApplyButtonState: function() {
			const updateApplyButtonState = function() {
				this._setElementState(this._applyButtonElement, this._isEditMode && this.isExpressionItemValid());
			}.bind(this);

			document.addEventListener(
				'effectivityExpressionInputChange',
				function(e) {
					this._isEffectivityExpressionValid = e.detail.isInputExpressionValid;
					updateApplyButtonState();
				}.bind(this));

			document.addEventListener(
				'effectivityScopeChange',
				function(e) {
					this._isScopeSelected = !!this.aras.uiGetItemByKeyedName('effs_scope', e.detail.scopeInputValue, true);
					updateApplyButtonState();
				}.bind(this));
		},

		_setupCancelButton: function(cancelButtonConnectId, cancelButtonOnClickHandler) {
			const cancelButtonElement = document.getElementById(cancelButtonConnectId);
			this._setupButton(
				cancelButtonElement,
				'effectivity_expression_editor.cancel_button_label',
				function() { cancelButtonOnClickHandler(this.expressionItemNode, this.isExpressionItemValid()); }.bind(this));
		},

		_setupButton: function(buttonElement, textContentResource, onClickEventListener) {
			buttonElement.firstElementChild.textContent = this.aras.getResource(this._effsModuleSolutionBasedRelativePath, textContentResource);
			buttonElement.addEventListener('click', onClickEventListener);
		},

		_setElementState: function(element, isEnabled) {
			element.disabled = !isEnabled;
		},

		_showFormInFrame: function(formId, frameContentWindow, viewMode, itemNode) {
			const formItem = this.aras.getFormForDisplay(formId);
			const formItemNode = formItem.node.cloneNode(true);

			const cssNode = formItemNode.selectSingleNode('Relationships/Item[@type="Body"]/css');
			ArasModules.xml.setText(cssNode, '@import "../Modules/aras.innovator.core.EffectivityServices/Styles/effectivityExpressionForm.css"');

			const itemTypeName = itemNode.getAttribute('type');
			const itemTypeItemNode = this.aras.getItemTypeNodeForClient(itemTypeName, 'name');

			this._bindFormItemFieldsToItemTypeItemProperties(formItemNode, itemTypeItemNode);

			this.aras.uiShowItemInFrameEx(frameContentWindow, itemNode, viewMode, 0, formItemNode, itemTypeItemNode);
		},

		_bindFormItemFieldsToItemTypeItemProperties: function(formItemNode, itemTypeItemNode) {
			const itemDataTypePropertyNodes = itemTypeItemNode.selectNodes('Relationships/Item[@type="Property" and data_type="item"]');
			if (!itemDataTypePropertyNodes.length) {
				return;
			}

			const itemDataTypePropertyIdsByNames =
				Array.prototype.reduce.call(itemDataTypePropertyNodes, function(accumulator, propertyItemNode) {
					const propertyId = this.aras.getItemProperty(propertyItemNode, 'id');
					const propertyName = this.aras.getItemProperty(propertyItemNode, 'name');
					accumulator[propertyName] = propertyId;

					return accumulator;
				}.bind(this), {});

			const lookupFormItemFieldsCondition = Object.keys(itemDataTypePropertyIdsByNames)
				.map(function(propertyName) {
					return '@keyed_name="' + propertyName + '"';
				})
				.join(' or ');

			const formItemFieldNodes =
				formItemNode.selectNodes(
					'Relationships/Item[@type="Body"]' +
						'/Relationships/Item[@type="Field" and field_type="item" and propertytype_id[' + lookupFormItemFieldsCondition + ']]');
			Array.prototype.forEach.call(formItemFieldNodes, function(formItemFieldNode) {
				const propertyTypeIdNode = formItemFieldNode.selectSingleNode('propertytype_id');

				const propertyTypeIdKeyedNameAttributeValue = propertyTypeIdNode.getAttribute('keyed_name');
				ArasModules.xml.setText(propertyTypeIdNode, itemDataTypePropertyIdsByNames[propertyTypeIdKeyedNameAttributeValue]);
			}.bind(this));
		},

		isExpressionItemValid: function() {
			return this._isScopeSelected && this._isEffectivityExpressionValid;
		}
	};

	window.EffectivityExpressionEditorViewController = EffectivityExpressionEditorViewController;
}(window));
