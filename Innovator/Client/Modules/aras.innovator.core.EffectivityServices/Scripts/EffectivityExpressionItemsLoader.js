﻿define(['dojo/_base/declare'], function(declare) {
	return declare(null, {
		aras: null,

		_effectiveItemTypeId: null,

		_effectivityExpressionItemTypeName: null,

		constructor: function(aras, effectiveItemTypeId) {
			this.aras = aras;
			this._effectiveItemTypeId = effectiveItemTypeId;

			Object.defineProperty(this, 'effectivityExpressionItemTypeName', {
				get: function() {
					if (!this._effectivityExpressionItemTypeName) {
						const scopeItemTypeRelationship = this._getRelationshipFromScopeToEffectiveItemType(this._effectiveItemTypeId);

						if (!scopeItemTypeRelationship.isError()) {
							this._effectivityExpressionItemTypeName = scopeItemTypeRelationship.getPropertyAttribute('effs_expression_itemtype_id', 'name');
						}
					}

					return this._effectivityExpressionItemTypeName;
				},
				enumerable: true
			});
		},

		getExpressionItems: function(effectiveItemId, expressionItemPropertiesToSelect) {
			const effectivityExpressionItemTypeName = this.effectivityExpressionItemTypeName;

			if (!effectivityExpressionItemTypeName) {
				return this.aras.IomInnovator.newError('No Effectivity Expression ItemType found');
			}

			const effectivityExpressionItems = this.aras.newIOMItem(effectivityExpressionItemTypeName, 'get');
			effectivityExpressionItems.setAttribute(
				'select',
				typeof expressionItemPropertiesToSelect === 'string' ? expressionItemPropertiesToSelect : 'effs_scope_id,string_notation'
			);
			effectivityExpressionItems.setProperty('source_id', effectiveItemId);

			return effectivityExpressionItems.apply();
		},

		_getRelationshipFromScopeToEffectiveItemType: function(effectiveItemTypeId) {
			const scopeItemTypeRelationship = this.aras.newIOMItem('effs_scope_itemtype', 'get');
			scopeItemTypeRelationship.setAttribute('select', 'effs_expression_itemtype_id');
			scopeItemTypeRelationship.setAttribute('maxRecords', '1');
			scopeItemTypeRelationship.setProperty('related_id', effectiveItemTypeId);

			return scopeItemTypeRelationship.apply();
		}
	});
});
