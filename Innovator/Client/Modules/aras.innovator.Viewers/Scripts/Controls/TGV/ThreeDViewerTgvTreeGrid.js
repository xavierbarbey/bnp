﻿define(['dojo/_base/declare',
		'TreeGridView/Scripts/Viewer/TgvTreeGrid'],
	function(declare, TgvTreeGrid) {
		return declare(TgvTreeGrid, {
			isToShowOnlyHeaders: false,

			addRows: function() {
				if (this.isToShowOnlyHeaders) {
					return;
				}
				return this.inherited(arguments);
			},

			expandAll: function() {
				if (this.isToShowOnlyHeaders) {
					return;
				}
				return this.inherited(arguments);
			}
		});
	});

