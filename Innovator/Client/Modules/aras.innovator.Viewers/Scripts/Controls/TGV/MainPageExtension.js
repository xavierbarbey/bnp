﻿define(['dojo/_base/declare',
		'dojo/_base/connect',
		'./PathManager'],
	function(declare, connect, PathManager) {
		return declare([], {
			_pathManager: new PathManager(),

			constructor: function(args) {
				var customObject = parent.customObject;
				var self = this;
				var callback = function() {
					//TODO: change the alert.//AlertError or AlertWarning?
					alert('Select failed. Perhaps some items were deleted/versioned.');
				};
				self.threeDViewerSetOnNotFoundItemCallback(callback);
				if (customObject) {
					let eventHandler = connect.connect(customObject, 'onSelectItemOnModel', function(pathsStr) {
						self.threeDViewerSelectByPaths(pathsStr);
					});
					this._eventHandlers.push(eventHandler);
				}
			},

			threeDViewerSetOnNotFoundItemCallback: function(callback) {
				this._pathManager.setOnNotFoundItemCallback(callback);
			},

			threeDViewerSelectByPaths: function(pathsStr) {
				this._pathManager.selectByPaths(pathsStr);
			},

			threeDViewerGetPathsOfSelectedNode: function() {
				var paths = this._pathManager.getPathsOfSelectedNode();
				return paths;
			},

			_createEmptyTree: function() {
				this._pathManager.setMainPage(this);
				return this.inherited(arguments);
			},

			_fillTree: function() {
				//TODO: uncomment and set using some settings, e.g, flag of customObject (can be set as property in constuctor)
				//this._grid.isToShowOnlyHeaders = true;
				return this.inherited(arguments);
			},

			_modifyParameters: function() {
				//it's overriden to do nothing, because the button shouldn't be clickable (it's better disabled/not visible) in 3D Viewer TGV.
				// it was decided to show warning message,
				// because this functionality will be implemented/available only in next PI
				const resourceKey = 'modifyParametersMessage';
				const warningMessage = aras.getResource('../Modules/aras.innovator.Viewers/', resourceKey);
				aras.AlertWarning(warningMessage);
			},

			_getTreeGridControlPath: function() {
				return 'Viewers/TGV/ThreeDViewerTgvTreeGrid';
			},

			getTreeGridData: function(rowId, rowContextData, includeHeaders) {
				if (includeHeaders) {
					//alert('Not implemented logic in 3D Viewer, refresh in TGV works as is.');
				}
				return this.inherited(arguments);
			},

			_getRequestParametersForTreeGridData: function() {
				const result = this.inherited(arguments);
				if (this._grid.isToShowOnlyHeaders) {
					result.fetch = 1; //TGV doesn't have a logic to get only a header, so, we get minimum data for better performance.
				}
				return result;
			}
		});
	});

