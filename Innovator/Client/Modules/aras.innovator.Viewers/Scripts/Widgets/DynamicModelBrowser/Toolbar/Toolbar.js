﻿VC.Utils.Page.LoadModules(['Controls/mtButton']);
VC.Utils.Page.LoadWidgets(['DynamicModelBrowser/Toolbar/Checkbox']);

require([
	'dojo/_base/declare',
	'dojo/_base/lang',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/_WidgetsInTemplateMixin',
	'dojo/text!../Views/DynamicModelBrowserToolbarTemplate.html'],
function(declare, lang, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, dynamicModelBrowserToolbarTemplate) {
	return dojo.setObject('VC.DynamicModelBrowser.Toolbar', declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
		{
			onRefreshClick: function() {},

			constructor: function(args) {
				var refreshBtnTitle = VC.Utils.GetResource('refreshBtnTitle');
				dynamicModelBrowserToolbarTemplate = dynamicModelBrowserToolbarTemplate.Format(refreshBtnTitle);
				this.templateString = dynamicModelBrowserToolbarTemplate;
			},

			postCreate: function() {
				this.inherited(arguments);
				const checkboxLabel = VC.Utils.GetResource('loadGeometryLabel');
				this.cbLoadGeometry.setLabel(checkboxLabel);
				this.btnRefresh.onClick = this._onRefreshClick.bind(this);
			},

			_onRefreshClick: function() {
				this.onRefreshClick();
			}
		}));
});
