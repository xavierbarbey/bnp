﻿require([
	'dojo/_base/fx',
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dijit/_TemplatedMixin',
	'dijit/_WidgetsInTemplateMixin'],
	function(baseFx, declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin) {
		return dojo.setObject('VC.DynamicModelBrowser.Checkbox', declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin],
			{
				widgetsInTemplate: true,
				templateString: '<span>' +
					'<input data-dojo-attach-point="sbCheckbox" class="arasCheckboxOrRadio" type="checkbox" />' +
					'<label data-dojo-attach-point="sbLabel"></label>' +
				'</span>',

				constructor: function() {
					Object.defineProperty(this, 'isChecked',
						{
							get: function() {
								return this.sbCheckbox.checked;
							}
						});
				},

				setLabel: function(label) {
					this.sbLabel.innerText = label;
				},

				postCreate: function() {
					const checkboxId = 'cb' + VC.Utils.getTimestampString();

					this.sbCheckbox.setAttribute('id', checkboxId);
					this.sbLabel.setAttribute('for', checkboxId);
				},

				check: function() {
					this.sbCheckbox.checked = true;
				}
			}));
	});
