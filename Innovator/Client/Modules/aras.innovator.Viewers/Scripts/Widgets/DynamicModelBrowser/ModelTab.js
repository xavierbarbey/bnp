﻿VC.Utils.Page.LoadWidgets([
	'DynamicModelBrowser/TabBase',
	'DynamicModelBrowser/TreeNodesManager',
	'DynamicModelBrowser/Toolbar/Toolbar']);

require([
	'dojo/aspect',
	'dojo/_base/declare',
	'dojo/_base/connect',
	'dijit/popup',
	'dojo/dom-style',
	'dojo/text!../styles/DynamicModelBrowser/structure_to_tree_constructor.xslt',
	'dojo/text!../styles/DynamicModelBrowser/unique_tree_constructor.xslt'],
	function(aspect, declare, connect, popup, domStyle, structureToTreeXSLT, uniqueTreeNodesXSLT) {

		return dojo.setObject('VC.DynamicModelBrowser.Tabs.ModelTab', declare([VC.DynamicModelBrowser.Tabs.TabBase, VC.DynamicModelBrowser.TreeNodesManager],
		{
			tabID: 'model',
			tgvdId: null,
			countTopLevelAssemblies: 1,
			tree: null,
			toolbar: null,

			constructor: function(args) {
				args = args || {
					tgvdId: null
				};
				args.id = 'modelTab';
				this.title = VC.Utils.GetResource('modelTabTitle');
				this.tgvdId = args.tgvdId;
				this.content = '<div><div id="modelBrowserToolbar"></div><div id="modelBrowserTree" data-dojo-type="dijit/layout/ContentPane"></div></div>';
				this.toolbar = new VC.DynamicModelBrowser.Toolbar();
			},

			startup: function() {
				var modelBrowserToolbar = document.getElementById('modelBrowserToolbar');
				modelBrowserToolbar.appendChild(this.toolbar.domNode);

				this.createTgvComponent();
			},

			createTgvComponent: function() {
				const url = this.constructTgvMainPageUrl();
				const tgvPage = this.createTgvPage(url);

				const modelBrowserTree = document.getElementById('modelBrowserTree');
				modelBrowserTree.appendChild(tgvPage);
			},

			constructTgvMainPageUrl: function() {
				const startConditionProviderParam = 'startConditionProvider=parent.CustomStartConditionProvider()';
				const tgvdIdParam = 'tgvdId=' + this.tgvdId;
				const tgvUrl = aras.getBaseURL('/Modules/aras.innovator.TreeGridView/Views/MainPage.html?');
				const allParams = [tgvdIdParam, startConditionProviderParam].join('&');
				this.setTgvComponentSettings();

				return tgvUrl.concat(allParams);
			},

			setTgvComponentSettings: function() {
				window.tgvMainPageCustomExtensionPath = 'Viewers/TGV/MainPageExtension';

				window.CustomStartConditionProvider = function() {
					this.getCondition = function() {
						const rootCadItem = parent.getItem();
						const rootCadId = rootCadItem.getAttribute('id');
						const idlist = [rootCadId];
						return {
							'id': idlist
						};
					};
				};
			},

			createTgvPage: function(tgvUrl) {
				const iframe = document.createElement('iframe');
				iframe.id = 'threeDTreeGridViewer';
				iframe.width = '100%';
				iframe.height = '100%';
				iframe.frameBorder = '0';
				iframe.scrolling = 'auto';
				iframe.src = tgvUrl;
				iframe.style.position = 'absolute';
				iframe.style.top = '50px';
				iframe.style.left = '0';

				return iframe;
			}
		}));
	});
