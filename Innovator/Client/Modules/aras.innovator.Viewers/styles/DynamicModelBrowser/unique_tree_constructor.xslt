﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:strip-space elements="*"/>

	<xsl:key name="kGDById" match="tr" use="@id"/>

	<xsl:template match="node()|@*">
		<xsl:apply-templates select="node()|@*"/>
	</xsl:template>

	<xsl:template match="tr[generate-id()=generate-id(key('kGDById',@id)[1])]">
		<xsl:variable name="instances">
			<xsl:for-each select="key('kGDById',@id)/userdata[@key='instanceId']/@value">
				<xsl:if test="position() > 1">
					<xsl:text>,</xsl:text>
				</xsl:if>
				<xsl:value-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="nodes">
			<xsl:for-each select="key('kGDById',@id)/userdata[@key='originId']/@value">
				<xsl:if test="position() > 1">
					<xsl:text>,</xsl:text>
				</xsl:if>
				<xsl:value-of select="."/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="isSyncWithModelAvailable">
			<xsl:choose>
				<xsl:when test="$nodes != ''">
					<xsl:text>true</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>false</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<tr id="{@id}" icon0="{@icon0}" icon1="{@icon1}">
			<userdata key="itemId" value="{userdata[@key='itemId']/@value}"/>
			<userdata key="itemType" value="{userdata[@key='itemType']/@value}"/>
			<userdata key="treePath" value="{userdata[@key='treePath']/@value}"/>
			<xsl:if test="userdata[@key='instanceId']">
				<userdata key="instanceIds" value="{$instances}"/>
			</xsl:if>
			<xsl:if test="userdata[@key='originId']">
			<userdata key="nodeIds" value="{$nodes}"/>
			</xsl:if>
			<xsl:if test="userdata[@key='color']">
				<userdata key="color" value="{userdata[@key='color']/@value}"/>
			</xsl:if>
			<xsl:if test="userdata[@key='interferenceColor']">
				<userdata key="interferenceColor" value="{userdata[@key='interferenceColor']/@value}"/>
			</xsl:if>
			<xsl:if test="userdata[@key='isLoaded']">
				<userdata key="isLoaded" value="{userdata[@key='isLoaded']/@value}"/>
			</xsl:if>
			<xsl:if test="userdata[@key='isVisible']">
				<userdata key="isVisible" value="{userdata[@key='isVisible']/@value}"/>
			</xsl:if>
			<userdata key="isSyncWithModelAvailable" value="{$isSyncWithModelAvailable}"/>
			<td><xsl:value-of select="td"/></td>
			<xsl:apply-templates/>
		</tr>
	</xsl:template>
	<xsl:template match="tr"/>

	<xsl:template match="table">
		<table xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:aras="http://www.aras-corp.com">
			<thead>
				<th align="c">tree</th>
			</thead>
			<columns>
				<column width="100%" edit="NOEDIT"  align="l" order="0"/>
			</columns>
			<xsl:apply-templates/>
		</table>
	</xsl:template>
</xsl:stylesheet>
