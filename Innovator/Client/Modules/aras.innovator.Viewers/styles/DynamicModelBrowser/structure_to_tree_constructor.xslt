﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:strip-space elements="*"/>
	<xsl:variable name="loadGeometry" select ="'{%0%}'"/>

	<xsl:template match="Root">
		<table>
			<xsl:apply-templates select="ModelFile"/>
		</table>
	</xsl:template>

	<xsl:template match="ModelFile">
		<xsl:apply-templates select="ProductOccurence|ProductOccurrence"/>
	</xsl:template>

	<xsl:template match="ProductOccurence|ProductOccurrence">
		<xsl:param name="treePath"/>
		<xsl:choose>
			<xsl:when test="Attributes">
				<xsl:variable name="itemType" select="Attributes/Attr[@Name = 'TYPE NAME']/@Value"/>
				<xsl:choose>
					<xsl:when test="$itemType = 'Scene'">
						<xsl:apply-templates select="ProductOccurence|ProductOccurrence"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="id" select="@Id"/>
						<xsl:variable name="itemName" select="@Name"/>
						<xsl:variable name="children" select="@Children"/>
						<xsl:variable name="itemId" select="Attributes/Attr[@Name = 'ITEM ID']/@Value"/>
						<xsl:variable name="instanceId" select="Attributes/Attr[@Name = 'INSTANCE ID']/@Value"/>
						<xsl:variable name="color" select="Attributes/Attr[@Name = 'COLOR']/@Value"/>
						<xsl:variable name="interferenceColor" select="Attributes/Attr[@Name = 'INTERFERENCE COLOR']/@Value"/>
						<xsl:variable name="instanceNodeId" select="Attributes/Attr[@Name = 'INSTANCE REF']/@Value"/>
						<xsl:variable name="icon">
							<xsl:choose>
								<xsl:when test="Attributes/Attr[@Name = 'ICON']/@Value != ''">
									<xsl:value-of select="Attributes/Attr[@Name = 'ICON']/@Value"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="concat('../images/',$itemType,'.svg')"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="refId">
							<xsl:choose>
								<xsl:when test="$instanceNodeId != ''">
									<xsl:value-of select="$instanceNodeId"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$id"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<tr id="{$refId}" icon0="{$icon}" icon1="{$icon}">
							<userdata key="itemId" value="{$itemId}"/>
							<userdata key="itemType" value="{$itemType}"/>
							<userdata key="instanceId" value="{$instanceId}"/>
							<userdata key="originId" value="{$id}"/>
							<userdata key="color" value="{$color}"/>
							<userdata key="interferenceColor" value="{$interferenceColor}"/>
							<userdata key="treePath" value="{$treePath}"/>
							<userdata key="isLoaded" value="{$loadGeometry}"/>
							<userdata key="isVisible" value="{$loadGeometry}"/>
							<td>
								<xsl:value-of select="$itemName"/>
							</td>
							<xsl:if test="Attributes/Attr[@Name = 'CUSTOM PROPS']">
								<xsl:variable name="customProps" select="Attributes/Attr[@Name = 'CUSTOM PROPS']/@Value"/>
								<xsl:call-template name="extractCustom">
									<xsl:with-param name="text" select="$customProps"/>
								</xsl:call-template>
							</xsl:if>
							<xsl:variable name="path">
								<xsl:choose>
									<xsl:when test="$treePath != ''">
										<xsl:value-of select="concat($treePath, ':', $refId)"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$refId"/>
								</xsl:otherwise>
							</xsl:choose>
							</xsl:variable>
							<xsl:apply-templates select="ProductOccurence|ProductOccurrence">
								<xsl:with-param name="treePath" select="$path"/>
							</xsl:apply-templates>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="extractCustom">
		<xsl:param name="text" select="."/>
		<xsl:param name="sep" select="';'"/>
		<xsl:choose>
			<xsl:when test="not(contains($text, $sep))">
				<xsl:variable name="object" select="normalize-space($text)"/>
				<xsl:call-template name="extractProps">
					<xsl:with-param name="props" select="$object"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="object" select="normalize-space(substring-before($text, $sep))"/>
				<xsl:call-template name="extractProps">
					<xsl:with-param name="props" select="$object"/>
				</xsl:call-template>
				<xsl:call-template name="extractCustom">
					<xsl:with-param name="text" select="substring-after($text, $sep)"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="extractProps">
		<xsl:param name="props" select="."/>
		<xsl:variable name="text" select="concat($props, ',')"/>
		<xsl:variable name="nodeId" select="substring-before(substring-after($text, 'NODE ID:'), ',')"/>
		<xsl:variable name="itemId" select="substring-before(substring-after($text, 'ITEM ID:'), ',')"/>
		<xsl:variable name="itemType" select="substring-before(substring-after($text, 'TYPE NAME:'), ',')"/>
		<xsl:variable name="itemName" select="substring-before(substring-after($text, 'LABEL:'), ',')"/>
		<xsl:variable name="instanceId" select="substring-before(substring-after($text, 'INSTANCE ID:'), ',')"/>
		<xsl:variable name="icon">
			<xsl:choose>
				<xsl:when test="$itemType = 'User'">
					<xsl:text>../../../images/User.svg</xsl:text>
				</xsl:when>
				<xsl:when test="$itemType = 'Identity'">
					<xsl:text>../../../images/Identity.svg</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>../../../images/Document.svg</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<tr id="{$nodeId}" icon0="{$icon}" icon1="{$icon}">
			<userdata key="itemId" value="{$itemId}"/>
			<userdata key="itemType" value="{$itemType}"/>
			<userdata key="originId" value="{$nodeId}"/>
			<userdata key="instanceId" value="{$instanceId}"/>
			<td>
				<xsl:value-of select="$itemName"/>
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
