﻿(function(wnd) {
	if (!wnd.Set) {
		return;
	}
	var NativeSet = wnd.Set;
	var tempInstance = new NativeSet([1]);
	if (tempInstance.size > 0) {
		return;
	}
	wnd.Set = function() {
		var tempInstance = new NativeSet();
		if (arguments[0] && arguments[0].forEach) {
			arguments[0].forEach(tempInstance.add, tempInstance);
		}
		return tempInstance;
	};
})(window);
