﻿function XmlDocument() {
	var xmlDoc;
	if (window.ActiveXObject !== undefined) {
		xmlDoc = new ActiveXObject('Msxml2.FreeThreadedDOMDocument.6.0');
		xmlDoc.setProperty('AllowXsltScript', true);
		xmlDoc.async = false;
		xmlDoc.preserveWhiteSpace = true;
		xmlDoc.validateOnParse = false;
	} else {
		xmlDoc = document.implementation.createDocument('', '', null);
		xmlDoc.async = false;
		xmlDoc.preserveWhiteSpace = true;
	}
	return xmlDoc;
}
